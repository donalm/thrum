#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

# pypy q00_t5.py -C 10 -D 1 --warmup-time 10 --pghost locahost --pgpass benchmarkdbpass --pguser benchmarkdbuser --pghost 127.0.0.1 --pgdb hello_world thrum /home/donal/apg/pgbench-master/queries/1-pg_type.json

import os
import sys
import json
import math
import time
import struct
import argparse
import traceback
import monotonic

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task
from twisted.python import log
from twisted.python import util
from termcolor import cprint

#from thrum.twisted import connection
from thrum.twisted import pool
from thrum import binary

credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)

def errback(f):
    print(f.getBriefTraceback())
    return f

class TestRunner(object):
    rows = 0
    queries = 0
    template3 = "!ixxxxixxxxhxxxx?xxxxcxxxx?xxxx?xxxxcxxxxixxxxixxxxi"
    unpack_from3 = struct.Struct(template3).unpack_from
    row_unpacker = None
    nullfield = b'\xff\xff\xff\xff'

    def __init__(self, connection, args):
        self.connection = connection
        self.args = args
        self.rows = 0
        self.queries = 0
        self.latency_stats = [0 for x in range(args.timeout * 100 * 1000)]
        self.min_latency = float('inf')
        self.max_latency = 0.0

    def unpack_row_1_pg_type(self, rowdata):
        strlen = binary.i_unpack(rowdata, 2)[0]
        typname = rowdata[6:6+strlen].decode("utf-8")
        index = strlen + 10
        result = (typname,) + self.unpack_from3(rowdata, index)
        return result

    def unpack_row_2_generate_series(self, rowdata):
        # NULLABLE BUT FIXED LEN (4): i (int4)
        if self.nullfield == rowdata[2:6]:
            return (None,)
        return (binary.i_unpack(rowdata, 6),)

    def unpack_row_3_large_object(self, rowdata):
        pointer = 2

        # NULLABLE WITH VARIABLE LEN: b (bytea)
        if self.nullfield == rowdata[pointer:pointer+4]:
            return (None,)
        else:
            field_length = binary.i_unpack(rowdata, 2)[0]
            # bytea ('varchar', 'bpchar', 'text', 'unknown', 'name')
            return (self.connection.decode.bytea_decode(rowdata, 6, field_length),)

        return Record(b)

    def unpack_row_4_arrays(self, rowdata):
        # NULLABLE WITH VARIABLE LEN: a (array_int4)
        if self.nullfield == rowdata[2:6]:
            return (None,)
        else:
            field_length = binary.i_unpack(rowdata, 2)[0]
            # array_int4 ('varchar', 'bpchar', 'text', 'unknown', 'name')
            return (self.connection.decode._binary_array_decode(rowdata, 6, field_length),)

    def per_row_callable(self, row):
        self.rows += 1
        return row

    def prepare_statement(self, query):
        prepared_statement = self.connection.prepare_statement(query)
        df = self.connection.parse_statement(prepared_statement)
        df.addCallback(self.optimize_row_unpacker)
        df.addErrback(errback)
        return df

    def optimize_row_unpacker(self, prepared_statement):
        if '1-pg_type.json' in self.args.queryfile:
            self.row_unpacker = self.unpack_row_1_pg_type
            return prepared_statement
        elif '2-generate_series.json' in self.args.queryfile:
            self.row_unpacker = self.unpack_row_2_generate_series
            return prepared_statement
        elif '3-large_object.json' in self.args.queryfile:
            self.row_unpacker = self.unpack_row_3_large_object
            return prepared_statement
        elif '4-arrays.json' in self.args.queryfile:
            self.row_unpacker = self.unpack_row_4_arrays
            return prepared_statement

        df = prepared_statement.compile_custom_unpacker(self.connection)
        df.addCallback(lambda x:prepared_statement)
        df.addErrback(errback)
        return df

    def dispatch(self, prepared_statement, query_args, until):
        return self._dispatch(None, prepared_statement, query_args, until)

    def _dispatch(self, _, prepared_statement, query_args, until):
        now = monotonic.monotonic()
        if now >= until:
            return

        portal = self.connection.make_portal(prepared_statement,
                                  parameters=query_args,
                                  row_cache_size=5000,
                                  row_unpacker=self.row_unpacker,
                                  per_row_callable=self.per_row_callable)
        df = self.connection.bind_and_execute(portal)
        df.addCallback(self.tally, now)
        df.addCallback(self._dispatch, prepared_statement, query_args, until)
        return df

    def tally(self, result, start_time):
        now = monotonic.monotonic()
        req_time = int(round((now - start_time) * 1000 * 100))

        if req_time > self.max_latency:
            self.max_latency = req_time
        if req_time < self.min_latency:
            self.min_latency = req_time
        self.latency_stats[req_time] += 1
        self.queries += 1
        return None

    def bracket(self, x):
        print("\n============\n")
        print("rows:    %s" % (self.rows,))
        print("queries: %s" % (self.queries,))
        self.rows = 0
        self.queries = 0
        return x

    def run_until(self, query, query_args, until):
        df = self.prepare_statement(query)
        df.addCallback(self.dispatch, query_args, until)
        df.addCallback(lambda x:self)
        df.addErrback(errback)
        return df

    def release_connection(self, cpool):
        cpool.release_connection(self.connection)
        return self

def do_run(cpool, args, duration, query, query_args):
    l = []
    until = monotonic.monotonic() + duration
    for index in range(args.concurrency):
        df = cpool.acquire_connection()
        df.addCallback(lambda connection: TestRunner(connection, args))
        df.addCallback(lambda testrunner: testrunner.run_until(query, query_args, until))
        df.addCallback(lambda testrunner: testrunner.release_connection(cpool))
        l.append(df)
    df = defer.gatherResults(l)
    return df

def report(testrunners):

    min_latency = float('inf')
    max_latency = 0.0
    queries = 0
    rows = 0
    latency_stats = None
    duration = testrunners[0].args.duration

    for testrunner in testrunners:
        '''
        t_queries, t_rows, t_latency_stats, t_min_latency, t_max_latency = \
            result
        '''

        queries += testrunner.queries
        rows += testrunner.rows
        if latency_stats is None:
            latency_stats = testrunner.latency_stats
        else:
            for index, element in enumerate(testrunner.latency_stats):
                latency_stats[index] += element
        if testrunner.max_latency > max_latency:
            max_latency = testrunner.max_latency
        if testrunner.min_latency < min_latency:
            min_latency = testrunner.min_latency

    data = {
        'queries': queries,
        'rows': rows,
        'duration': duration,
        'min_latency': min_latency,
        'max_latency': max_latency,
        'latency_stats': latency_stats,
        'output_format': 'json'
    }

    fh = open("/tmp/results.json", "w")
    fh.write(json.dumps(data))
    fh.close()
    print(json.dumps(data))


def main():
    parser = argparse.ArgumentParser(
        description='async pg driver benchmark [concurrent]')
    parser.add_argument(
        '-C', '--concurrency', type=int, default=10,
        help='number of concurrent connections')
    parser.add_argument(
        '-D', '--duration', type=int, default=30,
        help='duration of test in seconds')
    parser.add_argument(
        '--timeout', default=2, type=int,
        help='server timeout in seconds')
    parser.add_argument(
        '--warmup-time', type=int, default=5,
        help='duration of warmup period for each benchmark in seconds')
    parser.add_argument(
        '--output-format', default='text', type=str,
        help='output format', choices=['text', 'json'])
    parser.add_argument(
        '--pgdb', type=str, default=None,
        help='PostgreSQL server db')
    parser.add_argument(
        '--pghost', type=str, default='127.0.0.1',
        help='PostgreSQL server host')
    parser.add_argument(
        '--pgpass', type=str, default=None,
        help='PostgreSQL server password')
    parser.add_argument(
        '--pgport', type=int, default=5432,
        help='PostgreSQL server port')
    parser.add_argument(
        '--pguser', type=str, default='postgres',
        help='PostgreSQL server user')
    parser.add_argument(
        'driver', help='driver name; defaults to thrum')
    parser.add_argument(
        'queryfile', help='file to read benchmark query information from')

    args = parser.parse_args()

    if args.queryfile == '-':
        querydata_text = sys.stdin.read()
    else:
        with open(args.queryfile, 'rt') as f:
            querydata_text = f.read()

    querydata = json.loads(querydata_text)

    query = querydata.get('query')
    if not query:
        die('missing "query" in query JSON')

    query_args = querydata.get('args')
    if not query_args:
        query_args = []

    setup = querydata.get('setup')
    teardown = querydata.get('teardown')
    if setup and not teardown:
        die('"setup" is present, but "teardown" is missing in query JSON')

    # start the reactor to kick off connection estabilishing
    reactor.callWhenRunning(txmain, args, query, query_args, setup, teardown)
    reactor.run()


def ss(l):
    print("%s\n" % (l,))
    return l

def run_queries(connection_pool, queries):

    try:
        query, queries = queries.strip().strip(";").split(";", 1)
    except Exception:
        query = queries.strip().strip(";")
        queries = None

    try:
        df = connection_pool.acquire_connection()
        df.addCallback(lambda connection: connection.execute(query))
        df.addCallback(lambda connection: ss(query))
        df.addCallback(lambda x: task.deferLater(reactor, 1, lambda: x))
        if queries:
            df.addCallback(lambda x:run_queries(connection_pool, queries))
        else:
            df.addCallback(lambda x:connection_pool)
        df.addErrback(errback)
        return df
    except Exception:
        raise

def txmain(args, query, query_args, setup, teardown):
    # connect to the database

    credentials = {}
    if args.pguser:
        credentials['user'] = args.pguser
    if args.pgpass:
        credentials['password'] = args.pgpass
    if args.pghost:
        credentials['host'] = args.pghost
    if args.pgport:
        credentials['port'] = args.pgport
    if args.pgdb:
        credentials['database'] = args.pgdb

    cp = pool.ConnectionPool(**credentials)
    cp.max = args.concurrency
    cp.min = args.concurrency
    df = cp.start()

    if setup:
        df.addCallback(run_queries, setup)

    df.addCallback(do_run, args, 1, query, query_args)

    for i in range(args.warmup_time - 1):
        df.addCallback(lambda x: do_run(cp, args, 1, query, query_args))
    df.addCallback(lambda x: do_run(cp, args, args.duration, query, query_args))
    df.addCallback(lambda x: do_run(cp, args, args.duration, query, query_args))
    df.addCallback(lambda x: do_run(cp, args, args.duration, query, query_args))
    df.addCallback(lambda x: do_run(cp, args, args.duration, query, query_args))
    df.addCallback(report)

    if teardown:
        df.addCallback(lambda x: run_queries(cp, teardown))

    # log any errors and stop the reactor
    df.addErrback(errback)
    df.addBoth(lambda _: reactor.stop())


if __name__ == '__main__':
    main()
