##!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import math
import time
import struct
import traceback

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task
from twisted.python import log
from twisted.python import util
from termcolor import cprint

from thrum.twisted import connection
from thrum.twisted import cpool
from thrum import binary

RECORDS = 537
try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

TARGET = 1000000.0
LOOPS = int(math.ceil((TARGET/RANGE) / RECORDS))



QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2
'''
'''LIMIT 40'''
ARGS = (-1, True)

credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)

def eb(f):
    print(f.getBriefTraceback())
    return f

# define the libpq connection string and the query to use
query = 'select tablename from pg_tables order by tablename'

# connect to the database
cp = cpool.ConnectionPool(**credentials)
cp.min = 7
cp.max = 7#15
df = cp.start()


def show(r, v=''):
    sys.stdout.write("\nshow%s: " % (v,))
    try:
        for x in r:
            sys.stdout.write("\n  %s" % (x,))
        return
        sys.stdout.write("\n  %s" % (r[1],))
        sys.stdout.write("\n  %s" % (r[2],))
        sys.stdout.write("\n\n")
    except:
        sys.stdout.write("%s\n" % (r,))


c=0
def analyse(conn, index):
    turnarounds = conn._protocol.turnarounds
    global c
    print("\nstats %s %s" % (c, index))
    c += 1
    av = sum(turnarounds) / (len(turnarounds) * 1.0)
    mn = min(turnarounds)
    mx = max(turnarounds)
    print("""min: %s\nmax: %s\nave: %s""" % (mn, mx, av,))
    return conn


class RowHandling(object):
    rc = 0
    frc = 0
    qc = 0
    start_time = 0
    template3 = "!ixxxxixxxxhxxxx?xxxxcxxxx?xxxx?xxxxcxxxxixxxxixxxxi"
    unpack_from3 = struct.Struct(template3).unpack_from
    perf = 0

    @classmethod
    def ru(cls, rowdata):
        #print("ru")
        try:
            strlen = binary.i_unpack(rowdata[2:6])[0]
            typname = rowdata[6:6+strlen].decode("utf-8")
            index = strlen + 6 + 4
            remaining_data = rowdata[index:]

            result = (typname,) + cls.unpack_from3(remaining_data)
            return result
        except Exception as e:
            cls.frc += 1
            print(traceback.format_exc())
        return

    @classmethod
    def prc(cls, row):
        #print("prc")
        cls.rc += 1
        return row

    @classmethod
    def interval(cls, o=None):
        cls.perf =  "%s\n\n" % (str(time.time() - cls.start_time)[0:5],)
        return o

def run_queries(cpool, count):
    l = []
    RowHandling.start_time = time.time()
    for index in range(count):
        l.append(cpool.runQuery(QUERY, ARGS, row_unpacker=RowHandling.ru, per_row_callable=RowHandling.prc))
        l[-1].addErrback(eb)
        RowHandling.qc += 1
    return defer.DeferredList(l)


'''
'''

def brk(x):
    print("\n============\n")
    print("rows:    %s" % (RowHandling.rc,))
    print("queries: %s" % (RowHandling.qc,))
    print("time:    %s" % (RowHandling.perf,))
    RowHandling.rc = 0
    RowHandling.qc = 0
    return x

def make_connections(cpool, r):
    df = run_queries(cpool, LOOPS)
    df.addCallback(oof, cpool)
    df.addErrback(eb)
    return df

def oof(r, cpool):
    RowHandling.interval()
    l = []
    for index in range(RANGE):
        df = cpool.acquire_connection()
        df.addCallback(analyse, index)
        df.addCallback(cpool.release_connection)
        df.addErrback(eb)
        l.append(df)
    return defer.DeferredList(l)

df.addCallback(make_connections, RANGE)
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
'''
'''


# log any errors and stop the reactor
df.addErrback(eb)
df.addBoth(lambda _: reactor.stop())

# start the reactor to kick off connection estabilishing
reactor.run()
#print("rows:    %s" % (RowHandling.rc,))
#print("queries: %s" % (RowHandling.qc,))
