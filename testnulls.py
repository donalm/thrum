#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import struct
import random

from thrum import binary

MIN_INT2 = -2 ** 15                                                                                                                                                                
MIN_INT4 = -2 ** 31                                                                                                                                                                
MIN_INT8 = -2 ** 63                                                                                                                                                                
                                                                                                                                                                                   
MAX_INT2 = 2 ** 15 - 1                                                                                                                                                             
MAX_INT4 = 2 ** 31 - 1                                                                                                                                                             
MAX_INT8 = 2 ** 63 - 1 

c = 0
def i():
    global c
    y = random.randint(1, 100)
    if y < 5:
        return None
    return random.randint(MIN_INT4, MAX_INT4)


MINUS_ONE = b'\xff\xff\xff\xff'
ONE = b'\x00\x00\x00\x01'
TWO = b'\x00\x00\x00\x02'
THREE = b'\x00\x00\x00\x03'
FOUR = b'\x00\x00\x00\x04'
EIGHT = b'\x00\x00\x00\x08'
SIXTEEN = b'\x00\x00\x00\x10'
THIRTY_TWO = b'\x00\x00\x00\x20'

testdata = []

patternlen = 7

for x in range(1000):
    testrow = []
    for y in range(7):
        n = i()
        if n is None:
            testrow.append(MINUS_ONE)
            c += 1
        else:
            testrow.extend((FOUR, binary.i_pack(n)))
    testdata.append(b''.join(testrow))


z = 0
for i in testdata:
    if MINUS_ONE in i:
        z += 1
print(z)


y = 0
for i in testdata:
    print testdata

print(c)


'''



For a given field type, you

A row arrives.
For each field in a row we want to know its layout

FIELD:
    is it fixedlen?  DONE
    is it fixedlen but nullable? DONE
    is it varlen?
        is it NULL? DONE
        get its length from dict? DONE
        get its length from binary.i_unpack


'''

class Bloc(object):
    __slots__ = ('result',
                 'ready',
                 'start_index',
                 'end_index')

    def __init__(self):
        self.result = None
        self.ready = False
        self.start_index = 0
        self.end_index = 0


MINUS_ONE = b'\xff\xff\xff\xff'
def hr(rowdata):
    m_mone = MINUS_ONE

    blocs = []
    imprints = []

    index_l = 0
    index_d = index_l + 4
    if rowdata[index_l:4] == MINUS_ONE:
        typname = None
        index_l = index_d
        index_d += 4
    else:
        l = binary.i_unpack(rowdata[index_l:index_d]) 
        typname = rowdata[index_d:index_d+l]
        index_l = index_d + l
        index_d = index_l += 4


    accumulator = {'startindex':index_l}
    pattern = 'n'
    reason_tree = {'n':
                    {'n':'nn'}
                   'nn':
                    {'n':'nnn'}
                  }
    """
    Nullable
    """
    typnamespace = None
    typnamespace_is_null = False
    prefixlen = 4
    fixedlen = 4
    if rowdata[index_l:prefixlen] == MINUS_ONE:
        typnamespace_is_null = True
        index_l = index_d
        index_d += prefixlen
        imprints.append("xxxx")
    else:
        l = fixedlen
        typname = rowdata[index_d:index_d+l]
        index_l += prefixlen + fixedlen
        index_d += prefixlen + fixedlen
        imprints.append("xxxxi")






    for bloc in blocs:
        if bloc.result:
            continue
            imprint = ''.join(bloc.imprints)
            bloc.result = struct.unpack(imprint, rowdata[bloc.start_index:bloc.end_index])

    
























































