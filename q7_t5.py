#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import math
import time
import struct
import traceback
import collections

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task
from twisted.python import log
from twisted.python import util
from termcolor import cprint

#from thrum.twisted import connection
from thrum.twisted import pool
from thrum import binary

pid = os.getpid()

if sys.platform == 'darwin':
    RECORDS = 537
else:
    RECORDS = 413

#RECORDS = 10000

try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

TARGET = 1000000.0
LOOPS = math.ceil((TARGET/RANGE) / RECORDS)
LOOPS = LOOPS-5
print(LOOPS)

print("sudo renice -20 %s" % (pid,))
try:
    z = raw_input("<ENTER> to continue")
except:
    z = input("<ENTER> to continue")


R2 = collections.namedtuple("R2", ("id", "randomnumber", "rvn"))
def _unpack_row_s(rowdata):
    #print("ROW S")
    # NOT NULLABLE AND FIXED LEN (4): id (int4)
    # NOT NULLABLE AND FIXED LEN (4): randomnumber (int4)
    # NOT NULLABLE AND FIXED LEN (4): rvn (int4)
    return R2(*struct.unpack_from('!ixxxxixxxxi', rowdata, 6))

QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2
'''
'''LIMIT 40'''
ARGS = (-1, True)
#QUERY = """select id, randomnumber, randomnumber  from "World" """
#ARGS = ()
#QUERY = "SELECT i FROM generate_series(1, $1) AS i"
#ARGS = (1000,)
#QUERY = """ SELECT * FROM nulltest """
#ARGS =()
credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)

def eb(f):
    print(f.getBriefTraceback())
    return f

# define the libpq connection string and the query to use
query = 'select tablename from pg_tables order by tablename'

# connect to the database
cp = pool.ConnectionPool(**credentials)
cp.min = 7
cp.max = 15
#cp.min = 2#min(RANGE, cp.max)
df = cp.start()


def show(r, v=''):
    sys.stdout.write("\nshow%s: " % (v,))
    try:
        for x in r[0]:
            sys.stdout.write("\n  %s" % (x,))
        sys.stdout.write("\n  %s" % (r[1],))
        sys.stdout.write("\n  %s" % (r[2],))
        sys.stdout.write("\n\n")
    except:
        sys.stdout.write("%s\n" % (r,))


def rs9(r):
    return
    print("rs9")
    rows = r[0]
    if type(rows) in (list, tuple):
        print('x')
        for row in rows:
            print(row)
    else:
        print(type(r))
        print(r)
    return r

c=0
def analyse(conn, index):
    turnarounds = conn.turnarounds
    global c
    print("\nstats %s %s" % (c, index))
    c += 1
    av = sum(turnarounds) / (len(turnarounds) * 1.0)
    mn = min(turnarounds)
    mx = max(turnarounds)
    print("""min: %s\nmax: %s\nave: %s""" % (mn, mx, av,))
    conn.turnarounds = []
    return conn


class RowHandling(object):
    rc = 0
    frc = 0
    qc = 0
    maxmessages = 0
    start_time = 0
    template3 = "!ixxxxixxxxhxxxx?xxxxcxxxx?xxxx?xxxxcxxxxixxxxixxxxi"
    unpack_from3 = struct.Struct(template3).unpack_from
    perf = 0
    Record = collections.namedtuple("Record", ("typname",
                                               "typnamespace",
                                               "typowner",
                                               "typlen",
                                               "typbyval",
                                               "typcategory",
                                               "typispreferred",
                                               "typisdefined",
                                               "typdelim",
                                               "typrelid",
                                               "typelem",
                                               "typarray"))

    @classmethod
    def ru(cls, rowdata):
        try:
            strlen = binary.i_unpack(rowdata[2:6])[0]
            typname = rowdata[6:6+strlen].decode("utf-8")
            index = strlen + 6 + 4
            remaining_data = rowdata[index:]

            result = (typname,) + cls.unpack_from3(remaining_data)
            return result
        except Exception as e:
            cls.frc += 1
            print(traceback.format_exc())
        return

    @classmethod
    def unpack_row_1_pg_type(cls, rowdata):
        strlen = binary.i_unpack(rowdata, 2)[0]
        typname = rowdata[6:6+strlen].decode("utf-8")
        index = strlen + 10
        return cls.Record(typname, *cls.unpack_from3(rowdata, index))

    @classmethod
    def prc(cls, row):
        cls.rc += 1
        return row

    @classmethod
    def interval(cls, o):
        cls.perf =  "%s\n\n" % (str(time.time() - cls.start_time)[0:5],)
        return o

    @classmethod
    def query(cls, _, connection, statement, index):
        cls.qc += 1
        try:
            portal = connection.make_portal(statement, parameters=ARGS, row_cache_size=5000, row_unpacker=cls.unpack_row_1_pg_type, per_row_callable=cls.prc)
            #portal = connection.make_portal(statement, parameters=ARGS, row_cache_size=5000, row_unpacker=_unpack_row2, per_row_callable=cls.prc)
            #portal = connection.make_portal(statement, parameters=ARGS, row_cache_size=15000, row_unpacker=_unpack_row_s, per_row_callable=cls.prc)
            #portal = connection.make_portal(statement, parameters=ARGS, row_cache_size=15000, row_unpacker=None, per_row_callable=cls.prc)
        except Exception as e:
            print(traceback.format_exc())
        return connection.bind_and_execute(portal, getall=True)


def optimize_unpacker(prepared_statement, conn):
    df = prepared_statement.compile_custom_unpacker(conn)
    df.addCallback(rs9)
    df.addCallback(lambda x:prepared_statement)
    df.addErrback(eb)
    return df


def prepare_statement(conn, index):
    prepared_statement = conn.prepare_statement(QUERY)
    df = conn.parse_statement(prepared_statement)
    df.addCallback(optimize_unpacker, conn)
    df.addCallback(lambda prepared_statement:(conn, prepared_statement))
    df.addErrback(eb)
    return df

def run_queries(result, index):
    conn, prepared_statement = result
    RowHandling.start_time = time.time()
    df = dispatch(None, conn, prepared_statement, index, c=LOOPS)
    df.addCallback(RowHandling.interval)
    return df


def dispatch(_, conn, prepared_statement, index, c=1):
    RowHandling.maxmessages = max(RowHandling.maxmessages, conn.maxmessages)
    if c <=0:
        return
    try:
        df = RowHandling.query(None, conn, prepared_statement, index)
    except Exception as e:
        print(traceback.format_exc())

    df.addCallback(dispatch, conn, prepared_statement, index, c-1)
    df.addCallback(lambda x:conn)
    return df

def brk(x):
    print("\n============\n")
    print("msgs:    %s" % (RowHandling.maxmessages,))
    print("rows:    %s" % (RowHandling.rc,))
    print("queries: %s" % (RowHandling.qc,))
    print("time:    %s" % (RowHandling.perf,))
    RowHandling.rc = 0
    RowHandling.qc = 0
    return x

def make_connections(cpool, r):
    l = []
    for index in range(r):
        df = cpool.acquire_connection()
        df.addCallback(prepare_statement, index)
        df.addCallback(run_queries, index)
        #df.addCallback(analyse, index)
        df.addCallback(cpool.release_connection)
        l.append(df)
    return defer.DeferredList(l)

df.addCallback(make_connections, RANGE)
df.addCallback(brk)

for x in range(40):
    df.addCallback(lambda x:make_connections(cp, RANGE))
    df.addCallback(brk)
'''
'''


# log any errors and stop the reactor
df.addErrback(eb)
df.addBoth(lambda _: reactor.stop())

# start the reactor to kick off connection estabilishing
reactor.run()
