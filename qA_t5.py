##!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import time
import math
import struct
import traceback

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task
from twisted.python import log
from twisted.python import util
from termcolor import cprint

from thrum.twisted import connection
from thrum.twisted import cpool
from thrum import binary

try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

if sys.platform == 'darwin':
    RECORDS = 537
else:
    RECORDS = 413

TARGET = 1000000.0
LOOPS = math.ceil((TARGET/RANGE) / RECORDS)


QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2
'''
'''LIMIT 40'''
ARGS = (-1, True)

credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)

def eb(f):
    print(f.getBriefTraceback())
    return f

# connect to the database
cp = cpool.ConnectionPool(**credentials)
cp.min = 8
cp.max = 15
df = cp.start()


def show(r, v=''):
    sys.stdout.write("\nshow%s: " % (v,))
    try:
        for x in r[0]:
            sys.stdout.write("\n  %s" % (x,))
        sys.stdout.write("\n  %s" % (r[1],))
        sys.stdout.write("\n  %s" % (r[2],))
        sys.stdout.write("\n\n")
    except:
        sys.stdout.write("%s\n" % (r,))


c=0
def analyse(conn, index):
    turnarounds = conn._protocol.turnarounds
    global c
    print("\nstats %s %s" % (c, index))
    c += 1
    av = sum(turnarounds) / (len(turnarounds) * 1.0)
    mn = min(turnarounds)
    mx = max(turnarounds)
    print("""min: %s\nmax: %s\nave: %s""" % (mn, mx, av,))
    return conn


class RowHandling(object):
    rc = 0
    frc = 0
    qc = 0
    start_time = 0
    template3 = "!ixxxxixxxxhxxxx?xxxxcxxxx?xxxx?xxxxcxxxxixxxxixxxxi"
    unpack_from3 = struct.Struct(template3).unpack_from
    perf = 0

    @classmethod
    def ru(cls, rowdata):
        try:
            strlen = binary.i_unpack(rowdata[2:6])[0]
            typname = rowdata[6:6+strlen].decode("utf-8")
            index = strlen + 6 + 4
            remaining_data = rowdata[index:]

            result = (typname,) + cls.unpack_from3(remaining_data)
            return result
        except Exception as e:
            cls.frc += 1
            print(traceback.format_exc())
        return

    @classmethod
    def prc(cls, row):
        cls.rc += 1
        return row

    @classmethod
    def interval(cls, o):
        cls.perf =  "%s\n\n" % (str(time.time() - cls.start_time)[0:5],)
        return o

    @classmethod
    def query(cls, _, connection, statement, index):
        cls.qc += 1
        try:
            return connection.runQuery(statement, ARGS, row_unpacker=RowHandling.ru, per_row_callable=RowHandling.prc)
        except Exception as e:
            print(traceback.format_exc())

def run_queries(conn, index):
    RowHandling.start_time = time.time()
    df = dispatch(None, conn, QUERY, index, c=LOOPS)
    df.addCallback(RowHandling.interval)
    df.addCallback(lambda x:conn)
    df.addErrback(eb)
    return df

def dispatch(_, conn, query, index, c=1):
    if c <= 0:
        return
    try:
        df = RowHandling.query(None, conn, query, index) 
    except Exception as e:
        print(traceback.format_exc())

    df.addCallback(dispatch, conn, QUERY, index, c-1)
    df.addCallback(lambda x:conn)
    return df

def brk(x):
    print("\n============\n")
    print("rows:    %s" % (RowHandling.rc,))
    print("queries: %s" % (RowHandling.qc,))
    print("time:    %s" % (RowHandling.perf,))
    RowHandling.rc = 0
    RowHandling.qc = 0
    return x

def make_connections(cpool, r):
    l = []
    for index in range(r):
        df = cpool.acquire_connection()
        df.addCallback(run_queries, index)
        df.addCallback(analyse, index)
        df.addCallback(cpool.release_connection)
        df.addErrback(eb)
        l.append(df)
    return defer.DeferredList(l)

df.addCallback(make_connections, RANGE)
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
df.addCallback(lambda x:make_connections(cp, RANGE))
df.addCallback(brk)
'''
'''


# log any errors and stop the reactor
df.addErrback(eb)
df.addBoth(lambda _: reactor.stop())

# start the reactor to kick off connection estabilishing
reactor.run()
