#!/usr/bin/env python

import re

REGEX_0 = re.compile(r"^\b(\d)")
REGEX_1 = re.compile(r"[^a-zA-Z0-9_]+")
REGEX_2 = re.compile(r"^\b(_+)")


def fieldname(fieldname):
    """
    keys in a namedtuple must obey attribute-name constraints and additionally
    cannot begin with an underscore

    BEFORE:                   AFTER:
    Record.2unlimited         Record.f_2unlimited
    Record._breaks            Record.f_breaks
    Record.no spaces          Record.no_spaces
    Record.only-valid-char!   Record.only_valid_chars_

    Replace non-alphanumeric characters with underscores
    If first character is numeric, prefix with 'f_'
    If first character is an underscore, prefix with 'f'
    """

    fieldname = REGEX_0.sub(r"f_\1", fieldname)
    fieldname = REGEX_1.sub("_", fieldname)
    return REGEX_2.sub("f_", fieldname)
