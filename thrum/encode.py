#!/usr/bin/env pypy

import re
import six
import uuid
import math
import json
import decimal
import datetime
import calendar
import locale

try:
    intern
except NameError:
    from sys import intern

try:
    import ipaddress
except ImportError:
    ipaddress = None

try:
    import pghstore
except ImportError:
    pghstore = None


from .pypy import BytesBuilder
from . import tsvector
from . import constants
from . import errors
from . import tzutc
from . import binary


class Encode(object):
    """
    Translate binary data received over the wire from Postgres into Python
    objects, and vice-versa.
    """
    def __init__(self, oids, client_encoding=b"utf-8"):
        # self.client_encoding must be bytes, e.g.    b"utf-8"
        # self.uclient_encoding must be unicode, e.g. u"utf-8"
        try:
            self.client_encoding = client_encoding.encode("utf-8")
            self.uclient_encoding = client_encoding
        except:
            self.client_encoding = client_encoding
            self.uclient_encoding = client_encoding.decode("utf-8")
        self.oids = oids

        self.FC_TEXT = constants.FC_TEXT  # 0
        self.FC_BINARY = constants.FC_BINARY  # 1

        self.utc = tzutc.UTC()
        self.epoch = datetime.datetime(2000, 1, 1)
        self.epoch_tz = self.epoch.replace(tzinfo=self.utc)
        self.base_epoch_seconds = calendar.timegm(self.epoch.timetuple())

        self.max_date_ordinal = datetime.date.max.toordinal()
        self.min_date_ordinal = datetime.date.min.toordinal()
        self.epoch_ordinal = self.epoch.toordinal()

        date_format = self.get_date_format()
        self.index_year = date_format.index("year")
        self.index_month = date_format.index("month")
        self.index_day = date_format.index("day")

        self.infinity_microseconds = 2 ** 63 - 1
        self.minus_infinity_microseconds = -1 * self.infinity_microseconds - 1

        self.true = b"\x01"
        self.false = b"\x00"
        self.null = binary.i_pack(-1)
        self.null_byte = b"\x00"

        self.macaddr_regex = re.compile(":|-")
        self.datesplit_regex = re.compile("[^\d]+")
        self.timesplit_regex = re.compile("[^\d\.]+")
        self.tsvector_regex = re.compile("([^:]+)(:(\d+)([ABCD]?))?")

        self.encoders = {}
        self.update_encoders()

    def get_date_format(self):
        """
        Return the correct order of day/month/year for the current locale, in
        the format:
        ['day', 'month'. 'year']

        @return: A list of 'day','month'.'year' in some order
        @rtype: L{list}
        """
        if locale.getlocale() == (None, None):
            locale.setlocale(locale.LC_ALL, '')

        abbrs = {'d': 'day',
                 'm': 'month',
                 'y': 'year'}

        locale_date_template = locale.nl_langinfo(locale.D_FMT)
        locale_date_keys = locale_date_template.replace("%", "").split("/")
        return [abbrs[key.lower()] for key in locale_date_keys]

    def update_client_encoding(self, encoding):
        """
        Update our client encoding (e.g. 'utf-8'), normally based on the value
        sent to us by the server as part of a PARAMETER_STATUS message
        a datetime.datetime with no timezone data

        @param encoding: The new encoding, e.g. 'utf-8'
        @type encoding: L{str}
        """
        self.encodings = constants.pg_to_py
        self.client_encoding = constants.pg_to_py.get(encoding, encoding)

    def update_encoders(self):
        """
        Postgres requires clients to request data in either the binary or text
        format. Within one query you can request some fields to be returned as
        text and others as binary by supplying the correct format code.

        This method sets up the encoders for data we'll send to Postgres and
        for each one, it establishes whether that encoder will return bytes
        in the binary or data format.

        The format codes can be found in constants.py, but basically FC_TEXT
        is 0 and FC_BINARY is 1

        It's preferable to use binary for almost everything. The two
        exceptions are the 'numeric' data type (decimal.Decimal in Python) and
        hstore data, just because we're using pghstore for now.

        BTW if you're using hstore for key/value data, there is no penaly for
        switching to jsonb as the underlying code on the server side is ~the
        same.

        We intern the keys in this method, and also in the OID class, which in
        theory makes lookups faster at least in cpython, but I haven't
        profiled this.
        """
        fcb = self.FC_BINARY
        fct = self.FC_TEXT
        self.encoders["bool"] = (self.bool_encode, fcb)
        self.encoders["int2"] = (self.int2_encode, fcb)
        self.encoders["int4"] = (self.int4_encode, fcb)
        self.encoders["int8"] = (self.int8_encode, fcb)
        self.encoders["array_int2"] = (self.array_int2_encode, fcb)
        self.encoders["array_int4"] = (self.array_int4_encode, fcb)
        self.encoders["array_int8"] = (self.array_int8_encode, fcb)
        self.encoders["array_float4"] = (self.array_float4_encode, fcb)
        self.encoders["array_float8"] = (self.array_float8_encode, fcb)
        self.encoders["array_oid"] = (self.array_oid_encode, fcb)
        self.encoders["array_xid"] = (self.array_xid_encode, fcb)
        self.encoders["array_bool"] = (self.array_bool_encode, fcb)
        self.encoders["array_varchar"] = (self.array_varchar_encode, fcb)
        self.encoders["array_char"] = (self.array_bpchar_encode, fcb)
        self.encoders["array_bpchar"] = (self.array_bpchar_encode, fcb)
        self.encoders["array_json"] = (self.array_json_encode, fcb)
        self.encoders["array_jsonb"] = (self.array_jsonb_encode, fcb)
        self.encoders["array_point"] = (self.array_point_encode, fcb)
        self.encoders["array_circle"] = (self.array_circle_encode, fcb)
        self.encoders["array_lseg"] = (self.array_lseg_encode, fcb)
        self.encoders["array_path"] = (self.array_path_encode, fcb)
        self.encoders["array_line"] = (self.array_line_encode, fcb)
        self.encoders["array_box"] = (self.array_box_encode, fcb)
        self.encoders["array_polygon"] = (self.array_polygon_encode, fcb)
        self.encoders["array_cidr"] = (self.array_cidr_encode, fcb)
        self.encoders["array_money"] = (self.array_money_encode, fcb)
        self.encoders["array_bytea"] = (self.array_bytea_encode, fcb)
        self.encoders["array_text"] = (self.array_text_encode, fcb)
        self.encoders["array_macaddr"] = (self.array_macaddr_encode, fcb)
        self.encoders["array_inet"] = (self.array_inet_encode, fcb)
        self.encoders["array_numeric"] = (self.array_numeric_encode, fcb)
        self.encoders["array_timestamp"] = (self.array_timestamp_encode, fcb)
        self.encoders["array_timestamptz"] = (self.array_timestamptz_encode,
                                              fcb)
        self.encoders["array_timetz"] = (self.array_timetz_encode, fcb)
        self.encoders["array_interval"] = (self.array_interval_encode, fcb)
        self.encoders["array_time"] = (self.array_time_encode, fcb)
        self.encoders["array_date"] = (self.array_date_encode, fcb)
        self.encoders["array_bit"] = (self.array_bit_encode, fcb)
        self.encoders["array_varbit"] = (self.array_varbit_encode, fcb)
        self.encoders["array_uuid"] = (self.array_uuid_encode, fcb)
        self.encoders["array_xml"] = (self.array_xml_encode, fcb)
        self.encoders["bpchar"] = (self.text_encode, fcb)
        self.encoders["char"] = (self.text_encode, fcb)
        self.encoders["bytea"] = (self.bytea_encode, fcb)
        self.encoders["bit"] = (self.bit_encode, fcb)
        self.encoders["varbit"] = (self.bit_encode, fcb)
        self.encoders["text"] = (self.text_encode, fcb)
        self.encoders["varchar"] = (self.text_encode, fcb)
        self.encoders["xml"] = (self.text_encode, fcb)
        self.encoders["tsvector"] = (self.tsvector_encode, fcb)
        self.encoders["point"] = (self.point_encode, fcb)
        self.encoders["lseg"] = (self.line_segment_encode, fcb)
        self.encoders["path"] = (self.path_encode, fcb)
        self.encoders["box"] = (self.box_encode, fcb)
        self.encoders["polygon"] = (self.polygon_encode, fcb)
        self.encoders["line"] = (self.line_encode, fcb)
        self.encoders["cidr"] = (self.cidr_encode, fcb)
        self.encoders["float4"] = (binary.f_pack, fcb)
        self.encoders["float8"] = (binary.d_pack, fcb)
        self.encoders["unknown"] = (self.unknown_out, fcb)
        self.encoders["circle"] = (self.circle_encode, fcb)
        self.encoders["money"] = (self.money_encode, fcb)
        self.encoders["macaddr"] = (self.macaddr_encode, fcb)
        self.encoders["inet"] = (self.inet_encode, fcb)
        self.encoders["date"] = (self.date_encode, fcb)
        self.encoders["numeric"] = (self.numeric_encode_binary, fcb)
        self.encoders["cstring"] = (self.cstring_encode, fcb)
        self.encoders["uuid"] = (self.uuid_encode, fcb)
        self.encoders["json"] = (self.text_encode, fcb)
        self.encoders["jsonb"] = (self.jsonb_encode, fcb)
        self.encoders["int4range"] = (self.int4range_encode, fcb)
        self.encoders["int8range"] = (self.int8range_encode, fcb)
        self.encoders["oid"] = (self.oid_encode, fcb)
        self.encoders["xid"] = (self.xid_encode, fcb)
        self.encoders["hstore"] = (self.hstore_encode, fct)
        self._reintern_keys()

    def _reintern_keys(self):
        for key in self.encoders:
            self.encoders[intern(key)] = self.encoders[key]

    def timestamps_are_integers(self):
        """
        Once you connect and authorize successfully, Postgres will send a
        bunch of PARAMETER_STATUS messages. One of these will be the
        integer_datetimes message, which by default should be 'on'.

        Your code should call this method when it receives that message.
        """
        self.encoders.update({
            "timestamp": (self.timestamp_encode_integer, self.FC_BINARY,),
            "timestamptz": (self.timestamptz_encode_integer, self.FC_BINARY,),
            "interval": (self.interval_encode_integer, self.FC_BINARY,),
            "time": (self.time_encode_int, self.FC_BINARY,),
            "timetz": (self.timetz_encode_int, self.FC_BINARY)
        })
        self._reintern_keys()

    def timestamps_are_floats(self):
        """
        Once you connect and authorize successfully, Postgres will send a
        bunch of PARAMETER_STATUS messages. One of these will be the
        integer_datetimes message, which by default should be 'on'.

        Your code should call this method when it receives that message.

        msg = "Without a good way to test this right now, I'm going to " \
              "mark it unimplemented"
        raise NotImplementedError(msg)
        """
        self.encoders.update({
            "timestamp": (self.timestamp_encode_float, self.FC_BINARY,),
            "timestamptz": (self.timestamptz_encode_float, self.FC_BINARY,),
            "interval": (self.interval_encode_float, self.FC_BINARY,),
            "time": (self.time_encode_float, self.FC_BINARY,),
            "timetz": (self.timetz_encode_float, self.FC_BINARY)
        })
        self._reintern_keys()

    def numerics_are_text(self):
        """
        Call this method on your instance so it will encode numeric data as
        text, which is normally advisable.
        """
        self.encoders["numeric"] = (self.numeric_encode_text, self.FC_TEXT,)
        self._reintern_keys()

    def numerics_are_binary(self):
        """
        Call this method on your instance so it will encode numeric data as
        binary, which is not advisable. The binary format for numerics is
        arcane and Thrum's codecs are inefficient and not well tested
        (i.e. possibly buggy).
        """
        self.encoders["numeric"] = (self.numeric_encode_binary, self.FC_BINARY)
        self._reintern_keys()

    def add_enum_type(self, typname):
        """
        If your database includes any enum types, you can call this method to
        have the encoder register them. You'll also need to update self.oids
        for this to work. See the addprop method on that class.

        @param typname: Name of the new enum type
        @type typname: L{str}
        """
        self.encoders[typname] = (self.text_encode, self.FC_BINARY)
        self._reintern_keys()

        def array_enum_encode(self, value):
            return self.array_encode(value, typname)

        array_enum_encode.__doc__ = \
            """Encode an array of enums of typname '%s'""" % (typname,)
        setattr(self, "array_%s_encode" % (typname,), array_enum_encode)

    def cstring_encode(self, value):
        """
        Encode bytes data as a null-terminated cstring

        @param value: Data to encode, which can be bytes or str
        @type value: L{bytes}

        @return: Binary encoded data
        @rtype: L{bytes}
        """
        if type(value) == six.text_type:
            rval = value.encode(self.uclient_encoding)
        elif type(value) == six.binary_type:
            rval = value
        else:
            rval = six.text_type(value).encode(self.client_encoding)

        if rval[-1] == self.null_byte:
            return rval
        return rval + self.null_byte

    def money_encode(self, value):
        """
        Encode 'money' value to bytes.

        @param value: Money value in the form (dollars, cents)
        @type value: L{tuple}

        @return: An int8 encoded as 8 bytes
        @rtype: L{bytes}
        """
        try:
            dollars, cents = value
            value = (dollars * 100) + cents
        except Exception:
            if isinstance(value, decimal.Decimal):
                value = value * 100
            elif isinstance(value, float):
                value = value * 100

        return binary.q_pack(value)

    def macaddr_encode(self, value):
        """
        Encode macaddr value to bytes.

        @param value: Mac address, e.g. '02:28:1b:b5:14:a8'
        @type value: L{str}

        @return: 6 unsigned 1-byte ints encoded as 6 bytes
        @rtype: L{bytes}
        """
        try:
            fragments = self.macaddr_regex.split(value.lstrip()[:17])
            a, b, c, d, e, f = [int(fragment, 16) for fragment in fragments]
        except Exception:
            a, b, c, d, e, f = [int(fragment, 16) for fragment in
                                [value[x:x+2] for x in
                                range(0, len(value), 2)]
                                ]

        return binary.BBBBBB_pack(a, b, c, d, e, f)

    def bit_encode(self, bit):
        """
        Encode a string of zeros and ones to bytes.

        @param value: bit data, e.g. '10010011'
        @type value: L{str}

        @return: Binary encoded data
        @rtype: L{bytes}
        """
        rval = [binary.i_pack(len(bit))]

        chunk_len = 8
        chunks = [bit[i:i+chunk_len] for i in range(0, len(bit), chunk_len)]

        modulo = len(chunks[-1]) % chunk_len
        if modulo:
            zeroes = chunk_len - modulo
            chunks[-1] = chunks[-1] + (b"0" * zeroes)

        for chunk in chunks:
            rval.append(binary.B_pack(int(chunk, 2)))

        return b''.join(rval)

    def date_encode(self, date):
        """
        Convert a datetime.date into a binary packed string representation
        of a 32-bit integer representing days since 2000-01-01

        @param date: A datetime.date
        @type date: L{datetime.datetime}

        @return: An int4 packed into 4 bytes
        @rtype: L{bytes}
        """

        try:
            return binary.i_pack(date.toordinal() - self.epoch_ordinal)
        except AttributeError:
            numbers = [int(item) for item in self.datesplit_regex.split(date)]
            year = numbers[self.index_year]
            month = numbers[self.index_month]
            day = numbers[self.index_day]
            datetime_ordinal = datetime.date(year, month, day).toordinal()
            return binary.i_pack(datetime_ordinal - self.epoch_ordinal)

    def daterange_encode(self, daterange):
        raise NotImplementedError("daterange_encode")

    def timetz_encode_float(self, value):
        """
        The return value is a tuple of (time, offset), where time is an
        integer offset in microseconds since midnight, and the offset
        represents seconds west of UTC, so where Pacific Standard Time is
        UTC-8, an implementation might supply:
        (0, 60*60*8)
        Moscow Standard Time, where it is UTC+3 might appear as:
        (0, 60*60*-3)

        @param value: A datetime.time with timezone data
        @type value: L{datetime.time}

        @return: A double and an int4 packed into 12 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.di_pack(*value)
        except Exception:
            return binary.di_pack(*self._timetz_encode(value))

    def timetz_encode_int(self, value):
        """
        The return value is a tuple of (time, offset), where time is an
        integer offset in microseconds since midnight, and the offset
        represents seconds west of UTC, so where Pacific Standard Time is
        UTC-8, an implementation might supply:
        (0, 60*60*8)
        Moscow Standard Time, where it is UTC+3 might appear as:
        (0, 60*60*-3)

        @param value: A datetime.time with timezone data
        @type value: L{datetime.time}

        @return: An int8 and an int4 packed into 12 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.qi_pack(*value)
        except Exception:
            return binary.qi_pack(*self._timetz_encode(value))

    def _timetz_encode(self, value):
        """
        Given a datetime.time with timezone data, return a tuple of
        microseconds since midnight, and seconds west of UTC

        @param value: A datetime.time with timezone data
        @type value: L{datetime.time}

        @return: A tuple of (micros, offset)
        @rtype: L{tuple}
        """
        try:
            offset = int(round(value.tzinfo.utcoffset(None).total_seconds()))
        except Exception:
            offset = 0

        micros = (3600000000 * value.hour) + \
            (60000000 * value.minute) + \
            (1000000 * value.second) + \
            value.microsecond

        return micros, offset

    def time_encode_int(self, value):
        """
        Given a datetime.time, return binary-encoded long representing
        microseconds since midnight. See _time_encode for accepted args

        @param value: A datetime.time or a string, or a tuple.
        @type value: L{datetime.time}

        @return: Binary encoded data: An int8
        @rtype: L{bytes}
        """
        try:
            return binary.q_pack(value)
        except Exception:
            return binary.q_pack(self._time_encode(value))

    def _time_encode(self, value):
        """
        Given a datetime.time, return an int representing microseconds since
        midnight. Also accepts tuples:
            (hour, minute, second, microsecond,)
            (hour, minute, second,)
            (hour, minute,)
        Or a string:
            11:10:52
            11:20

        @param value: A datetime.time
        @type value: L{datetime.time}

        @return:  An int representing microseconds since midnight
        @rtype: L{int}
        """
        try:
            # Check value.upper to make sure we have a string
            value.upper
            components = self.timesplit_regex.split(value+":00")[0:3]
            try:
                hour, minute, second = components
            except ValueError:
                msg = "Bad value '%s' for field type 'time'" % (value,)
                raise ValueError(msg)

            hour = int(hour)
            minute = int(minute)
            second = int(second)
            microsecond = 0
        except AttributeError:
            try:
                hour, minute, second, microsecond = value
            except ValueError:
                second = microsecond = 0
                try:
                    hour, minute, second = value
                except ValueError:
                    hour, minute = value
            except TypeError:
                hour, minute = value.hour, value.minute
                second, microsecond = value.second, value.microsecond

        micros = (3600000000 * hour) + \
            (60000000 * minute) + \
            (1000000 * second) + \
            microsecond
        return micros

    def time_encode_float(self, value):
        """
        Given a datetime.time, return binary-encoded double representing
        microseconds since midnight. See _time_encode for accepted args

        @param value: A datetime.time
        @type value: L{datetime.time}

        @return:  A double representing microseconds since midnight
        @rtype: L{bytes}
        """
        return binary.d_pack(self._time_encode(value))

    def timestamp_encode_integer(self, timestamp):
        """
        Convert a datetime.datetime into a binary packed string representation
        of a 64-bit integer representing microseconds since 2000-01-01

        @param timestamp: A timestamp
        @type timestamp: L{datetime.datetime}

        @return: A Long, binary packed into eight bytes
        @rtype: L{bytes}
        """
        if timestamp == datetime.datetime.max:
            micros = self.infinity_microseconds
        elif timestamp == datetime.datetime.min:
            micros = self.minus_infinity_microseconds
        else:
            unixtime = calendar.timegm(timestamp.timetuple())
            secs = unixtime - self.base_epoch_seconds
            micros = int(secs * 1e6) + timestamp.microsecond
        return binary.q_pack(micros)

    def timestamp_encode_float(self, timestamp):
        """
        Convert a datetime.datetime into a binary packed string representation
        of a double-precision float representing seconds since 2000-01-01

        @param timestamp: A timestamp
        @type timestamp: L{datetime.datetime}

        @return: A Double, binary packed into eight bytes
        @rtype: L{bytes}
        """
        unixtime = calendar.timegm(timestamp.timetuple())
        fraction_of_second = timestamp.microsecond / 1e6
        val = unixtime + fraction_of_second - self.base_epoch_seconds
        return binary.d_pack(val)

    def timestamptz_encode_integer(self, timestamp):
        """
        Convert a datetime.datetime into a binary packed string representation
        of a 64-bit integer representing microseconds since 2000-01-01
        If the timestamp has timezone info, convert it to UTC.

        @param timestamp: A timestamp with timezone info
        @type timestamp: L{datetime.datetime}

        @return: A Long, binary packed into eight bytes
        @rtype: L{bytes}
        """
        _datetime = timestamp.astimezone(self.utc).replace(tzinfo=None)
        return self.timestamp_encode_integer(_datetime)

    def timestamptz_encode_float(self, timestamp):
        """
        Convert a datetime.datetime into a binary packed string representation
        of a double-precision float representing seconds since 2000-01-01
        If the timestamp has timezone info, convert it to UTC.

        @param timestamp: A timestamp with timezone info
        @type timestamp: L{datetime.datetime}

        @return: A Double, binary packed into eight bytes
        @rtype: L{bytes}
        """
        _datetime = timestamp.astimezone(self.utc).replace(tzinfo=None)
        return self.timestamp_encode_float(_datetime)

    def interval_encode_integer(self, intrvl):
        """
        Convert an Interval object into three binary-packed Integers
        representing microseconds, days and months

        @param intrvl: A thrum.interval.Interval object
        @type intrvl: L{thrum.interval.Interval}

        @return: An int8 and two int4s, binary packed into 16 bytes
        @rtype: L{bytes}
        """
        microseconds = intrvl.microseconds
        try:
            microseconds += int(intrvl.seconds * 1e6)
        except AttributeError:
            pass

        try:
            months = intrvl.months
        except AttributeError:
            months = 0

        return binary.qii_pack(microseconds, intrvl.days, months)

    def interval_encode_float(self, intrvl):
        """
        Convert an Interval object into a binary-packed Double and two
        binary-packed integers representing seconds, days and months

        @param intrvl: A thrum.interval.Interval object
        @type intrvl: L{thrum.interval.Interval}

        @return: A double and two floats, binary packed into 16 bytes
        @rtype: L{bytes}
        """
        seconds = intrvl.microseconds / 1000.0 / 1000.0
        try:
            seconds += intrvl.seconds
        except AttributeError:
            pass

        try:
            months = intrvl.months
        except AttributeError:
            months = 0

        return binary.dii_pack(seconds, intrvl.days, months)

    def text_encode(self, data):
        """
        Return data encoded in current client encoding

        @param data: A string or unicode value
        @type data: L{bytes}

        @return: Bytes
        @rtype: L{bytes}
        """
        typ = type(data)
        if typ is bytes:
            return data
        if typ is six.text_type:
            return data.encode(self.uclient_encoding)
        return six.text_type(data).encode(self.uclient_encoding)

    def tsvector_encode(self, data):
        """
        Breaks text data into words, discarding whitespace and punctuation.
        Creates a TsVector instance with the words, which encodes the data
        into the Postgres binary wire format for the tsvector data type.

        @param data: A string or TsVector value
        @type data: L{str}

        @return: Bytes
        @rtype: L{bytes}
        """
        if isinstance(data, tsvector.TsVector):
            return self.tsvector_encode_object(data)

        tsv = tsvector.TsVector()
        tsv.load_words_file()
        words = data.split()
        weights = ('A', 'B', 'C', 'D')

        for index, word in enumerate(words):
            word, _, pos, weight = self.tsvector_regex.search(word).groups()
            if not pos:
                pos = None
            else:
                try:
                    pos = int(pos)
                except Exception as e:
                    print(e)
                    pos = None
            if not weight:
                weight = b'D'
            elif weight not in weights:
                weight = None
            tsv.normalize_and_add(word, position=pos, weight=weight)

        return self.tsvector_encode_object(tsv)

    def tsvector_encode_object(self, tsv):
        """
        Given a TsVector object, returns bytes representing the Postgres binary
        wire format for the tsvector data type.
        """
        weights = (b'D', b'C', b'B', b'A')

        rval = BytesBuilder()
        rval.append(binary.i_pack(len(tsv)))

        for entry in tsv:
            npositions = len(entry.positions)
            rval.append(entry.text.encode(self.client_encoding) +
                        self.null_byte + binary.H_pack(npositions))
            for position, weight in entry.positions:
                rval.append(binary.H_pack(min(position, 16383) +
                            (weights.index(weight) << 14)))

        return rval.build()

    def point_encode(self, point):
        """
        Given a point (either an (x,y) tuple or an object with .x and .y
        attributes), return the Postgres binary wire format bytes for the
        point data type

        @param point: A point or tuple
        @type point: L{tuple}

        @return: Two doubles encoded in 16 bytes
        @rtype: L{bytes}
        """
        try:
            x = point.x
            y = point.y
        except Exception:
            x, y = point

        return binary.dd_pack(x, y)

    def line_segment_encode(self, line_segment):
        """
        Given a line segment described as a tuple, return the Postgres binary
        wire format bytes for the lseg type. Acceptable tuples include:
        (x1, y1, x2, y2)
        ((x1, y1), (x2, y2))
        """
        try:
            x1, y1, x2, y2 = line_segment
        except ValueError:
            p1, p2 = line_segment
            x1, y1 = p1
            x2, y2 = p2

        return binary.dddd_pack(x1, y1, x2, y2)

    def line_encode(self, line):
        """
        Given a line of infinite length described as a tuple, return the
        Postgres binary wire format bytes for the line type. Acceptable tuples
        include:
        (a, b, c)

        The Posgres docs note that lines are represented by the linear equation
        Ax + By + C = 0,
        where A and B are not both zero. I hope that means more to you than it
        does to me.

        @param line: A tuple
        @type line: L{tuple}

        @return: Three doubles encoded in 24 bytes
        @rtype: L{bytes}
        """
        a, b, c = line
        return binary.ddd_pack(a, b, c)

    def circle_encode(self, circle):
        """
        Given a circle ((x, y), radius) described as a tuple, return the
        Postgres binary wire format bytes for the line type. Acceptable tuples
        include:
        ((x, y), radius)

        @param circle: A tuple
        @type circle: L{tuple}

        @return: Three doubles encoded in 24 bytes
        @rtype: L{bytes}
        """
        ((x, y), r) = circle
        return binary.ddd_pack(x, y, r)

    def path_encode(self, path):
        """
        Given a path described as a list OR a thrum.geometry.Path object,
        return the Postgres binary wire format bytes for the path type.

        The argument should be a list of (x, y) points or a similar Path
        object. The default for 'closed' is 0.

        @param path: A list or thrum.geometry.Path
        @type path: L{list}

        @return: Variable length bytes
        @rtype: L{bytes}
        """
        try:
            closed = path.closed
        except AttributeError:
            closed = 0

        buff = [binary.Bi_pack(closed, len(path))]
        buff.extend([binary.dd_pack(x, y) for x, y in path])

        return b''.join(buff)

    def box_encode(self, box):
        """
        Given a box described as a tuple, return the Postgres binary wire
        format bytes for the box type. The tuple may have one of these forms:
        (x1, y1, x2, y2)
        ((x1, y1), (x2, y2))

        @param box: A tuple or nested tuples
        @type box: L{tuple}

        @return: Four doubles encoded as 32 bytes
        @rtype: L{bytes}
        """
        try:
            x1, y1, x2, y2 = box
        except ValueError:
            p1, p2 = box
            x1, y1 = p1
            x2, y2 = p2

        return binary.dddd_pack(x1, x2, y1, y2)

    def polygon_encode(self, polygon):
        """
        Given a polygon described as a list, return the Postgres binary wire
        format bytes for the polygon type. The argument should be a list of
        (x, y) points.

        @param polygon: A list of (x, y) points
        @type polygon: L{list}

        @return: Variable length bytes
        @rtype: L{bytes}
        """
        buff = [binary.i_pack(len(polygon))]
        for point in polygon:
            x, y = point
            buff.append(binary.dd_pack(x, y))
        return b''.join(buff)

    def cidr_encode(self, cidr):
        """
        Classless Internet Domain Routing
        The cidr argument may be one of the following, or a string that can be
        passed into ipaddress.ip_network
        ipaddress.IPv4Network
        ipaddress.IPv6Network

        @param cidr: A string or ipaddress.IPv*Network
        @type cidr: L{str}

        @return: Variable length bytes
        @rtype: L{bytes}
        """

        try:
            cidr.subnets
            # cidr is an ip_network
            network = cidr
        except AttributeError:
            # cidr is a string
            network = ipaddress.ip_network(six.text_type(cidr))

        family = constants.PGSQL_AF_INET
        if isinstance(network, ipaddress.IPv6Network):
            family = constants.PGSQL_AF_INET6

        addrlen = len(network.network_address.packed)

        IS_CIDR = 1
        packed_metadata = binary.BBBB_pack(family, network.prefixlen, IS_CIDR,
                                           addrlen)
        return packed_metadata + network.network_address.packed

    def unknown_out(self, data):
        """
        Return unknown data cast as string and encoded in current client
        encoding

        @param data: Some data
        @type data: C{unknown}

        @return: Bytes
        @rtype: L{bytes}
        """
        return six.text_type(data).encode(self.uclient_encoding)

    def bytea_encode(self, data):
        """
        Return byte data, unmodified

        @param data: Some data
        @type data: C{unknown}

        @return: Bytes
        @rtype: L{bytes}
        """
        return data

    def numeric_encode_text(self, number):
        """
        Cast a number to text, encode it and return bytes

        @param number: A decimal.Decimal, float, or int
        @type number: C{decimal.Decimal}

        @return: Bytes
        @rtype: L{bytes}
        """
        return str(number).encode(self.uclient_encoding)

    def _numeric_expand_tail(self, quad):
        """
        Given a tuple of digits, convert the last 4 into an integer and encode
        the result as an int2

        @param quad: A tuple of 4 digits, e.g. (9, 7, 2, 6)
        @type quad: C{tuple}

        @return: Bytes
        @rtype: L{bytes}
        """
        total = quad[-1]
        try:
            total += quad[-2] * 10
            total += quad[-3] * 100
            total += quad[-4] * 1000
        except IndexError:
            pass
        return binary.h_pack(total)

    def _numeric_expand_head(self, quad):
        """
        Given a tuple of digits, convert the last 4 into an integer and encode
        the result as an int2

        @param quad: A tuple of 4 digits, e.g. (9, 7, 2, 6)
        @type quad: C{tuple}

        @return: Bytes
        @rtype: L{bytes}
        """
        total = quad[0] * 1000
        try:
            total += quad[1] * 100
            total += quad[2] * 10
            total += quad[3]
        except IndexError:
            pass
        return binary.h_pack(total)

    def numeric_encode_binary(self, number):
        """
        Given a Decimal, return the Postgres binary wire format bytes for the
        numeric type. If a float or int is supplied, we cast it to a Decimal
        before proceeding.

        @param number: A Decimal
        @type number: C{decimal.Decimal}

        @return: Bytes
        @rtype: L{bytes}
        """

        if not type(number) == decimal.Decimal:
            number = decimal.Decimal(number)

        sign, digits, exponent = number.as_tuple()

        if number.is_nan():
            sign = binary.H_pack(constants.NUMERIC_NAN)
            return b''.join((binary.h_pack(0),
                             binary.h_pack(0),
                             sign,
                             binary.h_pack(0)))

        if sign == 1:
            sign = constants.NUMERIC_NEG
        else:
            sign = constants.NUMERIC_POS

        dscale = exponent * -1
        """
        If the argument was Decimal('0.1234e-6') Then we'll probably get
        a tuple like this:
        (0, (1, 2, 3, 4), -10)
        We will need to pad our digits tuple with zeros to get PG to
        store the value correctly. I know we SHOULDN'T have to do that,
        but I can't find another way.
        """
        missing_zeros = dscale - len(digits)
        if missing_zeros:
            digits = ((0,) * missing_zeros) + digits

        if exponent == 0:
            head, tail = digits, ""
        else:
            head, tail = digits[0:exponent], digits[exponent:]

        weight = int(math.ceil(len(head) / 4.0)) - 1
        ndigits = int(math.ceil(len(tail) / 4.0) + weight + 1)

        bb = BytesBuilder()
        bb.append(binary.hhHh_pack(ndigits, weight, sign, dscale))

        lh = len(head)
        lhr = lh % 4

        if lhr:
            bb.append(self._numeric_expand_tail(head[0:lhr]))

        for index in range(lhr, lh, 4):
            bb.append(self._numeric_expand_tail(head[index:index+4]))

        for index in range(0, len(tail), 4):
            bb.append(self._numeric_expand_head(tail[index:index+4]))

        return bb.build()

    def jsonb_encode(self, data):
        """
        Given some data, return the Postgres binary wire format bytes for the
        jsonb type.

        @param number: A dict, list, tuple or string
        @type number: C{dict}

        @return: Bytes
        @rtype: L{bytes}
        """
        prefix = b'\x01'
        return prefix + (json.dumps(data).encode("utf-8"))

    def json_encode(self, data):
        """
        Given some data, return JSON encoded text converted to bytes

        @param number: A dict, list, tuple or string
        @type number: C{dict}

        @return: Bytes
        @rtype: L{bytes}
        """
        return json.dumps(data, encoding=self.client_encoding)

    def uuid_encode(self, data):
        """
        Given a UUID, return a bytes representation of it

        @param data: A UUID
        @type data: L{uuid.UUID}

        @return: Bytes
        @rtype: L{bytes}
        """
        try:
            return data.bytes
        except AttributeError:
            return uuid.UUID(data).bytes

    def inet_encode(self, inet):
        """
        Classless Internet Domain Routing
        The cidr argument may be one of the following, or a string that can be
        passed into ipaddress.ip_network
        ipaddress.IPv4Network
        ipaddress.IPv6Network
        ipaddress.IPv4Address
        ipaddress.IPv6Address

        @param inet: A string or ipaddress.IPv*Network or ipaddress.IPv*Address
        @type inet: L{str}

        @return: Variable length bytes
        @rtype: L{bytes}
        """
        try:
            inet.subnets
            # inet is already an ipaddress object
            network = inet
        except AttributeError:
            # inet is a string
            network = ipaddress.ip_network(six.text_type(inet))

        family = constants.PGSQL_AF_INET
        IS_CIDR = 1
        if isinstance(network, ipaddress.IPv6Network):
            family = constants.PGSQL_AF_INET6
            if network.prefixlen == 128:
                # IPv6 network with prefixlen of 128 is a discrete IP address
                IS_CIDR = 0
        elif network.prefixlen == 32:
            # IPv4 network with prefixlen of 32 is a discrete IP address
            IS_CIDR = 0

        addrlen = len(network.network_address.packed)
        packed_metadata = binary.BBBB_pack(family, network.prefixlen, IS_CIDR,
                                           addrlen)
        return packed_metadata + network.network_address.packed

    def bool_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        bool type.

        @param value: True or False
        @type value: C{dict}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.true if value else self.false

    def array_uuid_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        array_uuid array type.

        @param value: A list of UUIDs
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "uuid")

    def array_varbit_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        varbit (variable length bit string) array type.

        @param value: A list of bit strings (e.g. ['10011', '11'])
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "varbit")

    def array_bit_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        bit (bit string) array type.

        @param value: A list of bit strings (e.g. ['1011', '1010'])
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "bit")

    def array_date_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        date array type.

        @param value: A list of dates
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "date")

    def array_timestamp_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        timestamp array type.

        @param value: A list of datetime.datetime
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "timestamp")

    def array_timestamptz_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        timestamptz array type.

        @param value: A list of datetime.datetime with timezone info
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "timestamptz")

    def array_interval_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        interval array type.

        @param value: A list of thrum.interval.Interval
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "interval")

    def array_timetz_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        timetz array type.

        @param value: A list of datetime.time with timezone info
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "timetz")

    def array_time_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        time array type.

        @param value: A list of datetime.time
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "time")

    def array_numeric_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        numeric array type.

        @param value: A list of decimal.Decimal
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "numeric")

    def array_int2_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        int2 array type.

        @param value: A list of integers
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "int2")

    def array_int4_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        int4 array type.

        @param value: A list of integers
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "int4")

    def array_int8_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        int8 array type.

        @param value: A list of integers
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "int8")

    def array_float4_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        float4 array type.

        @param value: A list of floats
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "float4")

    def array_float8_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        float8 array type.

        @param value: A list of floats
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "float8")

    def array_oid_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        oid array type.

        @param value: A list of ints
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "oid")

    def array_xid_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        xid array type.

        @param value: A list of ints
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "xid")

    def array_xml_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        xml array type.

        @param value: A list of strings
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "xml")

    def array_text_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        text array type.

        @param value: A list of strings
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "text")

    def array_inet_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        inet array type.

        @param value: A list of strings or ipaddress objects
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "inet")

    def array_macaddr_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        macaddr array type.

        @param value: A list of strings
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "macaddr")

    def array_varchar_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        varchar array type.

        @param value: A list of strings
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "varchar")

    def array_bpchar_encode(self, array):
        # For this array type, postgresql seems to want a blank-padded char,
        # (bpchar) rather than just a char. Which is odd.
        """
        Given a value, return the Postgres binary wire format bytes for the
        bpchar array type.

        @param value: A list of strings
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "bpchar")

    def array_jsonb_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        jsonb array type.

        @param value: A list of dicts, lists, tuples, strings, ints or floats
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "jsonb", expand=False)

    def array_json_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        json array type.

        @param value: A list of dicts, lists, tuples, strings, ints or floats
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "json", expand=False)

    def array_line_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        line array type.

        @param value: A list of tuples, each of three floats
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "line", expand=False)

    def array_box_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        box array type.

        @param value: A tuple of two tuples, ((x1, y1), (x2, y2))
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "box", expand=False)

    def array_polygon_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        polygon array type.

        @param value: A list of lists of (x, y) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "polygon", expand=False)

    def array_path_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        path array type.

        @param value: A list of lists of (x, y) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "path", expand=False)

    def array_lseg_encode(self, value):
        """
        Given a value, return the Postgres binary wire format bytes for the
        lseg array type.

        @param value: A list of tuples of (x1, y1, x2, y2)
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(value, "lseg", expand=False)

    def array_point_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        point array type.

        @param value: A list of (x, y) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "point", expand=False)

    def array_money_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        money array type.

        @param value: A list of (dollars, cents) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "money", expand=False)

    def array_circle_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        circle array type.

        @param value: A list of ((x, y), radius) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "circle", expand=False)

    def array_cidr_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        cidr array type.

        @param value: A list of ((x, y), radius) tuples
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "cidr")

    def array_bool_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        bool array type.

        @param value: A list of bools
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "bool")

    def array_bytea_encode(self, array):
        """
        Given a value, return the Postgres binary wire format bytes for the
        bytea array type.

        @param value: A list of bytes data
        @type value: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        return self.array_encode(array, "bytea")

    def array_encode(self, array, typname, expand=True):
        """
        Given a value and a postgres typname, return the Postgres binary wire
        format bytes for an array of that type. If the type is itself a
        scalar, for example a geometric point (which is a tuple of x,y
        co-ordinates), then specify expand=False, otherwise the data will be
        interpreted as a nested array.

        @param array: A list of some data type
        @type array: C{list}

        @param typname: The name of the postgres data type, e.g. varchar
        @type typname: C{list}

        @return: Bytes
        @rtype: L{bytes}
        """
        buff, dim_lengths, nulls = self._array_encode(array, typname, buff=[],
                                                      expand=expand)
        has_null = nulls["value"]

        oid = self.oids.types[typname]
        data = bytearray(binary.iii_pack(len(dim_lengths), has_null, oid))
        for i in dim_lengths.values():
            data.extend(binary.ii_pack(i, 1))

        data.extend(b''.join(buff))

        return bytes(data)

    def _array_encode(self, array, typname, buff, dim_lengths=None, ndims=0,
                      nulls=None, expand=True):
        """
        Recursively iterate over the data in array, encoding the content to
        bytes and appending it to the buff list.
        """
        dim_lengths = dim_lengths or {}
        nulls = nulls or {"value": False}

        len_array = len(array)
        dim_lengths.setdefault(ndims, len_array)
        if dim_lengths[ndims] != len_array:
            raise errors.ArrayDimensionsNotConsistentError(
                    "array dimensions not consistent")

        for index, item in enumerate(array):
            if expand and isinstance(item, (list, tuple)):
                self._array_encode(item, typname, buff, dim_lengths, ndims+1,
                                   nulls, expand=True)
            elif item is None:
                buff.append(binary.i_pack(-1))
                nulls["value"] = True
            else:
                pack, _ = self.encoders[typname]
                inner_data = pack(item)
                buff.extend((binary.i_pack(len(inner_data)), inner_data))

        return buff, dim_lengths, nulls

    def int2_encode(self, value):
        """
        Given a int, return the Postgres binary wire format bytes for the
        int2 type

        @param value: An int
        @type value: C{int}

        @return: An int2 encoded as 2 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.h_pack(value)
        except binary.error:
            if value < constants.MIN_INT2:
                msg = "Value '%s' is less than minimum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MIN_INT2,))
            if value > constants.MAX_INT2:
                msg = "Value '%s' is greater than maximum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MAX_INT2,))
            raise

    def int4_encode(self, value):
        """
        Given a int, return the Postgres binary wire format bytes for the
        int4 type

        @param value: An int
        @type value: C{int}

        @return: An int4 encoded as 4 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.i_pack(value)
        except binary.error:
            if value < constants.MIN_INT4:
                msg = "Value '%s' is less than minimum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MIN_INT4,))
            if value > constants.MAX_INT4:
                msg = "Value '%s' is greater than maximum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MAX_INT4,))
            raise

    def oid_encode(self, value):
        """
        Given an int greater than zero, return the Postgres binary wire format
        bytes for the oid type

        @param value: An int
        @type value: C{int}

        @return: An uint4 encoded as 4 bytes
        @rtype: L{bytes}
        """
        return self.uint4_encode(value)

    def xid_encode(self, value):
        """
        Given an int greater than zero, return the Postgres binary wire format
        bytes for the xid type

        @param value: An int
        @type value: C{int}

        @return: An uint4 encoded as 4 bytes
        @rtype: L{bytes}
        """
        return self.uint4_encode(value)

    def uint4_encode(self, value):
        """
        Given an int greater than zero, return the Postgres binary wire format
        bytes for the xid type

        @param value: An int
        @type value: C{int}

        @return: An uint4 encoded as 4 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.I_pack(value)
        except binary.error:
            if value < constants.MIN_UINT4:
                msg = "Value '%s' is less than minimum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MIN_UINT4,))
            if value > constants.MAX_UINT4:
                msg = "Value '%s' is greater than maximum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MAX_UINT4,))
            raise

    def int8_encode(self, value):
        """
        Given a int, return the Postgres binary wire format bytes for the
        int8 type

        @param value: An int
        @type value: C{int}

        @return: An int8 encoded as 8 bytes
        @rtype: L{bytes}
        """
        try:
            return binary.q_pack(value)
        except binary.error:
            if value < constants.MIN_INT8:
                msg = "Value '%s' is less than minimum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MIN_INT8,))
            if value > constants.MAX_INT8:
                msg = "Value '%s' is greater than maximum allowed value '%s'"
                raise errors.ThrumEncodeError(msg % (value,
                                              constants.MAX_INT8,))
            raise

    def int4range_encode(self, value):
        """
        Given a tuple of two values, return the Postgres binary wire format
        bytes for the int4range type

        @param value: A tuple of (start, end) where start & end are int or None
        @type value: C{tuple}

        @return: An int4range encoded as 1-17 bytes
        @rtype: L{bytes}
        """
        start, end = value
        if start is None and end is None:
            return binary.b_pack(24)
        elif start is None:
            return binary.bii_pack(8, 4, end)
        elif end is None:
            return binary.bii_pack(18, 4, start)
        return binary.biiii_pack(2, 4, start, 4, end)

    def int8range_encode(self, value):
        """
        Given a tuple of two values, return the Postgres binary wire format
        bytes for the int8range type

        @param value: A tuple of (start, end) where start & end are int or None
        @type value: C{tuple}

        @return: An int8range encoded as 1-25 bytes
        @rtype: L{bytes}
        """
        start, end = value
        if start is None and end is None:
            return binary.b_pack(24)
        elif start is None:
            return binary.biq_pack(8, 8, end)
        elif end is None:
            return binary.biq_pack(18, 8, start)
        return binary.biqiq_pack(2, 8, start, 8, end)

    def hstore_encode(self, value):
        """
        Given a dict return the Postgres text wire format bytes for the
        hstore type

        @param value: A dict of key/value pairs. Key and value must be strings
        @type value: C{dict}

        @return: Bytes
        @rtype: L{bytes}
        """
        if pghstore is None:
            msg = "pghstore could not be imported. Try 'pip install pghstore'"
            raise Exception(msg)
        try:
            # raise Exception("WHAT IS THIS")
            rval = pghstore.dumps(value).encode(self.client_encoding)
            return rval
            print(rval)
            return binary.i_pack(len(value)+1) + rval + self.null_byte
        except Exception:
            raise
