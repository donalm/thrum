#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

from . import constants
from . import errors
from . import binary


class StartupMessage(object):

    @classmethod
    def construct(cls, database, user,
                  protocol_version=None,
                  client_encoding="utf-8",
                  application_name=None,
                  **kwargs):
        """
        @param database: Database name. Either a utf8 string or bytes
        @type database: C{bytes|utf8}

        @param user: Username. Either a utf8 string or bytes
        @type user: C{bytes|utf8}

        @param protocol_version: Protocol version. Normally 3
        @type protocol_version: C{int4}

        @param client_encoding: Is this ever not utf-8?
        @type client_encoding: C{bytes}

        @param application_name: A descriptive name for this connection
        @type application_name: C{bytes|utf8}

        @param kwargs: Additional key/value args to add to the startup message
        @type kwargs: C{dict}

        @return: A startup message encoded as bytes
        @rtype: L{bytes}
        """

        if application_name is None:
            application_name = constants.APPLICATION_NAME

        protocol_version = protocol_version or \
            constants.DEFAULT_PROTOCOL_VERSION

        message = binary.i_pack(protocol_version)
        message += cls.make_argument("user", user)

        if database is not None:
            message += cls.make_argument("database", database)

        if application_name:
            message += cls.make_argument("application_name", application_name)

        for key, value in kwargs.items():
            message += cls.make_argument(key, value)

        # End of message
        message += constants.NULL_BYTE

        message_length = len(message)

        # Add four bytes to store the encoded message length value itself
        message_length += 4

        # The message is the l
        return bytes(binary.i_pack(message_length) + message)

    @classmethod
    def make_argument(cls, key, value):
        """
        @param key: Key
        @type key: C{bytes|utf8}

        @param value: Value
        @type value: C{bytes|utf8}

        @return: A key/value pair encoded as bytes
        @rtype: L{bytes}
        """

        if isinstance(key, str):
            key = key.encode('utf8')

        if not isinstance(key, bytes):
            raise errors.ThrumError(
                "Expected bytes, got '%s' for startup "
                "message key '%s'" % (type(key), key,)
            )

        if isinstance(value, str):
            value = value.encode('utf8')

        if not isinstance(value, bytes):
            raise errors.ThrumError(
                "Expected bytes, got '%s' for startup "
                "message value '%s'" % (type(value), value,)
            )

        return key + constants.NULL_BYTE + value + constants.NULL_BYTE
