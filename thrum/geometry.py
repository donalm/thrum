#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.


class Path(list):
    def __init__(self, closed, *args, **kwargs):
        list.__init__(self, *args, **kwargs)
        self.closed = bool(closed)

    def __eq__(self, other):
        if not hasattr(other, 'closed'):
            return False
        if self.closed != other.closed:
            return False
        return list.__eq__(self, other)

    def __repr__(self):
        parent_repr = list.__repr__(self)
        return "<Path(%s, %s)>" % (self.closed, parent_repr,)
