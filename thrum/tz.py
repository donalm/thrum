#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

import datetime


class PgTimeZone(datetime.tzinfo):
    def __init__(self, seconds_west_of_UTC):
        datetime.tzinfo.__init__(self)
        self._seconds_west_of_UTC = seconds_west_of_UTC
        self._delta = datetime.timedelta(seconds_west_of_UTC * -1)

    def utcoffset(self, dt):
        return self._delta

    def dst(self, dt):
        return self._delta
