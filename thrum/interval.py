#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

from __future__ import division, absolute_import

import math

class Interval(object):
    """
    PostgreSQL intervals are defined with attributes for months, days, and
    microseconds. These attributes shouldn't be converted to each other,
    since months can have varying numbers of days, for e.g.

    @type microseconds: C{int8}
    @ivar microseconds: Microseconds in the interval

    @type days: C{int4}
    @ivar days: Days in the interval

    @type months: C{int4}
    @ivar months: Months in the interval

    @param microseconds: Microseconds in the interval
    @type microseconds: C{int8}

    @param months: Months in the interval
    @type months: C{int4}

    @param days: Days in the interval
    @type days: C{int4}
    """

    def __init__(self, microseconds=0, days=0, months=0):
        self.microseconds = microseconds
        self.days = days
        self.months = months

    def __eq__(self, other):
        if self.microseconds != other.microseconds:
            return False
        if self.days != other.days:
            return False
        if self.months != other.months:
            return False
        return True

    def __str__(self):
        seconds = self.microseconds / 1000000
        hours = int(math.floor(seconds // 3600))
        seconds = seconds % 3600
        minutes = int(seconds // 60)
        seconds = seconds % 60

        return "%s months  %s days  %s hours  %s minutes  %s seconds" % (self.months, self.days, hours, minutes, seconds,)
