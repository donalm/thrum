#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `oid` module.
"""

from __future__ import division, absolute_import

from twisted.trial import unittest
from thrum import oid


class OidTests(unittest.TestCase):
    def test_text_type(self):
        o = oid.OID()
        self.assertEqual('text', o.oids[25])

    def test_text_oid(self):
        o = oid.OID()
        self.assertEqual(25, o.types['text'])

    def test_polygon_type(self):
        o = oid.OID()
        self.assertEqual('polygon', o.oids[604])

    def test_polygon_oid(self):
        o = oid.OID()
        self.assertEqual(604, o.types['polygon'])

    def test_interval_type(self):
        o = oid.OID()
        self.assertEqual('interval', o.oids[1186])

    def test_interval_oid(self):
        o = oid.OID()
        self.assertEqual(1186, o.types['interval'])

    def test_uuid_type(self):
        o = oid.OID()
        self.assertEqual('uuid', o.oids[2950])

    def test_uuid_oid(self):
        o = oid.OID()
        self.assertEqual(2950, o.types['uuid'])
