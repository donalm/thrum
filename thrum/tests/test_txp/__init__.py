#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Test that datatypes correlate between psycopg2 and thrum
"""

from __future__ import division, absolute_import

import re
import os
import json
import glob
import tempfile
import ipaddress

from datetime import datetime
from datetime import date
from datetime import timedelta
from txpostgres import txpostgres

CFFI=False
try:
    import psycopg2.extensions as psycopg2_extensions
except:
    import psycopg2cffi.extensions as psycopg2_extensions
    CFFI=True

#from thrum.twisted import pool
from thrum.twisted import connection
from thrum.interval import Interval
from twisted.trial import unittest
from twisted.internet import defer
from thrum import oid

try:
    buffer
except Exception as e:
    buffer = memoryview

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (Interval,)):
        return str(obj.microseconds/1000000.0)
        return str([obj.months, obj.days, obj.microseconds])

    if isinstance(obj, (timedelta,)):
        return str(obj.total_seconds())

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))

def UnicodeCursorFactory(cursor, connection):
    psycopg2_extensions.register_type(psycopg2_extensions.UNICODE, cursor)
    psycopg2_extensions.register_type(psycopg2_extensions.UNICODEARRAY, cursor)
    psycopg2_extensions.register_type(
        psycopg2_extensions.new_type(psycopg2_extensions.INTERVAL.values, 'ThrumInterval', mkinterval),
        cursor)
    return txpostgres.Cursor(cursor, connection)

def mkinterval(value, cur):
    try:
        hours, minutes, seconds = [int(x) for x in value.split(":")]
    except:
        return None

    microseconds = ((hours * 3600) + (minutes * 60) + seconds) * 1000000
    return Interval(microseconds=microseconds)

class QueryTests(unittest.TestCase):

    def setUp(self):
        configdir = os.environ.get("THRUMCONFDIR")
        if not configdir:
            raise Exception("The THRUMCONFDIR environment variable is not set. Cannot find THRUMCONFDIR/config.json Missing database credentials")
        configfile = os.path.join(configdir, "config.json")
        configdata = json.load(open(configfile))
        self.dsn = "dbname={database} user={user} password={password} host={host} port={port} client_encoding=utf-8".format(**configdata)
        self.txpostgres = None
        self.txpostgres = txpostgres.Connection()
        self.txpostgres.cursorFactory = UnicodeCursorFactory
        df0 = self.txpostgres.connect(self.dsn)

        self.thrum = connection.Connection()
        df1 = self.thrum.connect(**configdata)

        return defer.gatherResults([df0, df1])

    def _deep_compare_results(self, results, method_name):
        txpostgres_result, thrum_result = results

        if method_name == "xtest_array_byteas":
            print('\ntxpostgres_result')
            print(txpostgres_result)
            print('\nthrum_result')
            print(thrum_result)
            print("")

        for index, txpostgres_row in enumerate(txpostgres_result):
            thrum_row = thrum_result[index]
            self.deep_compare(thrum_row, txpostgres_row)

    def deep_compare(self, thrum_value, txpostgres_value):
        for index, thrum_element in enumerate(thrum_value):
            txpostgres_element = txpostgres_value[index]

            if isinstance(thrum_element, (tuple, list)) and isinstance(txpostgres_element, (tuple, list)):
                self.deep_compare(thrum_element, txpostgres_element)
            elif isinstance(thrum_element, ipaddress.IPv6Network) and not isinstance(txpostgres_element, ipaddress.IPv6Network):
                self.assertEqual(str(thrum_element), str(txpostgres_element))
            elif isinstance(thrum_element, float) and isinstance(txpostgres_element, float):
                self.assertAlmostEqual(thrum_element, txpostgres_element)
            elif isinstance(txpostgres_element, buffer):
                self.assertEqual(bytes(thrum_element), bytes(txpostgres_element))
            else:
                self.assertEqual(thrum_element, txpostgres_element)

    def compare_results(self, query, method_name, callback=None):
        df0 = self.txpostgres.runQuery(query)
        df1 = self.thrum.runQuery(query)
        df = defer.gatherResults([df0, df1])
        df.addCallback(self._deep_compare_results, method_name)
        return df

    def xtest_a(self):
        query = u'create table if not exists "booté" (a int4 not null)'
        return self.compare_results(query, 'xtest_a')

    def test_Fortune(self):
        query = 'select * from "Fortune"'
        return self.compare_results(query, 'test_Fortune')

    def test_World(self):
        query = 'select * from "World"'
        return self.compare_results(query, 'test_World')

    def test_array_bools(self):
        query = 'select * from "array_bools"'
        return self.compare_results(query, 'test_array_bools')

    def _test_array_boxes(self):
        '''
        This test is disabled because psycopg2 doesn't unpack arrays of boxes
        '''
        query = 'select * from "array_boxes"'
        return self.compare_results(query, 'test_array_boxes')

    def test_array_byteas(self):
        query = 'select * from "array_byteas"'
        return self.compare_results(query, 'test_array_byteas')

    def test_array_chars(self):
        query = 'select * from "array_chars"'
        return self.compare_results(query, 'test_array_chars')

    def test_array_cidrs(self):
        if CFFI:
            # psycopg2cffi is based on psycopg2 2.5, and prior to psycopg2 2.7,
            # arrays of networking types were not treated as arrays.
            # This test cannot succeed for now with pypy
            return
        query = 'select * from "array_cidrs"'
        return self.compare_results(query, 'test_array_cidrs')

    def _test_array_circles(self):
        query = 'select * from "array_circles"'
        return self.compare_results(query, 'test_array_circles')

    def test_array_floats(self):
        query = 'select * from "array_floats"'
        return self.compare_results(query, 'test_array_floats', self.assertAlmostEqual)

    def _test_array_inets(self):
        query = 'select * from "array_inets"'
        return self.compare_results(query, 'test_array_inets')

    def test_array_jsonbs(self):
        query = 'select * from "array_jsonbs"'
        return self.compare_results(query, 'test_array_jsonbs')

    def _test_array_jsons(self):
        query = 'select * from "array_jsons"'
        return self.compare_results(query, 'test_array_jsons')

    def _test_array_lines(self):
        query = 'select * from "array_lines"'
        return self.compare_results(query, 'test_array_lines')

    def _test_array_lsegs(self):
        query = 'select * from "array_lsegs"'
        return self.compare_results(query, 'test_array_lsegs')

    def _test_array_macaddrs(self):
        query = 'select * from "array_macaddrs"'
        return self.compare_results(query, 'test_array_macaddrs')

    def _test_array_moneys(self):
        query = 'select * from "array_moneys"'
        return self.compare_results(query, 'test_array_moneys')

    def _test_array_numerics(self):
        query = 'select * from "array_numerics"'
        return self.compare_results(query, 'test_array_numerics')

    def _test_array_paths(self):
        query = 'select * from "array_paths"'
        return self.compare_results(query, 'test_array_paths')

    def _test_array_points(self):
        query = 'select * from "array_points"'
        return self.compare_results(query, 'test_array_points')

    def _test_array_polygons(self):
        query = 'select * from "array_polygons"'
        return self.compare_results(query, 'test_array_polygons')

    def _test_array_texts(self):
        query = 'select * from "array_texts"'
        return self.compare_results(query, 'test_array_texts')

    def _test_array_times(self):
        query = 'select * from "array_times"'
        return self.compare_results(query, 'test_array_times')

    def _test_array_timetzs(self):
        query = 'select * from "array_timetzs"'
        return self.compare_results(query, 'test_array_timetzs')

    def _test_array_varchars(self):
        query = 'select * from "array_varchars"'
        return self.compare_results(query, 'test_array_varchars')

    def _test_bits(self):
        query = 'select * from "bits"'
        return self.compare_results(query, 'test_bits')

    def _test_bitstrings(self):
        query = 'select * from "bitstrings"'
        return self.compare_results(query, 'test_bitstrings')

    def _test_blunder(self):
        query = 'select * from "blunder"'
        return self.compare_results(query, 'test_blunder')

    def _test_bools(self):
        query = 'select * from "bools"'
        return self.compare_results(query, 'test_bools')

    def _test_boote(self):
        query = u'select * from "booté"'
        return self.compare_results(query, 'test_boote')

    def _test_boxes(self):
        query = 'select * from "boxes"'
        return self.compare_results(query, 'test_boxes')

    def _test_chars(self):
        query = 'select * from "chars"'
        return self.compare_results(query, 'test_chars')

    def _test_cidrs(self):
        query = 'select * from "cidrs"'
        return self.compare_results(query, 'test_cidrs')

    def _test_circles(self):
        query = 'select * from "circles"'
        return self.compare_results(query, 'test_circles')

    def _test_dates(self):
        query = 'select * from "dates"'
        return self.compare_results(query, 'test_dates')

    def _test_floats(self):
        query = 'select * from "floats"'
        return self.compare_results(query, 'test_floats', self.assertAlmostEqual)

    def _test_hss(self):
        query = 'select * from "hss"'
        return self.compare_results(query, 'test_hss')

    def _test_i2(self):
        query = 'select * from "i2"'
        return self.compare_results(query, 'test_i2')

    def _test_i4(self):
        query = 'select * from "i4"'
        return self.compare_results(query, 'test_i4')

    def _test_i8(self):
        query = 'select * from "i8"'
        return self.compare_results(query, 'test_i8')

    def _test_inets(self):
        query = 'select * from "inets"'
        return self.compare_results(query, 'test_inets')

    def _test_intvls(self):
        query = 'select * from "intvls"'
        return self.compare_results(query, 'test_intvls')

    def _test_jsonbs(self):
        query = 'select * from "jsonbs"'
        return self.compare_results(query, 'test_jsonbs')

    def _test_jsons(self):
        query = 'select * from "jsons"'
        return self.compare_results(query, 'test_jsons')

    def _test_leptons(self):
        query = 'select * from "leptons"'
        return self.compare_results(query, 'test_leptons')

    def _test_lines(self):
        query = 'select * from "lines"'
        return self.compare_results(query, 'test_lines')

    def _test_lsegs(self):
        query = 'select * from "lsegs"'
        return self.compare_results(query, 'test_lsegs')

    def _test_macaddrs(self):
        query = 'select * from "macaddrs"'
        return self.compare_results(query, 'test_macaddrs')

    def _test_moneys(self):
        query = 'select * from "moneys"'
        return self.compare_results(query, 'test_moneys')

    def _test_nulltest(self):
        query = 'select * from "nulltest"'
        return self.compare_results(query, 'test_nulltest')

    def _test_numerics(self):
        query = 'select * from "numerics"'
        return self.compare_results(query, 'test_numerics')

    def _test_paths(self):
        query = 'select * from "paths"'
        return self.compare_results(query, 'test_paths')

    def _test_points(self):
        query = 'select * from "points"'
        return self.compare_results(query, 'test_points')

    def _test_polygons(self):
        query = 'select * from "polygons"'
        return self.compare_results(query, 'test_polygons')

    def _test_polys(self):
        query = 'select * from "polys"'
        return self.compare_results(query, 'test_polys')

    def _test_testchar(self):
        query = 'select * from "testchar"'
        return self.compare_results(query, 'test_testchar')

    def _test_times(self):
        query = 'select * from "times"'
        return self.compare_results(query, 'test_times')

    def _test_timestamptzs(self):
        query = 'select * from "timestamptzs"'
        return self.compare_results(query, 'test_timestamptzs')

    def _test_timetzs(self):
        query = 'select * from "timetzs"'
        return self.compare_results(query, 'test_timetzs')

    def _test_tnulls(self):
        query = 'select * from "tnulls"'
        return self.compare_results(query, 'test_tnulls')

    def _test_tsv(self):
        query = 'select * from "tsv"'
        return self.compare_results(query, 'test_tsv')

    def _test_unknowns(self):
        query = 'select * from "unknowns"'
        return self.compare_results(query, 'test_unknowns')

    def _test_uuids(self):
        query = 'select * from "uuids"'
        return self.compare_results(query, 'test_uuids')

    def _test_vr(self):
        query = 'select * from "vr"'
        return self.compare_results(query, 'test_vr')

    def test_unicode_tablename(self):
        query = u'select * from "シア地下鉄"'
        return self.compare_results(query, 'test_unicode_tablename')

    def test_films(self):
        query = "select * from films"#tablename from pg_tables where tablename like 'boot%'"
        return self.compare_results(query, 'test_films')

    def tearDown(self):
        _none = self.txpostgres.close()
        return self.thrum.close()
