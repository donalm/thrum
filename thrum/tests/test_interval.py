#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `interval` module.
"""

from __future__ import division, absolute_import

from twisted.trial import unittest
from thrum import interval


class IntervalTests(unittest.TestCase):
    def test_attributes(self):
        i = interval.Interval(1123133, 22, 4)
        self.assertEqual(i.microseconds, 1123133)
        self.assertEqual(i.days, 22)
        self.assertEqual(i.months, 4)

    def test_stuff(self):
        i = interval.Interval(1123133, 22, 4)
        self.assertEqual(i.microseconds, 1123133)
        self.assertEqual(i.days, 22)
        self.assertEqual(i.months, 4)
