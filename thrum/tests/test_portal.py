#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `pgportal` module.
"""

from __future__ import division, absolute_import

from twisted.trial import unittest
# from thrum.twisted import portal
from thrum.twisted import statementfactory


class PgPortalTests(unittest.TestCase):

    def setUp(self):
        query = "SELECT * FROM pg_type LIMIT 4"
        self.statement = \
            statementfactory.PreparedStatementFactory.create(query)

    def test_attributes(self):
        return

    def test_stuff(self):
        return
