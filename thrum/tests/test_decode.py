#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `encode` module.
"""

from __future__ import division, absolute_import

import datetime
import base64
import ipaddress
import decimal
import uuid
from twisted.trial import unittest
from thrum import decode
from thrum import oid
from thrum import constants
from thrum import tzutc
from thrum import interval
from thrum import geometry
try:
    import pghstore
except ImportError:
    pghstore = None

try:
    buffer
except Exception as e:
    buffer = memoryview
'''
b6 = base64.b64encode
o = oid.OID()
e = decode.Decode(o)
'''


class DecodeTests(unittest.TestCase):

    def setUp(self):
        self.oids = oid.OID()
        self.decode = decode.Decode(self.oids)

    def test_decode_hstore(self):
        if pghstore is None:
            # Not currently available on py3
            return
        encoded = b'"one"=>"ONE","two"=>"TWO"'
        decoded = self.decode.hstore_decode(encoded, 0, len(encoded))
        self.assertEqual(decoded, {u"one": u"ONE", u"two": u"TWO"})

    def test_decode_unknown(self):
        decoder = self.decode.pg_types[self.oids.unknown][1]
        encoded = b'unknown data p\xc3\xb3'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(decoded, u'unknown data pó')

    def test_decode_cstring(self):
        decoder = self.decode.pg_types[self.oids.cstring][1]
        data = b'cstring data p\xc3\xb3\x00'
        decoded = decoder(data, 0, len(data))

        self.assertEqual(decoded, u'cstring data pó\x00')

    def test_decode_int4range(self):
        decoder = self.decode.pg_types[self.oids.int4range][1]

        b64 = 'AgAAAAQAAAAKAAAABAAAAGQ='
        data = base64.b64decode(b64)
        decoded = decoder(data, 0, len(data))
        self.assertEqual(decoded, (10, 100))

    def test_decode_int8range(self):
        decoder = self.decode.pg_types[self.oids.int8range][1]

        b64 = 'AgAAAAgAAAAAAAAACgAAAAgAAAAAAAAAZA=='
        data = base64.b64decode(b64)
        decoded = decoder(data, 0, len(data))
        self.assertEqual(decoded, (10, 100))

    def test_decode_uuid(self):
        decoder = self.decode.pg_types[self.oids.uuid][1]

        data = b'\xb8N\x9eo\xb2\xffE\x15\xb8m lB[\xc7('
        decoded = decoder(data, 0, len(data))
        self.assertEqual(decoded,
                         uuid.UUID('b84e9e6f-b2ff-4515-b86d-206c425bc728'))

    def test_decode_array_uuid(self):
        decoder = self.decode.pg_types[self.oids.array_uuid][1]

        b64 = 'AAAAAQAAAAAAAAuGAAAAAwAAAAEAAAAQNS0zFug9QGyiju3IiVaWZgAAABC4' \
              'Tp5vsv9FFbhtIGxCW8coAAAAEBATO3lP5EZanftPVVBN+Ts='
        data = base64.b64decode(b64)
        decoded = decoder(data, 0, len(data))

        expected = [uuid.UUID('352d3316-e83d-406c-a28e-edc889569666'),
                    uuid.UUID('b84e9e6f-b2ff-4515-b86d-206c425bc728'),
                    uuid.UUID('10133b79-4fe4-465a-9dfb-4f55504df93b')]
        self.assertEqual(decoded, expected)

    def test_decode_bit(self):
        decoder = self.decode.pg_types[self.oids.bit][1]
        encoded = b'\x00\x00\x00\x08\x9b'
        decoded = decoder(encoded, 0, len(encoded))
        data = '10011011'
        self.assertEqual(decoded, data)

    def test_decode_array_bit(self):

        decoder = self.decode.pg_types[self.oids.array_bit][1]
        b64 = 'AAAAAQAAAAAAAAYYAAAAAwAAAAEAAAAFAAAACJsAAAAHAAAAGIGB/wAAAAYA' \
              'AAAQkzM='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))

        data = ['10011011',
                '100000011000000111111111',
                '1001001100110011']
        self.assertEqual(decoded, data)

    def test_decode_varbit(self):
        decoder = self.decode.pg_types[self.oids.varbit][1]
        encoded = b'\x00\x00\x00\x08\x9b'
        decoded = decoder(encoded, 0, len(encoded))
        data = '10011011'
        self.assertEqual(decoded, data)

    def test_decode_array_varbit(self):
        decoder = self.decode.pg_types[self.oids.array_varbit][1]
        b64 = 'AAAAAQAAAAAAAAYaAAAAAwAAAAEAAAAFAAAACJsAAAAHAAAAGIGB/wAAAAYA' \
              'AAAQkzM='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))

        data = ['10011011',
                '100000011000000111111111',
                '1001001100110011']
        self.assertEqual(decoded, data)

    def test_decode_date(self):
        decoder = self.decode.pg_types[self.oids.date][1]
        encoded = b'\x00\x00\x16\x1f'
        decoded = decoder(encoded, 0, len(encoded))
        data = datetime.date(2015, 7, 4)
        self.assertEqual(decoded, data)

    def test_decode_array_date(self):
        decoder = self.decode.pg_types[self.oids.array_date][1]
        b64 = 'AAAAAQAAAAAAAAQ6AAAAAwAAAAEAAAAEAAAWHwAAAAQAABbUAAAABAAAERg='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))

        data = [datetime.date(2015, 7, 4),
                datetime.date(2016, 1, 1),
                datetime.date(2011, 12, 25)]
        self.assertEqual(decoded, data)

    def test_decode_time_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.time][1]
        encoded = b'B#\x13+\x8d\x80\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        data = datetime.time(11, 22, 43, 0)
        self.assertEqual(decoded, data)

    def test_decode_time_int(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.time][1]
        encoded = b'\x00\x00\x00\t\x89\x95\xc6\xc0'
        decoded = decoder(encoded, 0, len(encoded))
        data = datetime.time(11, 22, 43, 0)
        self.assertEqual(decoded, data)

    def test_decode_array_time_int(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.array_time][1]
        b64 = 'AAAAAQAAAAAAAAQ7AAAAAwAAAAEAAAAIAAAACYmVxsAAAAAIAAAAE7/YQ8AA' \
              'AAAIAAAADDWq5gA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [datetime.time(11, 22, 43, 0),
                datetime.time(23, 33, 43, 0),
                datetime.time(14, 34, 0, 0)]
        self.assertEqual(decoded, data)

    def test_decode_array_time_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.array_time][1]
        b64 = 'AAAAAQAAAAAAAAQ7AAAAAwAAAAEAAAAIQiMTK42AAAAAAAAIQjO/2EPAAAAA' \
              'AAAIQihrVcwAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [datetime.time(11, 22, 43, 0),
                datetime.time(23, 33, 43, 0),
                datetime.time(14, 34, 0, 0)]
        self.assertEqual(decoded, data)

    def test_decode_timestamp_int(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.timestamp][1]
        encoded = b'\x00\x01\xeb\xe1T\xd3\x18\x00'
        decoded = decoder(encoded, 0, len(encoded))
        data = datetime.datetime.utcfromtimestamp(1487512800)
        self.assertEqual(decoded, data)

    def test_decode_array_timestamp_int(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.array_timestamp][1]
        b64 = 'AAAAAQAAAAAAAARaAAAAAwAAAAEAAAAIAAHr4VTTGAAAAAAIAAHr4VTTGAAA' \
              'AAAIAAHr4VTTGAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800)]
        self.assertEqual(decoded, data)

    def test_decode_timestamp_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.timestamp][1]
        encoded = b'A\xc0\x1e0\xb0\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        data = datetime.datetime.utcfromtimestamp(1487512800)
        self.assertEqual(decoded, data)

    def test_decode_array_timestamp_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.array_timestamp][1]
        b64 = 'AAAAAQAAAAAAAARaAAAAAwAAAAEAAAAIQcAeMLAAAAAAAAAIQcAeMLAAAAAA' \
              'AAAIQcAeMLAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800)]
        self.assertEqual(decoded, data)

    def test_decode_timestamptz_int(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.timestamptz][1]
        encoded = b'\x00\x01\xae\x8a\xac\x87\xa0\x00'
        decoded = decoder(encoded, 0, len(encoded))
        tzinfo = tzutc.UTC()
        data = datetime.datetime(2015, 1, 1, 0, 0, 0, 0, tzinfo)
        self.assertEqual(decoded, data)

    def test_decode_timestamptz_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.timestamptz][1]
        encoded = b'A\xbc7J\x80\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        tzinfo = tzutc.UTC()
        data = datetime.datetime(2015, 1, 1, 0, 0, 0, 0, tzinfo)
        self.assertEqual(decoded, data)

    def test_decode_timetz(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.timetz][1]
        encoded = b'\x00\x00\x00\x08\x89\xd2P\r\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        tzinfo = tzutc.UTC()
        data = datetime.time(10, 11, 12, 13, tzinfo)
        self.assertEqual(decoded, data)

    def test_decode_array_timetz(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.array_timetz][1]

        b64 = 'AAAAAQAAAAAAAATyAAAAAwAAAAEAAAAMAAAACInSUA0AAAAAAAAADAAAAAiJ' \
              '0lANAAAAAAAAAAwAAAAIidJQDQAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        tzinfo = tzutc.UTC()
        data = [datetime.time(10, 11, 12, 13, tzinfo),
                datetime.time(10, 11, 12, 13, tzinfo),
                datetime.time(10, 11, 12, 13, tzinfo)]
        self.assertEqual(decoded, data)

    def test_decode_interval_integer(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.interval][1]

        b64 = 'AAAAAAAAJxAAAAAAAAAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = interval.Interval(microseconds=10000, days=0, months=0)
        self.assertEqual(decoded, data)

    def test_decode_array_interval_integer(self):
        self.decode.timestamps_are_integers()
        decoder = self.decode.pg_types[self.oids.array_interval][1]

        b64 = 'AAAAAQAAAAAAAASiAAAAAwAAAAEAAAAQAAAAAAAAJxAAAAAAAAAAAAAAABAA' \
              'AAAAAAAAAAAAAAQAAAAAAAAAEAAAAAAAAAAAAAAABAAAAAw='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [interval.Interval(microseconds=10000, days=0, months=0),
                interval.Interval(microseconds=0, days=4, months=0),
                interval.Interval(microseconds=0, days=4, months=12)]
        self.assertEqual(decoded, data)

    def test_decode_interval_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.interval][1]

        b64 = 'P4R64UeuFHsAAAAAAAAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = interval.Interval(microseconds=10000, days=0, months=0)
        self.assertEqual(decoded, data)

    def test_decode_array_interval_float(self):
        self.decode.timestamps_are_floats()
        decoder = self.decode.pg_types[self.oids.array_interval][1]

        b64 = 'AAAAAQAAAAAAAASiAAAAAwAAAAEAAAAQP4R64UeuFHsAAAAAAAAAAAAAABAA' \
              'AAAAAAAAAAAAAAQAAAAAAAAAEAAAAAAAAAAAAAAABAAAAAw='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [interval.Interval(microseconds=10000, days=0, months=0),
                interval.Interval(microseconds=0, days=4, months=0),
                interval.Interval(microseconds=0, days=4, months=12)]
        self.assertEqual(decoded, data)

    def test_decode_bool_true(self):
        decoder = self.decode.pg_types[self.oids.bool][1]

        encoded = b'\x01'
        decoded = decoder(encoded, 0, len(encoded))
        data = True
        self.assertEqual(decoded, data)

    def test_decode_bool_false(self):
        decoder = self.decode.pg_types[self.oids.bool][1]

        encoded = b'\x00'
        decoded = decoder(encoded, 0, len(encoded))
        data = False
        self.assertEqual(decoded, data)

    def test_decode_array_bool(self):
        decoder = self.decode.pg_types[self.oids.array_bool][1]

        b64 = 'AAAAAQAAAAAAAAAQAAAABQAAAAEAAAABAQAAAAEBAAAAAQAAAAABAAAAAAEB'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        data = [True, True, False, False, True]
        self.assertEqual(decoded, data)

    def test_decode_bytea(self):
        decoder = self.decode.pg_types[self.oids.bytea][1]

        encoded = buffer(b'bytes data')
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(decoded, encoded)

    def test_decode_array_bytea(self):
        decoder = self.decode.pg_types[self.oids.array_bytea][1]

        data = [buffer(b'bytes data'), buffer(b'more bytes data')]
        b64 = 'AAAAAQAAAAAAAAARAAAAAgAAAAEAAAAKYnl0ZXMgZGF0YQAAAA9tb3JlIGJ5' \
              'dGVzIGRhdGE='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(decoded, data)

    def test_decode_macaddr(self):
        decoder = self.decode.pg_types[self.oids.macaddr][1]

        encoded = b'\x02B\x1b\xd5$\xd8'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual("02:42:1B:D5:24:D8", decoded)

    def test_decode_array_macaddr(self):
        decoder = self.decode.pg_types[self.oids.array_macaddr][1]

        data = ["02:42:1B:D5:24:D8", "E8:39:35:B1:E2:F0"]
        b64 = 'AAAAAQAAAAAAAAM9AAAAAgAAAAEAAAAGAkIb1STYAAAABug5NbHi8A=='

        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(decoded, data)

    def test_decode_text(self):
        decoder = self.decode.pg_types[self.oids.text][1]

        encoded = b'bytes data'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(u'bytes data', decoded)

    def test_decode_array_text(self):
        decoder = self.decode.pg_types[self.oids.array_text][1]

        data = [u'bytes data', u'more bytes data']
        b64 = 'AAAAAQAAAAAAAAAZAAAAAgAAAAEAAAAKYnl0ZXMgZGF0YQAAAA9tb3JlIGJ5' \
              'dGVzIGRhdGE='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_xml(self):
        decoder = self.decode.pg_types[self.oids.xml][1]

        data = '''<?xml version="1.0" encoding="UTF-8"?>
        <project>
          <name>Thrum</name>
          <language>Python</language>
          <domain>Database Driver</domain>
          <license>MIT</license>
        </project>'''

        b64 = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KICAgICAg' \
              'ICA8cHJvamVjdD4KICAgICAgICAgIDxuYW1lPlRocnVtPC9uYW1lPgogICAg' \
              'ICAgICAgPGxhbmd1YWdlPlB5dGhvbjwvbGFuZ3VhZ2U+CiAgICAgICAgICA8' \
              'ZG9tYWluPkRhdGFiYXNlIERyaXZlcjwvZG9tYWluPgogICAgICAgICAgPGxp' \
              'Y2Vuc2U+TUlUPC9saWNlbnNlPgogICAgICAgIDwvcHJvamVjdD4='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_xml(self):
        decoder = self.decode.pg_types[self.oids.array_xml][1]

        data = '''<?xml version="1.0" encoding="UTF-8"?>
        <project>
          <name>Thrum</name>
          <language>Python</language>
          <domain>Database Driver</domain>
          <license>MIT</license>
        </project>'''
        data = [data, data]

        b64 = 'AAAAAQAAAAAAAACOAAAAAgAAAAEAAADaPD94bWwgdmVyc2lvbj0iMS4wIiBl' \
              'bmNvZGluZz0iVVRGLTgiPz4KICAgICAgICA8cHJvamVjdD4KICAgICAgICAg' \
              'IDxuYW1lPlRocnVtPC9uYW1lPgogICAgICAgICAgPGxhbmd1YWdlPlB5dGhv' \
              'bjwvbGFuZ3VhZ2U+CiAgICAgICAgICA8ZG9tYWluPkRhdGFiYXNlIERyaXZl' \
              'cjwvZG9tYWluPgogICAgICAgICAgPGxpY2Vuc2U+TUlUPC9saWNlbnNlPgog' \
              'ICAgICAgIDwvcHJvamVjdD4AAADaPD94bWwgdmVyc2lvbj0iMS4wIiBlbmNv' \
              'ZGluZz0iVVRGLTgiPz4KICAgICAgICA8cHJvamVjdD4KICAgICAgICAgIDxu' \
              'YW1lPlRocnVtPC9uYW1lPgogICAgICAgICAgPGxhbmd1YWdlPlB5dGhvbjwv' \
              'bGFuZ3VhZ2U+CiAgICAgICAgICA8ZG9tYWluPkRhdGFiYXNlIERyaXZlcjwv' \
              'ZG9tYWluPgogICAgICAgICAgPGxpY2Vuc2U+TUlUPC9saWNlbnNlPgogICAg' \
              'ICAgIDwvcHJvamVjdD4='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_varchar(self):
        decoder = self.decode.pg_types[self.oids.varchar][1]

        data = u"J'écoutais mais je n'ai pas compris."
        encoded = b"J'\xc3\xa9coutais mais je n'ai pas compris."
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_varchar(self):
        decoder = self.decode.pg_types[self.oids.array_varchar][1]

        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        b64 = 'AAAAAQAAAAAAAAQTAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
              'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
              'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_char(self):
        decoder = self.decode.pg_types[self.oids.char][1]
        data = u"J'écoutais mais je n'ai pas compris."
        encoded = b"J'\xc3\xa9coutais mais je n'ai pas compris."
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_char(self):
        decoder = self.decode.pg_types[self.oids.array_char][1]
        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        b64 = 'AAAAAQAAAAAAAAQSAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
              'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
              'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_bpchar(self):
        decoder = self.decode.pg_types[self.oids.bpchar][1]
        data = u"J'écoutais mais je n'ai pas compris."
        encoded = b"J'\xc3\xa9coutais mais je n'ai pas compris."
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_bpchar(self):
        decoder = self.decode.pg_types[self.oids.array_bpchar][1]
        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        b64 = 'AAAAAQAAAAAAAAQSAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
              'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
              'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_json(self):
        decoder = self.decode.pg_types[self.oids.json][1]
        data = {"one": [1, 2, 3, 4]}
        encoded = b'{"one":[1, 2, 3, 4]}'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_json(self):
        decoder = self.decode.pg_types[self.oids.array_json][1]
        data = [{u"one": [1, 2, 3, 4]},
                [0, 10, 12, 1],
                {u"one": 1, u"two": 2, u"three": 3}]
        b64 = 'AAAAAQAAAAAAAAByAAAAAwAAAAEAAAAUeyJvbmUiOlsxLCAyLCAzLCA0XX0A' \
              'AAAOWzAsIDEwLCAxMiwgMV0AAAAgeyJvbmUiOiAxLCAidHdvIjogMiwgInRo' \
              'cmVlIjogM30='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_jsonb(self):
        decoder = self.decode.pg_types[self.oids.jsonb][1]
        data = {"one": [1, 2, 3, 4]}
        encoded = b'\x01{"one":[1, 2, 3, 4]}'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_jsonb(self):
        decoder = self.decode.pg_types[self.oids.array_jsonb][1]
        data = [{u"one": [1, 2, 3, 4]},
                [0, 10, 12, 1],
                {u"one": 1, u"two": 2, u"three": 3}]
        b64 = 'AAAAAQAAAAAAAA7aAAAAAwAAAAEAAAAWAXsib25lIjogWzEsIDIsIDMsIDRd' \
              'fQAAAA8BWzAsIDEwLCAxMiwgMV0AAAAhAXsidGhyZWUiOiAzLCAidHdvIjog' \
              'MiwgIm9uZSI6IDF9'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_point(self):
        decoder = self.decode.pg_types[self.oids.point][1]

        data = (1, 2)
        b64 = b'P/AAAAAAAABAAAAAAAAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_point(self):
        decoder = self.decode.pg_types[self.oids.array_point][1]

        data = [(1, 2),
                (3, 4),
                (5, 6)]
        b64 = 'AAAAAQAAAAAAAAJYAAAAAwAAAAEAAAAQP/AAAAAAAABAAAAAAAAAAAAAABBA' \
              'CAAAAAAAAEAQAAAAAAAAAAAAEEAUAAAAAAAAQBgAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_circle(self):
        decoder = self.decode.pg_types[self.oids.circle][1]

        data = ((1, 2), 7)
        b64 = b'P/AAAAAAAABAAAAAAAAAAEAcAAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_circle(self):
        decoder = self.decode.pg_types[self.oids.array_circle][1]

        data = [((1, 2), 10),
                ((3, 4), 15),
                ((5, 6), 20)]
        b64 = 'AAAAAQAAAAAAAALOAAAAAwAAAAEAAAAYP/AAAAAAAABAAAAAAAAAAEAkAAAA' \
              'AAAAAAAAGEAIAAAAAAAAQBAAAAAAAABALgAAAAAAAAAAABhAFAAAAAAAAEAY' \
              'AAAAAAAAQDQAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_lseg(self):
        decoder = self.decode.pg_types[self.oids.lseg][1]
        data = (1.1, 2.0, 3.0, 4.5)
        b64 = b'P/GZmZmZmZpAAAAAAAAAAEAIAAAAAAAAQBIAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_lseg(self):
        decoder = self.decode.pg_types[self.oids.array_lseg][1]

        data = [(1.1, 2.2, 3.3, 4.4),
                (99.0, 100.0, 1000.0, 12000.0),
                (0.3, 33.0, 22.0, 3.0)]
        b64 = 'AAAAAQAAAAAAAAJZAAAAAwAAAAEAAAAgP/GZmZmZmZpAAZmZmZmZmkAKZmZm' \
              'ZmZmQBGZmZmZmZoAAAAgQFjAAAAAAABAWQAAAAAAAECPQAAAAAAAQMdwAAAA' \
              'AAAAAAAgP9MzMzMzMzNAQIAAAAAAAEA2AAAAAAAAQAgAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_path(self):
        decoder = self.decode.pg_types[self.oids.path][1]

        data = geometry.Path(False,
                             [(1.01, 2.02), (3.0, 4.0),
                              (5.0, 6.0), (99, 1000)])
        b64 = 'AAAAAAQ/8Cj1wo9cKUAAKPXCj1wpQAgAAAAAAABAEAAAAAAAAEAUAAAAAAAA' \
              'QBgAAAAAAABAWMAAAAAAAECPQAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_path(self):
        decoder = self.decode.pg_types[self.oids.array_path][1]

        data = [geometry.Path(False, [(1.1, 2.2), (3.3, 4.4)]),
                geometry.Path(False, [(99.0, 100.0), (1000.0, 12000.0),
                                      (22.0, 332.0), (12.2, 229.5)]),
                geometry.Path(False, [(0.3, 33.0), (22.0, 3.0),
                                      (47.5, 66.0), (233.0, 412.0)])]
        b64 = 'AAAAAQAAAAAAAAJaAAAAAwAAAAEAAAAlAAAAAAI/8ZmZmZmZmkABmZmZmZma' \
              'QApmZmZmZmZAEZmZmZmZmgAAAEUAAAAABEBYwAAAAAAAQFkAAAAAAABAj0AA' \
              'AAAAAEDHcAAAAAAAQDYAAAAAAABAdMAAAAAAAEAoZmZmZmZmQGywAAAAAAAA' \
              'AABFAAAAAAQ/0zMzMzMzM0BAgAAAAAAAQDYAAAAAAABACAAAAAAAAEBHwAAA' \
              'AAAAQFCAAAAAAABAbSAAAAAAAEB5wAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_line(self):
        decoder = self.decode.pg_types[self.oids.line][1]

        data = (1.01, 2.02, 3.0)

        b64 = 'P/Ao9cKPXClAACj1wo9cKUAIAAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_line(self):
        decoder = self.decode.pg_types[self.oids.array_line][1]

        data = [(1.01, 2.02, 3), (10.01, 20.02, 30)]
        b64 = 'AAAAAQAAAAAAAAJ0AAAAAgAAAAEAAAAYP/Ao9cKPXClAACj1wo9cKUAIAAAA' \
              'AAAAAAAAGEAkBR64UeuFQDQFHrhR64VAPgAAAAAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_box(self):
        decoder = self.decode.pg_types[self.oids.box][1]

        data = ((1.01, 99.0), (2.02, 3.0))
        b64 = 'P/Ao9cKPXClAWMAAAAAAAEAAKPXCj1wpQAgAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_box(self):
        decoder = self.decode.pg_types[self.oids.array_box][1]

        data = [((1.0, 3.0), (2.0, 4.0)), ((5.0, 7.0), (6.0, 8.0))]
        b64 = 'AAAAAQAAAAAAAAJbAAAAAgAAAAEAAAAgP/AAAAAAAABACAAAAAAAAEAAAAAA' \
              'AAAAQBAAAAAAAAAAAAAgQBQAAAAAAABAHAAAAAAAAEAYAAAAAAAAQCAAAAAA' \
              'AAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_polygon(self):
        decoder = self.decode.pg_types[self.oids.polygon][1]

        data = [(1.01, 2.02), (99, 3), (333, 100), (20, 20)]
        b64 = 'AAAABD/wKPXCj1wpQAAo9cKPXClAWMAAAAAAAEAIAAAAAAAAQHTQAAAAAABA' \
              'WQAAAAAAAEA0AAAAAAAAQDQAAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_polygon(self):
        decoder = self.decode.pg_types[self.oids.array_polygon][1]

        data = [[(1.0, 2.0), (3.0, 44.0), (15.0, 26.0), (37.0, 8.0)],
                [(100.0, 100.0), (200.0, 100.0), (200.0, 0.0),
                 (100.0, 0.0), (100.0, 100.0)]]
        b64 = 'AAAAAQAAAAAAAAJcAAAAAgAAAAEAAABEAAAABD/wAAAAAAAAQAAAAAAAAABA' \
              'CAAAAAAAAEBGAAAAAAAAQC4AAAAAAABAOgAAAAAAAEBCgAAAAAAAQCAAAAAA' \
              'AAAAAABUAAAABUBZAAAAAAAAQFkAAAAAAABAaQAAAAAAAEBZAAAAAAAAQGkA' \
              'AAAAAAAAAAAAAAAAAEBZAAAAAAAAAAAAAAAAAABAWQAAAAAAAEBZAAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_cidr_ip4net(self):
        decoder = self.decode.pg_types[self.oids.cidr][1]

        data = ipaddress.IPv4Network(u"192.168.100.128/25")
        b64 = 'AhkBBMCoZIA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_cidr_ip6net(self):
        decoder = self.decode.pg_types[self.oids.cidr][1]

        data = ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128")
        b64 = 'A4ABECABBPgAAwC6AuCB//4i0fE='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_cidr(self):
        decoder = self.decode.pg_types[self.oids.array_cidr][1]

        data = [ipaddress.IPv4Network(u"192.168.100.128/25"),
                ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128")
                ]
        b64 = 'AAAAAQAAAAAAAAKKAAAAAgAAAAEAAAAIAhkBBMCoZIAAAAAUA4ABECABBPgA' \
              'AwC6AuCB//4i0fE='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_inet_ip4net(self):
        decoder = self.decode.pg_types[self.oids.inet][1]

        data = ipaddress.IPv4Network(u"192.168.100.128/25")
        b64 = 'AhkBBMCoZIA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_inet_ip6net(self):
        decoder = self.decode.pg_types[self.oids.inet][1]

        data = ipaddress.IPv6Network(u"2001:db8:1234::/48")
        b64 = 'AzABECABDbgSNAAAAAAAAAAAAAA='

        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_inet_ip4addr(self):
        decoder = self.decode.pg_types[self.oids.inet][1]

        data = ipaddress.IPv4Address(u"192.168.100.128")
        b64 = 'AiAABMCoZIA='

        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_inet_ip6addr(self):
        decoder = self.decode.pg_types[self.oids.inet][1]

        ns = u"2001:0db8:1234:0012:0000:0000:0000:0000/112"
        data = ipaddress.IPv6Network(ns)
        b64 = 'A3ABECABDbgSNAASAAAAAAAAAAA='

        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_inet(self):
        decoder = self.decode.pg_types[self.oids.array_inet][1]

        data = [ipaddress.IPv4Network(u"192.168.100.128/25"),
                ipaddress.IPv6Network(u"2001:db8:1234::/48"),
                ipaddress.IPv4Address(u"192.168.100.128"),
                ipaddress.IPv6Address(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1")
                ]
        b64 = 'AAAAAQAAAAAAAANlAAAABAAAAAEAAAAIAhkBBMCoZIAAAAAUAzABECABDbgS' \
              'NAAAAAAAAAAAAAAAAAAIAiAABMCoZIAAAAAUA4AAECABBPgAAwC6AuCB//4i' \
              '0fE='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_money(self):
        decoder = self.decode.pg_types[self.oids.money][1]

        data = (100, 10)
        b64 = 'AAAAAAAAJxo='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_array_money(self):
        decoder = self.decode.pg_types[self.oids.array_money][1]

        data = [(100, 10), (200, 10), (410, 10)]
        b64 = 'AAAAAQAAAAAAAAMWAAAAAwAAAAEAAAAIAAAAAAAAJxoAAAAIAAAAAAAATioA' \
              'AAAIAAAAAAAAoDI='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_numeric_binary(self):
        self.decode.numerics_are_binary()
        decoder = self.decode.pg_types[self.oids.numeric][1]

        data = decimal.Decimal('1322.1231322')
        encoded = b'\x00\x03\x00\x00\x00\x00\x00\x07\x05*\x04\xcf\x0c\x94'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_numeric_text(self):
        self.decode.numerics_are_text()
        decoder = self.decode.pg_types[self.oids.numeric][1]

        data = decimal.Decimal('1322.1231322')
        encoded = b'1322.1231322'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_numeric(self):
        self.decode.numerics_are_binary()
        decoder = self.decode.pg_types[self.oids.array_numeric][1]

        data = [decimal.Decimal('122.123234223423454234235452'),
                decimal.Decimal('66.6666666666666666666666666'),
                decimal.Decimal('33.3333333333333333333333333')]
        b64 = 'AAAAAQAAAAAAAAakAAAAAwAAAAEAAAAWAAcAAAAAABgAegTQDV4NXxG+DV8V' \
              'TAAAABgACAAAAAAAGQBCGgoaChoKGgoaChoKF3AAAAAYAAgAAAAAABkAIQ0F' \
              'DQUNBQ0FDQUNBQu4'

        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_numeric_nan(self):
        self.decode.numerics_are_text()
        decoder = self.decode.pg_types[self.oids.array_numeric][1]

        data = [decimal.Decimal('122.123234223423454234235452'),
                decimal.Decimal('66.6666666666666666666666666'),
                decimal.Decimal('NaN'),
                decimal.Decimal('33.3333333333333333333333333')]
        encoded = b"{122.123234223423454234235452," \
                  b"66.6666666666666666666666666," \
                  b"NaN," \
                  b"33.3333333333333333333333333}"
        decoded = decoder(encoded, 0, len(encoded))
        for index, element in enumerate(data):
            self.assertEqual(element.is_nan(), decoded[index].is_nan())
            if element.is_nan():
                # No two NaN values are equal, e.g. this is False:
                # decimal.Decimal("NaN") == decimal.Decimal("NaN")
                continue
            self.assertEqual(element, decoded[index])

    def test_decode_float4(self):
        decoder = self.decode.pg_types[self.oids.float4][1]

        data = 4.5
        encoded = b'@\x90\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_float8(self):
        decoder = self.decode.pg_types[self.oids.float8][1]

        data = 4.5
        encoded = b'@\x12\x00\x00\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_float4(self):
        decoder = self.decode.pg_types[self.oids.array_float4][1]

        data = [1.0, 1.5, 333.5]
        b64 = 'AAAAAQAAAAAAAAK8AAAAAwAAAAEAAAAEP4AAAAAAAAQ/wAAAAAAABEOmwAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_float8(self):
        decoder = self.decode.pg_types[self.oids.array_float8][1]

        data = [1.0, 1.5, 333.5]
        b64 = 'AAAAAQAAAAAAAAK9AAAAAwAAAAEAAAAIP/AAAAAAAAAAAAAIP/gAAAAAAAAA' \
              'AAAIQHTYAAAAAAA='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_2d_array_float4(self):
        decoder = self.decode.pg_types[self.oids.array_float4][1]

        data = [[1.0, 2.5], [0.5, 4.0], [0.5, 6.0]]
        b64 = 'AAAAAgAAAAAAAAK8AAAAAwAAAAEAAAACAAAAAQAAAAQ/gAAAAAAABEAgAAAA' \
              'AAAEPwAAAAAAAARAgAAAAAAABD8AAAAAAAAEQMAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_2d_array_float8(self):
        decoder = self.decode.pg_types[self.oids.array_float8][1]

        data = [[1.0, 2.5], [0.5, 4.0], [0.5, 6.0]]
        b64 = 'AAAAAgAAAAAAAAK9AAAAAwAAAAEAAAACAAAAAQAAAAg/8AAAAAAAAAAAAAhA' \
              'BAAAAAAAAAAAAAg/4AAAAAAAAAAAAAhAEAAAAAAAAAAAAAg/4AAAAAAAAAAA' \
              'AAhAGAAAAAAAAA=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_3d_array_float4(self):
        decoder = self.decode.pg_types[self.oids.array_float4][1]

        data = [[[1.0, 1.5, 1.0],
                 [2.0, 2.5, 2.0]],
                [[3.0, 3.5, 3.0],
                 [4.0, 4.5, 4.0]],
                [[5.0, 5.5, 5.0],
                 [6.0, 6.5, 6.0]]]
        b64 = 'AAAAAwAAAAAAAAK8AAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAABD+AAAAA' \
              'AAAEP8AAAAAAAAQ/gAAAAAAABEAAAAAAAAAEQCAAAAAAAARAAAAAAAAABEBA' \
              'AAAAAAAEQGAAAAAAAARAQAAAAAAABECAAAAAAAAEQJAAAAAAAARAgAAAAAAA' \
              'BECgAAAAAAAEQLAAAAAAAARAoAAAAAAABEDAAAAAAAAEQNAAAAAAAARAwAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_3d_array_float8(self):
        decoder = self.decode.pg_types[self.oids.array_float8][1]

        data = [[[1.0, 1.5, 1.0],
                 [2.0, 2.5, 2.0]],
                [[3.0, 3.5, 3.0],
                 [4.0, 4.5, 4.0]],
                [[5.0, 5.5, 5.0],
                 [6.0, 6.5, 6.0]]]
        b64 = 'AAAAAwAAAAAAAAK9AAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAACD/wAAAA' \
              'AAAAAAAACD/4AAAAAAAAAAAACD/wAAAAAAAAAAAACEAAAAAAAAAAAAAACEAE' \
              'AAAAAAAAAAAACEAAAAAAAAAAAAAACEAIAAAAAAAAAAAACEAMAAAAAAAAAAAA' \
              'CEAIAAAAAAAAAAAACEAQAAAAAAAAAAAACEASAAAAAAAAAAAACEAQAAAAAAAA' \
              'AAAACEAUAAAAAAAAAAAACEAWAAAAAAAAAAAACEAUAAAAAAAAAAAACEAYAAAA' \
              'AAAAAAAACEAaAAAAAAAAAAAACEAYAAAAAAAA'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int8_zero(self):
        decoder = self.decode.pg_types[self.oids.int8][1]

        data = 0
        encoded = b'\x00\x00\x00\x00\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int8_max(self):
        decoder = self.decode.pg_types[self.oids.int8][1]

        data = constants.MAX_INT8
        encoded = b'\x7f\xff\xff\xff\xff\xff\xff\xff'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int8_min(self):
        decoder = self.decode.pg_types[self.oids.int8][1]

        data = constants.MIN_INT8
        encoded = b'\x80\x00\x00\x00\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int4_zero(self):
        decoder = self.decode.pg_types[self.oids.int4][1]

        data = 0
        encoded = b'\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int4_max(self):
        decoder = self.decode.pg_types[self.oids.int4][1]

        data = constants.MAX_INT4
        encoded = b'\x7f\xff\xff\xff'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int4_min(self):
        decoder = self.decode.pg_types[self.oids.int4][1]

        data = constants.MIN_INT4
        encoded = b'\x80\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int2_zero(self):
        decoder = self.decode.pg_types[self.oids.int2][1]

        data = 0
        encoded = b'\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int2_max(self):
        decoder = self.decode.pg_types[self.oids.int2][1]

        data = constants.MAX_INT2
        encoded = b'\x7f\xff'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_int2_min(self):
        decoder = self.decode.pg_types[self.oids.int2][1]

        data = constants.MIN_INT2
        encoded = b'\x80\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_int4(self):
        decoder = self.decode.pg_types[self.oids.array_int4][1]
        data = [constants.MIN_INT4, -1, 0, 1, 2, 3, 4, constants.MAX_INT4]
        b64 = 'AAAAAQAAAAAAAAAXAAAACAAAAAEAAAAEgAAAAAAAAAT/////AAAABAAA' \
              'AAAAAAAEAAAAAQAAAAQAAAACAAAABAAAAAMAAAAEAAAABAAAAAR/////'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_2d_array_int4(self):
        decoder = self.decode.pg_types[self.oids.array_int4][1]
        data = [[1, 1, 1], [2, 2, 2]]
        b64 = 'AAAAAgAAAAAAAAAXAAAAAgAAAAEAAAADAAAAAQAAAAQAAAABAAAABA' \
              'AAAAEAAAAEAAAAAQAAAAQAAAACAAAABAAAAAIAAAAEAAAAAg=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_3d_array_int4(self):
        decoder = self.decode.pg_types[self.oids.array_int4][1]
        data = [
            [[1, 1, 1],
             [2, 2, 2]],
            [[3, 3, 3],
             [4, 4, 4]],
            [[5, 5, 5],
             [6, 6, 6]]
        ]
        b64 = 'AAAAAwAAAAAAAAAXAAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAABAAAAAEA' \
              'AAAEAAAAAQAAAAQAAAABAAAABAAAAAIAAAAEAAAAAgAAAAQAAAACAAAABAAA' \
              'AAMAAAAEAAAAAwAAAAQAAAADAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAA' \
              'BAAAAAUAAAAEAAAABQAAAAQAAAAFAAAABAAAAAYAAAAEAAAABgAAAAQAAAAG'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_int2(self):
        decoder = self.decode.pg_types[self.oids.array_int2][1]
        data = [constants.MIN_INT2, -1, 0, 1, 2, 3, 4, constants.MAX_INT2]
        b64 = 'AAAAAQAAAAAAAAAVAAAACAAAAAEAAAACgAAAAAAC//8AAA' \
              'ACAAAAAAACAAEAAAACAAIAAAACAAMAAAACAAQAAAACf/8='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_2d_array_int2(self):
        decoder = self.decode.pg_types[self.oids.array_int2][1]
        data = [[1, 1], [2, 2], [3, 3]]
        b64 = 'AAAAAgAAAAAAAAAVAAAAAwAAAAEAAAACAAAAAQAAAAI' \
              'AAQAAAAIAAQAAAAIAAgAAAAIAAgAAAAIAAwAAAAIAAw=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_3d_array_int2(self):
        decoder = self.decode.pg_types[self.oids.array_int2][1]
        data = [[[1, 1], [2, 2]], [[3, 3], [4, 4]]]
        b64 = 'AAAAAwAAAAAAAAAVAAAAAgAAAAEAAAACAAAAAQAAAAIAAAABAAAAAgA' \
              'BAAAAAgABAAAAAgACAAAAAgACAAAAAgADAAAAAgADAAAAAgAEAAAAAgAE'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_int8(self):
        decoder = self.decode.pg_types[self.oids.array_int8][1]
        data = [constants.MIN_INT8, -1, 0, 1, 2, 3, 4, constants.MAX_INT8]
        b64 = 'AAAAAQAAAAAAAAAUAAAACAAAAAEAAAAIgAAAAAAAAAAAAAAI////' \
              '//////8AAAAIAAAAAAAAAAAAAAAIAAAAAAAAAAEAAAAIAAAAAAAA' \
              'AAIAAAAIAAAAAAAAAAMAAAAIAAAAAAAAAAQAAAAIf/////////8='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_2d_array_int8(self):
        decoder = self.decode.pg_types[self.oids.array_int8][1]
        data = [[1, 1], [2, 2], [3, 3]]
        b64 = 'AAAAAgAAAAAAAAAUAAAAAwAAAAEAAAACAAAAAQAAAAgAAAAAAAAA' \
              'AQAAAAgAAAAAAAAAAQAAAAgAAAAAAAAAAgAAAAgAAAAAAAAAAgAA' \
              'AAgAAAAAAAAAAwAAAAgAAAAAAAAAAw=='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_3d_array_int8(self):
        decoder = self.decode.pg_types[self.oids.array_int8][1]
        data = [[[1, 1], [2, 2]], [[3, 3], [4, 4]]]
        b64 = 'AAAAAwAAAAAAAAAUAAAAAgAAAAEAAAACAAAAAQAAAAIAAAABAAAA' \
              'CAAAAAAAAAABAAAACAAAAAAAAAABAAAACAAAAAAAAAACAAAACAAA' \
              'AAAAAAACAAAACAAAAAAAAAADAAAACAAAAAAAAAADAAAACAAAAAAA' \
              'AAAEAAAACAAAAAAAAAAE'
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_xid(self):
        self._test_generic_uint4('xid')

    def test_decode_oid(self):
        self._test_generic_uint4('oid')

    def _test_generic_uint4(self, name):
        oid = getattr(self.oids, name)
        decoder = self.decode.pg_types[oid][1]

        data = 9
        encoded = b'\x00\x00\x00\x09'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

        data = constants.MAX_UINT4
        encoded = b'\xff\xff\xff\xff'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

        data = constants.MIN_UINT4
        encoded = b'\x00\x00\x00\x00'
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)

    def test_decode_1d_array_oid(self):
        return self._test_generic_array_uint4("array_oid")

    def test_decode_1d_array_xid(self):
        return self._test_generic_array_uint4("array_xid")

    def _test_generic_array_uint4(self, name):
        oid = getattr(self.oids, name)
        decoder = self.decode.pg_types[oid][1]
        data = [constants.MIN_UINT4, 1, 2, 3, 4, constants.MAX_UINT4]
        b64 = 'AAAAAQAAAAAAAAAaAAAABgAAAAEAAAAEAAAAAAAAAAQAAA' \
              'ABAAAABAAAAAIAAAAEAAAAAwAAAAQAAAAEAAAABP////8='
        encoded = base64.b64decode(b64)
        decoded = decoder(encoded, 0, len(encoded))
        self.assertEqual(data, decoded)
