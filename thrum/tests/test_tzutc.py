#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `tzutc` module.
"""

from __future__ import division, absolute_import

import datetime
from twisted.trial import unittest
from thrum import tzutc


class TzUtcTests(unittest.TestCase):
    def test_tz(self):
        utc = tzutc.UTC()
        now = datetime.datetime.now()
        now = now.replace(tzinfo=utc)
        now = now.replace(second=0)
        now = now.replace(microsecond=0)

        then = datetime.datetime.now(utc)
        then = then.replace(second=0)
        then = then.replace(microsecond=0)

        self.assertEqual(now, then)
