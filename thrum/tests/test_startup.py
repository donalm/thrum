#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `startup` module.
"""

from __future__ import division, absolute_import

from twisted.trial import unittest
from thrum import startup
import base64


class StartupTests(unittest.TestCase):
    def test_construct(self):
        s = startup.StartupMessage.construct('dbname',
                                             'uname',
                                             3,
                                             'utf8',
                                             'thrum_app')
        data = "AAAAPwAAAAN1c2VyAHVuYW1lAGRhdGFiYXNlAGRib" \
               "mFtZQBhcHBsaWNhdGlvbl9uYW1lAHRocnVtX2FwcAAA"
        data = base64.b64decode(data)
        self.assertEqual(s, data)

    def test_make_argument(self):

        data = [('key', 'value', b'key\x00value\x00')]

        for key, value, encoded in data:
            s = startup.StartupMessage.make_argument(key, value)
            self.assertEqual(s, encoded)
