#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Tests for `encode` module.
"""

from __future__ import division, absolute_import

import datetime
import base64
import ipaddress
import decimal
import uuid
from twisted.trial import unittest
from thrum import encode
from thrum import oid
from thrum import constants
from thrum import errors
from thrum import tzutc
from thrum import interval
try:
    import pghstore
except ImportError:
    pghstore = None

'''
b6 = base64.b64encode
o = oid.OID()
e = encode.Encode(o)
'''


class EncodeTests(unittest.TestCase):

    def setUp(self):
        self.oids = oid.OID()
        self.encode = encode.Encode(self.oids)
        self.encode.timestamps_are_integers()

    def test_encode_hstore(self):
        if pghstore is None:
            # Not currently available on py3
            return
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["hstore"]
        data = {u"one": u"ONE", u"two": u"TWO"}
        bytesval = encoder(data)
        self.assertIn(bytesval, (b'"one"=>"ONE","two"=>"TWO"',
                                 b'"two"=>"TWO","one"=>"ONE"'))

    def test_encode_unknown(self):
        encoder, format_code = self.encode.encoders["unknown"]
        data = u'unknown data pó'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'unknown data p\xc3\xb3')

    def test_encode_cstring(self):
        encoder, format_code = self.encode.encoders["cstring"]
        data = u'cstring data pó'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'cstring data p\xc3\xb3\x00')

    def test_encode_int4range(self):
        encoder, format_code = self.encode.encoders["int4range"]
        data = [10, 100]
        bytesval = encoder(data)
        rval = 'AgAAAAQAAAAKAAAABAAAAGQ='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_int8range(self):
        encoder, format_code = self.encode.encoders["int8range"]
        data = [10, 100]
        bytesval = encoder(data)
        rval = 'AgAAAAgAAAAAAAAACgAAAAgAAAAAAAAAZA=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_uuid(self):
        encoder, format_code = self.encode.encoders["uuid"]
        data = uuid.UUID('b84e9e6f-b2ff-4515-b86d-206c425bc728')
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\xb8N\x9eo\xb2\xffE\x15\xb8m lB[\xc7(')

    def test_encode_array_uuid(self):
        encoder, format_code = self.encode.encoders["array_uuid"]
        data = [uuid.UUID('352d3316-e83d-406c-a28e-edc889569666'),
                uuid.UUID('b84e9e6f-b2ff-4515-b86d-206c425bc728'),
                uuid.UUID('10133b79-4fe4-465a-9dfb-4f55504df93b')]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAuGAAAAAwAAAAEAAAAQNS0zFug9QGyiju3IiVaWZgAAABC4' \
               'Tp5vsv9FFbhtIGxCW8coAAAAEBATO3lP5EZanftPVVBN+Ts='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_bit(self):
        encoder, format_code = self.encode.encoders["bit"]
        data = '10011011'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x00\x00\x08\x9b')

    def test_encode_array_bit(self):
        encoder, format_code = self.encode.encoders["array_bit"]
        data = ['10011011',
                '100000011000000111111111',
                '1001001100110011']
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAYYAAAAAwAAAAEAAAAFAAAACJsAAAAHAAAAGIGB/wAAAAYA' \
               'AAAQkzM='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_varbit(self):
        encoder, format_code = self.encode.encoders["varbit"]
        data = '10011011'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x00\x00\x08\x9b')

    def test_encode_array_varbit(self):
        encoder, format_code = self.encode.encoders["array_varbit"]
        data = ['10011011',
                '100000011000000111111111',
                '1001001100110011']
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAYaAAAAAwAAAAEAAAAFAAAACJsAAAAHAAAAGIGB/wAAAAYA' \
               'AAAQkzM='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_date(self):
        encoder, format_code = self.encode.encoders["date"]
        data = datetime.date(2015, 7, 4)
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x00\x16\x1f')

    def test_encode_array_date(self):
        encoder, format_code = self.encode.encoders["array_date"]
        data = [datetime.date(2015, 7, 4),
                datetime.date(2016, 1, 1),
                datetime.date(2011, 12, 25)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQ6AAAAAwAAAAEAAAAEAAAWHwAAAAQAABbUAAAABAAAERg='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_time_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["time"]
        data = '11:22:43'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'B#\x13+\x8d\x80\x00\x00')

    def test_encode_time_int(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["time"]
        data = '11:22:43'
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x00\x00\t\x89\x95\xc6\xc0')

    def test_encode_array_time_int(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["array_time"]
        data = ['11:22:43',
                '23:33:43',
                '14:34:00']
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQ7AAAAAwAAAAEAAAAIAAAACYmVxsAAAAAIAAAAE7/YQ8AA' \
               'AAAIAAAADDWq5gA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_time_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["array_time"]
        data = ['11:22:43',
                '23:33:43',
                '14:34:00']
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQ7AAAAAwAAAAEAAAAIQiMTK42AAAAAAAAIQjO/2EPAAAAA' \
               'AAAIQihrVcwAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_timestamp_int(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["timestamp"]
        data = datetime.datetime.utcfromtimestamp(1487512800)
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x01\xeb\xe1T\xd3\x18\x00')

    def test_encode_array_timestamp_int(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["array_timestamp"]
        data = [datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAARaAAAAAwAAAAEAAAAIAAHr4VTTGAAAAAAIAAHr4VTTGAAA' \
               'AAAIAAHr4VTTGAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_timestamp_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["timestamp"]
        data = datetime.datetime.utcfromtimestamp(1487512800)
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'A\xc0\x1e0\xb0\x00\x00\x00')

    def test_encode_array_timestamp_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["array_timestamp"]
        data = [datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800),
                datetime.datetime.utcfromtimestamp(1487512800)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAARaAAAAAwAAAAEAAAAIQcAeMLAAAAAAAAAIQcAeMLAAAAAA' \
               'AAAIQcAeMLAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_timestamptz_int(self):
        """
        On modern systems, timestamps are by default encoded as
        64-bit intevers
        """
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["timestamptz"]

        tzinfo = tzutc.UTC()
        data = datetime.datetime(2015, 1, 1, 0, 0, 0, 0, tzinfo)
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'\x00\x01\xae\x8a\xac\x87\xa0\x00')

    def test_encode_timestamptz_float(self):
        """
        On older systems, timestamps may be encoded as floats
        """
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["timestamptz"]

        tzinfo = tzutc.UTC()
        data = datetime.datetime(2015, 1, 1, 0, 0, 0, 0, tzinfo)
        bytesval = encoder(data)
        self.assertEqual(bytesval, b'A\xbc7J\x80\x00\x00\x00')

    def test_encode_timetz(self):
        encoder, format_code = self.encode.encoders["timetz"]

        tzinfo = tzutc.UTC()
        data = datetime.time(10, 11, 12, 13, tzinfo)
        bytesval = encoder(data)
        self.assertEqual(bytesval,
                         b'\x00\x00\x00\x08\x89\xd2P\r\x00\x00\x00\x00')

    def test_encode_array_timetz(self):
        encoder, format_code = self.encode.encoders["array_timetz"]
        tzinfo = tzutc.UTC()

        data = [datetime.time(10, 11, 12, 13, tzinfo),
                datetime.time(10, 11, 12, 13, tzinfo),
                datetime.time(10, 11, 12, 13, tzinfo)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAATyAAAAAwAAAAEAAAAMAAAACInSUA0AAAAAAAAADAAAAAiJ' \
               '0lANAAAAAAAAAAwAAAAIidJQDQAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_interval_integer(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["interval"]

        data = interval.Interval(microseconds=10000, days=0, months=0)
        bytesval = encoder(data)
        rval = 'AAAAAAAAJxAAAAAAAAAAAA=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_interval_integer(self):
        self.encode.timestamps_are_integers()
        encoder, format_code = self.encode.encoders["array_interval"]

        data = [interval.Interval(microseconds=10000, days=0, months=0),
                interval.Interval(microseconds=0, days=4, months=0),
                interval.Interval(microseconds=0, days=4, months=12)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAASiAAAAAwAAAAEAAAAQAAAAAAAAJxAAAAAAAAAAAAAAABAA' \
               'AAAAAAAAAAAAAAQAAAAAAAAAEAAAAAAAAAAAAAAABAAAAAw='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_interval_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["interval"]

        data = interval.Interval(microseconds=10000, days=0, months=0)
        bytesval = encoder(data)
        rval = 'P4R64UeuFHsAAAAAAAAAAA=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_interval_float(self):
        self.encode.timestamps_are_floats()
        encoder, format_code = self.encode.encoders["array_interval"]

        data = [interval.Interval(microseconds=10000, days=0, months=0),
                interval.Interval(microseconds=0, days=4, months=0),
                interval.Interval(microseconds=0, days=4, months=12)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAASiAAAAAwAAAAEAAAAQP4R64UeuFHsAAAAAAAAAAAAAABAA' \
               'AAAAAAAAAAAAAAQAAAAAAAAAEAAAAAAAAAAAAAAABAAAAAw='

        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_bool(self):
        encoder, format_code = self.encode.encoders["bool"]
        bytesval = encoder(False)
        self.assertEqual(bytesval, b'\x00')

        bytesval = encoder(True)
        self.assertEqual(bytesval, b'\x01')

    def test_encode_array_bool(self):
        encoder, format_code = self.encode.encoders["array_bool"]
        bytesval = encoder([True, True, False, False, True])
        rval = 'AAAAAQAAAAAAAAAQAAAABQAAAAEAAAABAQAAAAEBAAAAAQAAAAABAAAAAAEB'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_bytea(self):
        encoder, format_code = self.encode.encoders["bytea"]
        bytesval = encoder(b'bytes data')
        self.assertEqual(bytesval, b'bytes data')

    def test_encode_array_bytea(self):
        encoder, format_code = self.encode.encoders["array_bytea"]
        bytesval = encoder([b'bytes data', b'more bytes data'])
        rval = 'AAAAAQAAAAAAAAARAAAAAgAAAAEAAAAKYnl0ZXMgZGF0YQAAAA9tb3JlIGJ5' \
               'dGVzIGRhdGE='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_macaddr(self):
        encoder, format_code = self.encode.encoders["macaddr"]
        bytesval = encoder("02:42:1b:d5:24:d8")
        self.assertEqual(bytesval, b'\x02B\x1b\xd5$\xd8')

    def test_encode_array_macaddr(self):
        encoder, format_code = self.encode.encoders["array_macaddr"]
        bytesval = encoder(["02:42:1b:d5:24:d8", "e8:39:35:b1:e2:f0"])
        rval = 'AAAAAQAAAAAAAAM9AAAAAgAAAAEAAAAGAkIb1STYAAAABug5NbHi8A=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_text(self):
        encoder, format_code = self.encode.encoders["text"]
        bytesval = encoder(u'bytes data')
        self.assertEqual(bytesval, b'bytes data')

    def test_encode_array_text(self):
        encoder, format_code = self.encode.encoders["array_text"]
        bytesval = encoder([u'bytes data', u'more bytes data'])
        rval = 'AAAAAQAAAAAAAAAZAAAAAgAAAAEAAAAKYnl0ZXMgZGF0YQAAAA9tb3JlIGJ5' \
               'dGVzIGRhdGE='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_xml(self):
        encoder, format_code = self.encode.encoders["xml"]
        xmltest = '''<?xml version="1.0" encoding="UTF-8"?>
        <project>
          <name>Thrum</name>
          <language>Python</language>
          <domain>Database Driver</domain>
          <license>MIT</license>
        </project>'''

        rval = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KICAgICAg' \
               'ICA8cHJvamVjdD4KICAgICAgICAgIDxuYW1lPlRocnVtPC9uYW1lPgogICAg' \
               'ICAgICAgPGxhbmd1YWdlPlB5dGhvbjwvbGFuZ3VhZ2U+CiAgICAgICAgICA8' \
               'ZG9tYWluPkRhdGFiYXNlIERyaXZlcjwvZG9tYWluPgogICAgICAgICAgPGxp' \
               'Y2Vuc2U+TUlUPC9saWNlbnNlPgogICAgICAgIDwvcHJvamVjdD4='
        expected = base64.b64decode(rval)
        bytesval = encoder(xmltest)
        self.assertEqual(bytesval, expected)

    def test_encode_array_xml(self):
        encoder, format_code = self.encode.encoders["array_xml"]
        xmltest = '''<?xml version="1.0" encoding="UTF-8"?>
        <project>
          <name>Thrum</name>
          <language>Python</language>
          <domain>Database Driver</domain>
          <license>MIT</license>
        </project>'''

        data = [xmltest, xmltest]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAACOAAAAAgAAAAEAAADaPD94bWwgdmVyc2lvbj0iMS4wIiBl' \
               'bmNvZGluZz0iVVRGLTgiPz4KICAgICAgICA8cHJvamVjdD4KICAgICAgICAg' \
               'IDxuYW1lPlRocnVtPC9uYW1lPgogICAgICAgICAgPGxhbmd1YWdlPlB5dGhv' \
               'bjwvbGFuZ3VhZ2U+CiAgICAgICAgICA8ZG9tYWluPkRhdGFiYXNlIERyaXZl' \
               'cjwvZG9tYWluPgogICAgICAgICAgPGxpY2Vuc2U+TUlUPC9saWNlbnNlPgog' \
               'ICAgICAgIDwvcHJvamVjdD4AAADaPD94bWwgdmVyc2lvbj0iMS4wIiBlbmNv' \
               'ZGluZz0iVVRGLTgiPz4KICAgICAgICA8cHJvamVjdD4KICAgICAgICAgIDxu' \
               'YW1lPlRocnVtPC9uYW1lPgogICAgICAgICAgPGxhbmd1YWdlPlB5dGhvbjwv' \
               'bGFuZ3VhZ2U+CiAgICAgICAgICA8ZG9tYWluPkRhdGFiYXNlIERyaXZlcjwv' \
               'ZG9tYWluPgogICAgICAgICAgPGxpY2Vuc2U+TUlUPC9saWNlbnNlPgogICAg' \
               'ICAgIDwvcHJvamVjdD4='

        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_varchar(self):
        encoder, format_code = self.encode.encoders["varchar"]
        bytesval = encoder(u"J'écoutais mais je n'ai pas compris.")
        self.assertEqual(bytesval,
                         b"J'\xc3\xa9coutais mais je n'ai pas compris.")

    def test_encode_array_varchar(self):
        encoder, format_code = self.encode.encoders["array_varchar"]
        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQTAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
               'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
               'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_char(self):
        encoder, format_code = self.encode.encoders["char"]
        bytesval = encoder(u"J'écoutais mais je n'ai pas compris.")
        self.assertEqual(bytesval,
                         b"J'\xc3\xa9coutais mais je n'ai pas compris.")

    def test_encode_array_char(self):
        encoder, format_code = self.encode.encoders["array_char"]
        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQSAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
               'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
               'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_bpchar(self):
        encoder, format_code = self.encode.encoders["bpchar"]
        bytesval = encoder(u"J'écoutais mais je n'ai pas compris.")
        self.assertEqual(bytesval,
                         b"J'\xc3\xa9coutais mais je n'ai pas compris.")

    def test_encode_array_bpchar(self):
        encoder, format_code = self.encode.encoders["array_bpchar"]
        data = [u"J'écoutais mais je n'ai pas compris.",
                u"",
                u"Nā Ngā Pae O Te Māramatanga te pūtea tautoko."]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAQSAAAAAwAAAAEAAAAlSifDqWNvdXRhaXMgbWFpcyBqZSBu' \
               'J2FpIHBhcyBjb21wcmlzLgAAAAAAAAAxTsSBIE5nxIEgUGFlIE8gVGUgTcSB' \
               'cmFtYXRhbmdhIHRlIHDFq3RlYSB0YXV0b2tvLg=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_json(self):
        encoder, format_code = self.encode.encoders["json"]
        bytesval = encoder('{"one":[1, 2, 3, 4]}')
        self.assertEqual(bytesval, b'{"one":[1, 2, 3, 4]}')

    def test_encode_array_json(self):
        encoder, format_code = self.encode.encoders["array_json"]
        data = ['{"one":[1, 2, 3, 4]}',
                '[0, 10, 12, 1]',
                '{"one": 1, "two": 2, "three": 3}']
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAByAAAAAwAAAAEAAAAUeyJvbmUiOlsxLCAyLCAzLCA0XX0A' \
               'AAAOWzAsIDEwLCAxMiwgMV0AAAAgeyJvbmUiOiAxLCAidHdvIjogMiwgInRo' \
               'cmVlIjogM30='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_jsonb(self):
        encoder, format_code = self.encode.encoders["jsonb"]
        bytesval = encoder('{"one":[1, 2, 3, 4]}')
        self.assertEqual(bytesval, b'\x01"{\\"one\\":[1, 2, 3, 4]}"')

    def test_encode_array_jsonb(self):
        encoder, format_code = self.encode.encoders["array_jsonb"]
        data = [{"one": [1, 2, 3, 4]},
                [0, 10, 12, 1],
                {"one": 1}]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAA7aAAAAAwAAAAEAAAAWAXsib25lIjogWzEsIDIsIDMsIDRd' \
               'fQAAAA8BWzAsIDEwLCAxMiwgMV0AAAALAXsib25lIjogMX0='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_point(self):
        encoder, format_code = self.encode.encoders["point"]
        bytesval = encoder((1, 2))
        rval = b'P/AAAAAAAABAAAAAAAAAAA=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_point(self):
        encoder, format_code = self.encode.encoders["array_point"]
        data = [(1, 2),
                (3, 4),
                (5, 6)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJYAAAAAwAAAAEAAAAQP/AAAAAAAABAAAAAAAAAAAAAABBA' \
               'CAAAAAAAAEAQAAAAAAAAAAAAEEAUAAAAAAAAQBgAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_circle(self):
        encoder, format_code = self.encode.encoders["circle"]
        bytesval = encoder(((1, 2), 7))
        rval = b'P/AAAAAAAABAAAAAAAAAAEAcAAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_circle(self):
        encoder, format_code = self.encode.encoders["array_circle"]
        data = [((1, 2), 10),
                ((3, 4), 15),
                ((5, 6), 20)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAALOAAAAAwAAAAEAAAAYP/AAAAAAAABAAAAAAAAAAEAkAAAA' \
               'AAAAAAAAGEAIAAAAAAAAQBAAAAAAAABALgAAAAAAAAAAABhAFAAAAAAAAEAY' \
               'AAAAAAAAQDQAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_lseg(self):
        encoder, format_code = self.encode.encoders["lseg"]
        bytesval = encoder((1.1, 2.0, 3.0, 4.5))
        rval = b'P/GZmZmZmZpAAAAAAAAAAEAIAAAAAAAAQBIAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_lseg(self):
        encoder, format_code = self.encode.encoders["array_lseg"]
        data = [(1.1, 2.2, 3.3, 4.4),
                (99, 100, 1000, 12000),
                (0.3, 33, 22, 3)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJZAAAAAwAAAAEAAAAgP/GZmZmZmZpAAZmZmZmZmkAKZmZm' \
               'ZmZmQBGZmZmZmZoAAAAgQFjAAAAAAABAWQAAAAAAAECPQAAAAAAAQMdwAAAA' \
               'AAAAAAAgP9MzMzMzMzNAQIAAAAAAAEA2AAAAAAAAQAgAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_path(self):
        encoder, format_code = self.encode.encoders["path"]
        bytesval = encoder([(1.01, 2.02), (3, 4), (5, 6), (99, 1000)])
        rval = 'AAAAAAQ/8Cj1wo9cKUAAKPXCj1wpQAgAAAAAAABAEAAAAAAAAEAUAAAAAAAA' \
               'QBgAAAAAAABAWMAAAAAAAECPQAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_path(self):
        encoder, format_code = self.encode.encoders["array_path"]
        data = [[(1.1, 2.2), (3.3, 4.4)],
                [(99, 100), (1000, 12000), (22, 332), (12.2, 229.5)],
                [(0.3, 33), (22, 3), (47.5, 66), (233, 412)]]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJaAAAAAwAAAAEAAAAlAAAAAAI/8ZmZmZmZmkABmZmZmZma' \
               'QApmZmZmZmZAEZmZmZmZmgAAAEUAAAAABEBYwAAAAAAAQFkAAAAAAABAj0AA' \
               'AAAAAEDHcAAAAAAAQDYAAAAAAABAdMAAAAAAAEAoZmZmZmZmQGywAAAAAAAA' \
               'AABFAAAAAAQ/0zMzMzMzM0BAgAAAAAAAQDYAAAAAAABACAAAAAAAAEBHwAAA' \
               'AAAAQFCAAAAAAABAbSAAAAAAAEB5wAAAAAAA'
        rval = 'AAAAAQAAAAAAAAJaAAAAAwAAAAEAAAAlAAAAAAI/8ZmZmZmZmkABmZmZmZma' \
               'QApmZmZmZmZAEZmZmZmZmgAAAEUAAAAABEBYwAAAAAAAQFkAAAAAAABAj0AA' \
               'AAAAAEDHcAAAAAAAQDYAAAAAAABAdMAAAAAAAEAoZmZmZmZmQGywAAAAAAAA' \
               'AABFAAAAAAQ/0zMzMzMzM0BAgAAAAAAAQDYAAAAAAABACAAAAAAAAEBHwAAA' \
               'AAAAQFCAAAAAAABAbSAAAAAAAEB5wAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_path_object(self):

        class Path(list):
            def __init__(self, closed, *args, **kwargs):
                list.__init__(self, *args, **kwargs)
                self.closed = bool(closed)

        encoder, format_code = self.encode.encoders["path"]
        bytesval = encoder(Path(False,
                                [(1.01, 2.02), (3, 4), (5, 6), (99, 1000)]))
        rval = 'AAAAAAQ/8Cj1wo9cKUAAKPXCj1wpQAgAAAAAAABAEAAAAAAAAEAUAAAAAAAA' \
               'QBgAAAAAAABAWMAAAAAAAECPQAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_line(self):
        encoder, format_code = self.encode.encoders["line"]
        bytesval = encoder((1.01, 2.02, 3))
        rval = 'P/Ao9cKPXClAACj1wo9cKUAIAAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_line(self):
        encoder, format_code = self.encode.encoders["array_line"]
        data = [(1.01, 2.02, 3), (10.01, 20.02, 30)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJ0AAAAAgAAAAEAAAAYP/Ao9cKPXClAACj1wo9cKUAIAAAA' \
               'AAAAAAAAGEAkBR64UeuFQDQFHrhR64VAPgAAAAAAAA=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_box(self):
        encoder, format_code = self.encode.encoders["box"]
        bytesval = encoder(((1.01, 2.02), (99, 3)))
        rval = 'P/Ao9cKPXClAWMAAAAAAAEAAKPXCj1wpQAgAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_box(self):
        encoder, format_code = self.encode.encoders["array_box"]
        data = [((1, 2), (3, 4)), ((5, 6), (7, 8))]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJbAAAAAgAAAAEAAAAgP/AAAAAAAABACAAAAAAAAEAAAAAA' \
               'AAAAQBAAAAAAAAAAAAAgQBQAAAAAAABAHAAAAAAAAEAYAAAAAAAAQCAAAAAA' \
               'AAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_polygon(self):
        encoder, format_code = self.encode.encoders["polygon"]
        bytesval = encoder(((1.01, 2.02), (99, 3), (333, 100), (20, 20)))
        rval = 'AAAABD/wKPXCj1wpQAAo9cKPXClAWMAAAAAAAEAIAAAAAAAAQHTQAAAAAABA' \
               'WQAAAAAAAEA0AAAAAAAAQDQAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_polygon(self):
        encoder, format_code = self.encode.encoders["array_polygon"]
        data = [((1, 2), (3, 44), (15, 26), (37, 8)),
                ((100, 100), (200, 100), (200, 0), (100, 0), (100, 100))]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAJcAAAAAgAAAAEAAABEAAAABD/wAAAAAAAAQAAAAAAAAABA' \
               'CAAAAAAAAEBGAAAAAAAAQC4AAAAAAABAOgAAAAAAAEBCgAAAAAAAQCAAAAAA' \
               'AAAAAABUAAAABUBZAAAAAAAAQFkAAAAAAABAaQAAAAAAAEBZAAAAAAAAQGkA' \
               'AAAAAAAAAAAAAAAAAEBZAAAAAAAAAAAAAAAAAABAWQAAAAAAAEBZAAAAAAAA'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_cidr_ip4net(self):
        encoder, format_code = self.encode.encoders["cidr"]
        bytesval = encoder(ipaddress.IPv4Network(u"192.168.100.128/25"))
        rval = 'AhkBBMCoZIA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_cidr_ip6net(self):
        encoder, format_code = self.encode.encoders["cidr"]
        bytesval = encoder(
            ipaddress.IPv6Network(u"2001:db8:1234::/48")
        )
        rval = 'AzABECABDbgSNAAAAAAAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_cidr(self):
        encoder, format_code = self.encode.encoders["array_cidr"]
        data = [ipaddress.IPv4Network(u"192.168.100.128/25"),
                ipaddress.IPv6Network(u"2001:db8:1234::/48")
                ]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAKKAAAAAgAAAAEAAAAIAhkBBMCoZIAAAAAUAzABECABDbgS' \
               'NAAAAAAAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_inet_ip5net(self):
        encoder, format_code = self.encode.encoders["inet"]
        bytesval = encoder(ipaddress.IPv4Network(u"192.168.100.128/25"))
        rval = 'AhkBBMCoZIA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_inet_ip6net(self):
        encoder, format_code = self.encode.encoders["inet"]
        bytesval = encoder(
            ipaddress.IPv6Network(u"2001:db8:1234::/48")
        )
        rval = 'AzABECABDbgSNAAAAAAAAAAAAAA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_inet_ip4addr(self):
        encoder, format_code = self.encode.encoders["inet"]
        bytesval = encoder(ipaddress.IPv4Address(u"192.168.100.128"))
        rval = 'AiAABMCoZIA='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_inet_ip6addr(self):
        encoder, format_code = self.encode.encoders["inet"]
        bytesval = encoder(
            ipaddress.IPv6Address(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1")
        )
        rval = 'A4AAECABBPgAAwC6AuCB//4i0fE='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_inet(self):
        encoder, format_code = self.encode.encoders["array_inet"]
        data = [ipaddress.IPv4Network(u"192.168.100.128/25"),
                ipaddress.IPv6Network(u"2001:db8:1234::/48"),
                ipaddress.IPv4Address(u"192.168.100.128"),
                ipaddress.IPv6Address(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1")
                ]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAANlAAAABAAAAAEAAAAIAhkBBMCoZIAAAAAUAzABECABDbgS' \
               'NAAAAAAAAAAAAAAAAAAIAiAABMCoZIAAAAAUA4AAECABBPgAAwC6AuCB//4i' \
               '0fE='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_money(self):
        encoder, format_code = self.encode.encoders["money"]
        bytesval = encoder((100, 10))
        rval = 'AAAAAAAAJxo='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_array_money(self):
        encoder, format_code = self.encode.encoders["array_money"]
        data = [(100, 10), (200, 10), (410, 10)]
        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAMWAAAAAwAAAAEAAAAIAAAAAAAAJxoAAAAIAAAAAAAATioA' \
               'AAAIAAAAAAAAoDI='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_numeric_binary(self):
        encoder, format_code = self.encode.encoders["numeric"]
        bytesval = encoder(decimal.Decimal('1322.1231322'))
        rval = b'AAMAAAAAAAcFKgTPDJQ='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_numeric_text(self):
        self.encode.numerics_are_text()
        encoder, format_code = self.encode.encoders["numeric"]
        bytesval = encoder(decimal.Decimal('1322.1231322'))
        self.assertEqual(bytesval,
                         b'1322.1231322')

    def test_encode_1d_array_numeric_text(self):
        self.encode.numerics_are_text()
        encoder, format_code = self.encode.encoders["array_numeric"]

        data = [decimal.Decimal('122.123234223423454234235452'),
                decimal.Decimal('66.6666666666666666666666666'),
                decimal.Decimal('33.3333333333333333333333333')]

        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAakAAAAAwAAAAEAAAAcMTIyLjEyMzIzNDIyMzQyMzQ1NDIz' \
               'NDIzNTQ1MgAAABw2Ni42NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2AAAAHDMz' \
               'LjMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzMzM='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_1d_array_numeric_binary(self):
        self.encode.numerics_are_binary()
        encoder, format_code = self.encode.encoders["array_numeric"]

        data = [decimal.Decimal('122.123234223423454234235452'),
                decimal.Decimal('66.6666666666666666666666666'),
                decimal.Decimal('33.3333333333333333333333333')]

        bytesval = encoder(data)
        rval = 'AAAAAQAAAAAAAAakAAAAAwAAAAEAAAAWAAcAAAAAABgAegTQDV4NXxG+DV8V' \
               'TAAAABgACAAAAAAAGQBCGgoaChoKGgoaChoKF3AAAAAYAAgAAAAAABkAIQ0F' \
               'DQUNBQ0FDQUNBQu4'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_float4(self):
        encoder, format_code = self.encode.encoders["float4"]
        bytesval = encoder(1e-23)
        self.assertEqual(bytesval, b'\x19Am\x9a')

    def test_encode_float8(self):
        encoder, format_code = self.encode.encoders["float8"]
        bytesval = encoder(1e-23)
        self.assertEqual(bytesval, b';(-\xb3@\x12\xb2Q')

    def test_encode_1d_array_float4(self):
        encoder, format_code = self.encode.encoders["array_float4"]
        bytesval = encoder([0.3, 1e-23, 4.4, 333.1])
        rval = 'AAAAAQAAAAAAAAK8AAAABAAAAAEAAAAEPpmZ' \
               'mgAAAAQZQW2aAAAABECMzM0AAAAEQ6aMzQ=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_2d_array_float4(self):
        encoder, format_code = self.encode.encoders["array_float4"]
        bytesval = encoder([[0.1, 0.2], [0.3, 0.4], [0.5, 0.6]])
        rval = 'AAAAAgAAAAAAAAK8AAAAAwAAAAEAAAACAAAAAQAAAAQ9zMzNAAAA' \
               'BD5MzM0AAAAEPpmZmgAAAAQ+zMzNAAAABD8AAAAAAAAEPxmZmg=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_3d_array_float4(self):
        encoder, format_code = self.encode.encoders["array_float4"]
        list_3d = [[[1.1, 1.2, 1.3],
                    [2.1, 2.2, 2.3]],
                   [[3.1, 3.2, 3.3],
                    [4.1, 4.2, 4.3]],
                   [[5.1, 5.2, 5.3],
                    [6.1, 6.2, 6.3]]]

        bytesval = encoder(list_3d)
        rval = 'AAAAAwAAAAAAAAK8AAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAABD+MzM0A' \
               'AAAEP5mZmgAAAAQ/pmZmAAAABEAGZmYAAAAEQAzMzQAAAARAEzMzAAAABEBG' \
               'ZmYAAAAEQEzMzQAAAARAUzMzAAAABECDMzMAAAAEQIZmZgAAAARAiZmaAAAA' \
               'BECjMzMAAAAEQKZmZgAAAARAqZmaAAAABEDDMzMAAAAEQMZmZgAAAARAyZma'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_1d_array_float8(self):
        encoder, format_code = self.encode.encoders["array_float8"]
        bytesval = encoder([0.3, 1e-23, 4.4, 333.1])
        rval = 'AAAAAQAAAAAAAAK9AAAABAAAAAEAAAAIP9MzMzMzMzMAA' \
               'AAIOygts0ASslEAAAAIQBGZmZmZmZoAAAAIQHTRmZmZmZo='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_2d_array_float8(self):
        encoder, format_code = self.encode.encoders["array_float8"]
        bytesval = encoder([[0.1, 0.2], [0.3, 0.4], [0.5, 0.6]])
        rval = 'AAAAAgAAAAAAAAK9AAAAAwAAAAEAAAACAAAAAQAAAAg/uZmZmZmZmgAAAAg/' \
               'yZmZmZmZmgAAAAg/0zMzMzMzMwAAAAg/2ZmZmZmZmgAAAAg/4AAAAAAAAAAA' \
               'AAg/4zMzMzMzMw=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_3d_array_float8(self):
        encoder, format_code = self.encode.encoders["array_float8"]
        list_3d = [[[1.1, 1.2, 1.3],
                    [2.1, 2.2, 2.3]],
                   [[3.1, 3.2, 3.3],
                    [4.1, 4.2, 4.3]],
                   [[5.1, 5.2, 5.3],
                    [6.1, 6.2, 6.3]]]

        bytesval = encoder(list_3d)
        rval = 'AAAAAwAAAAAAAAK9AAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAACD/xmZmZ' \
               'mZmaAAAACD/zMzMzMzMzAAAACD/0zMzMzMzNAAAACEAAzMzMzMzNAAAACEAB' \
               'mZmZmZmaAAAACEACZmZmZmZmAAAACEAIzMzMzMzNAAAACEAJmZmZmZmaAAAA' \
               'CEAKZmZmZmZmAAAACEAQZmZmZmZmAAAACEAQzMzMzMzNAAAACEARMzMzMzMz' \
               'AAAACEAUZmZmZmZmAAAACEAUzMzMzMzNAAAACEAVMzMzMzMzAAAACEAYZmZm' \
               'ZmZmAAAACEAYzMzMzMzNAAAACEAZMzMzMzMz'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_int8(self):
        encoder, format_code = self.encode.encoders["int8"]
        bytesval = encoder(0)
        self.assertEqual(bytesval, b'\x00\x00\x00\x00\x00\x00\x00\x00')

        bytesval = encoder(constants.MAX_INT8)
        self.assertEqual(bytesval, b'\x7f\xff\xff\xff\xff\xff\xff\xff')

        bytesval = encoder(constants.MIN_INT8)
        self.assertEqual(bytesval, b'\x80\x00\x00\x00\x00\x00\x00\x00')

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MAX_INT8 + 1)

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MIN_INT8 - 1)

    def test_encode_int4(self):
        encoder, format_code = self.encode.encoders["int4"]
        bytesval = encoder(0)
        self.assertEqual(bytesval, b'\x00\x00\x00\x00')

        bytesval = encoder(constants.MAX_INT4)
        self.assertEqual(bytesval, b'\x7f\xff\xff\xff')

        bytesval = encoder(constants.MIN_INT4)
        self.assertEqual(bytesval, b'\x80\x00\x00\x00')

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MAX_INT4 + 1)

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MIN_INT4 - 1)

    def test_encode_int2(self):
        encoder, format_code = self.encode.encoders["int2"]
        bytesval = encoder(0)
        self.assertEqual(bytesval, b'\x00\x00')

        bytesval = encoder(constants.MAX_INT2)
        self.assertEqual(bytesval, b'\x7f\xff')

        bytesval = encoder(constants.MIN_INT2)
        self.assertEqual(bytesval, b'\x80\x00')

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MAX_INT2 + 1)

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MIN_INT2 - 1)

    def test_encode_1d_array_int4(self):
        encoder, format_code = self.encode.encoders["array_int4"]
        bytesval = encoder([constants.MIN_INT4, -1, 0, 1, 2, 3, 4,
                           constants.MAX_INT4])
        rval = 'AAAAAQAAAAAAAAAXAAAACAAAAAEAAAAEgAAAAAAAAAT/////AAAABAAA' \
               'AAAAAAAEAAAAAQAAAAQAAAACAAAABAAAAAMAAAAEAAAABAAAAAR/////'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_2d_array_int4(self):
        encoder, format_code = self.encode.encoders["array_int4"]
        bytesval = encoder([[1, 1, 1], [2, 2, 2]])
        rval = 'AAAAAgAAAAAAAAAXAAAAAgAAAAEAAAADAAAAAQAAAAQAAAABAAAABA' \
               'AAAAEAAAAEAAAAAQAAAAQAAAACAAAABAAAAAIAAAAEAAAAAg=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_3d_array_int4(self):
        encoder, format_code = self.encode.encoders["array_int4"]
        list_3d = [
            [[1, 1, 1],
             [2, 2, 2]],
            [[3, 3, 3],
             [4, 4, 4]],
            [[5, 5, 5],
             [6, 6, 6]]
        ]

        bytesval = encoder(list_3d)
        rval = 'AAAAAwAAAAAAAAAXAAAAAwAAAAEAAAACAAAAAQAAAAMAAAABAAAABAAAAAEA' \
               'AAAEAAAAAQAAAAQAAAABAAAABAAAAAIAAAAEAAAAAgAAAAQAAAACAAAABAAA' \
               'AAMAAAAEAAAAAwAAAAQAAAADAAAABAAAAAQAAAAEAAAABAAAAAQAAAAEAAAA' \
               'BAAAAAUAAAAEAAAABQAAAAQAAAAFAAAABAAAAAYAAAAEAAAABgAAAAQAAAAG'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_1d_array_int2(self):
        encoder, format_code = self.encode.encoders["array_int2"]
        bytesval = encoder([constants.MIN_INT2, -1, 0, 1, 2, 3, 4,
                           constants.MAX_INT2])
        rval = 'AAAAAQAAAAAAAAAVAAAACAAAAAEAAAACgAAAAAAC//8AAA' \
               'ACAAAAAAACAAEAAAACAAIAAAACAAMAAAACAAQAAAACf/8='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_2d_array_int2(self):
        encoder, format_code = self.encode.encoders["array_int2"]
        bytesval = encoder([[1, 1], [2, 2], [3, 3]])
        rval = 'AAAAAgAAAAAAAAAVAAAAAwAAAAEAAAACAAAAAQAAAAI' \
               'AAQAAAAIAAQAAAAIAAgAAAAIAAgAAAAIAAwAAAAIAAw=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_3d_array_int2(self):
        encoder, format_code = self.encode.encoders["array_int2"]
        bytesval = encoder([[[1, 1], [2, 2]], [[3, 3], [4, 4]]])
        rval = 'AAAAAwAAAAAAAAAVAAAAAgAAAAEAAAACAAAAAQAAAAIAAAABAAAAAgA' \
               'BAAAAAgABAAAAAgACAAAAAgACAAAAAgADAAAAAgADAAAAAgAEAAAAAgAE'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_1d_array_int8(self):
        encoder, format_code = self.encode.encoders["array_int8"]
        bytesval = encoder([constants.MIN_INT8, -1, 0, 1, 2, 3, 4,
                           constants.MAX_INT8])
        rval = 'AAAAAQAAAAAAAAAUAAAACAAAAAEAAAAIgAAAAAAAAAAAAAAI////' \
               '//////8AAAAIAAAAAAAAAAAAAAAIAAAAAAAAAAEAAAAIAAAAAAAA' \
               'AAIAAAAIAAAAAAAAAAMAAAAIAAAAAAAAAAQAAAAIf/////////8='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_2d_array_int8(self):
        encoder, format_code = self.encode.encoders["array_int8"]
        bytesval = encoder([[1, 1], [2, 2], [3, 3]])
        rval = 'AAAAAgAAAAAAAAAUAAAAAwAAAAEAAAACAAAAAQAAAAgAAAAAAAAA' \
               'AQAAAAgAAAAAAAAAAQAAAAgAAAAAAAAAAgAAAAgAAAAAAAAAAgAA' \
               'AAgAAAAAAAAAAwAAAAgAAAAAAAAAAw=='
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_3d_array_int8(self):
        encoder, format_code = self.encode.encoders["array_int8"]
        bytesval = encoder([[[1, 1], [2, 2]], [[3, 3], [4, 4]]])
        rval = 'AAAAAwAAAAAAAAAUAAAAAgAAAAEAAAACAAAAAQAAAAIAAAABAAAA' \
               'CAAAAAAAAAABAAAACAAAAAAAAAABAAAACAAAAAAAAAACAAAACAAA' \
               'AAAAAAACAAAACAAAAAAAAAADAAAACAAAAAAAAAADAAAACAAAAAAA' \
               'AAAEAAAACAAAAAAAAAAE'
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)

    def test_encode_oid(self):
        return self._test_generic_uint4("oid")

    def test_encode_xid(self):
        return self._test_generic_uint4("xid")

    def _test_generic_uint4(self, name):
        encoder, format_code = self.encode.encoders[name]
        bytesval = encoder(9)
        self.assertEqual(bytesval, b'\x00\x00\x00\x09')

        bytesval = encoder(constants.MAX_UINT4)
        self.assertEqual(bytesval, b'\xff\xff\xff\xff')

        bytesval = encoder(constants.MIN_UINT4)
        self.assertEqual(bytesval, b'\x00\x00\x00\x00')

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MAX_UINT4 + 1)

        with self.assertRaises(errors.ThrumEncodeError):
            encoder(constants.MIN_UINT4 - 1)

    def test_encode_1d_array_oid(self):
        rval = 'AAAAAQAAAAAAAAAaAAAABgAAAAEAAAAEAAAAAAAAAAQAAA' \
               'ABAAAABAAAAAIAAAAEAAAAAwAAAAQAAAAEAAAABP////8='
        return self._test_generic_array_uint4("array_oid", rval)

    def test_encode_1d_array_xid(self):
        rval = 'AAAAAQAAAAAAAAAcAAAABgAAAAEAAAAEAAAAAAAAAAQAAA' \
               'ABAAAABAAAAAIAAAAEAAAAAwAAAAQAAAAEAAAABP////8='
        return self._test_generic_array_uint4("array_xid", rval)

    def _test_generic_array_uint4(self, name, rval):
        encoder, format_code = self.encode.encoders[name]
        bytesval = encoder([constants.MIN_UINT4, 1, 2, 3, 4,
                           constants.MAX_UINT4])
        expected = base64.b64decode(rval)
        self.assertEqual(bytesval, expected)
