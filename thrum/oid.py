#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

try:
    intern
except NameError:
    from sys import intern


class OID(object):
    """
    Cache a dictionary of Postgres field type name to OID and another of OID to
    Postgres field type name. In theory (some) of these values could be
    different from one database to another, although normally they are the
    same.

    @type types: C{dict}
    @ivar types: Dict with Postgres typenames as keys and OIDs as values

    @type oids: C{dict}
    @ivar oids: Dict with OIDs as keys and Postgres typenames as values
    """
    def __init__(self):
        self.types = {}
        self.oids = {
            16: "bool",
            17: "bytea",
            18: "char",
            19: "name",
            20: "int8",
            21: "int2",
            23: "int4",
            24: "regproc",  # Alias for OID (internal)
            25: "text",
            26: "oid",  # Object ID (internal)
            27: "tid",  # Tuple ID (internal)
            28: "xid",  # Transaction ID (internal)
            29: "cid",  # Command ID (internal)
            142: "xml",
            600: "point",
            601: "lseg",
            602: "path",
            603: "box",
            604: "polygon",
            628: "line",
            650: "cidr",
            700: "float4",
            701: "float8",
            705: "unknown",
            718: "circle",
            790: "money",
            829: "macaddr",
            869: "inet",
            1042: "bpchar",
            1043: "varchar",
            1082: "date",
            1083: "time",
            1114: "timestamp",
            1184: "timestamptz",
            1186: "interval",
            1266: "timetz",
            1560: "bit",
            1562: "varbit",
            1700: "numeric",
            1790: "refcursor",
            2249: "record",
            2278: "void",
            2287: "array_record",
            2202: "regprocedure",  # Alias for OID (internal)
            2203: "regoper",  # Alias for OID (internal)
            2204: "regoperator",  # Alias for OID (internal)
            2205: "regclass",  # Alias for OID (internal)
            2206: "regtype",  # Alias for OID (internal)
            2275: "cstring",
            2950: "uuid",
            114: "json",
            3802: "jsonb",
            22: "int2vector",
            30: "oidvector",
            143: "array_xml",
            199: "array_json",
            629: "array_line",
            651: "array_cidr",
            719: "array_circle",
            791: "array_money",
            1000: "array_bool",
            1001: "array_bytea",
            1002: "array_char",
            1003: "array_name",
            1005: "array_int2",
            1006: "array_int2vector",
            1007: "array_int4",
            1008: "array_regproc",
            1009: "array_text",
            1010: "array_tid",
            1011: "array_xid",
            1012: "array_cid",
            1013: "array_oidvector",
            1014: "array_bpchar",
            1015: "array_varchar",
            1016: "array_int8",
            1017: "array_point",
            1018: "array_lseg",
            1019: "array_path",
            1020: "array_box",
            1021: "array_float4",
            1022: "array_float8",
            1027: "array_polygon",
            1028: "array_oid",
            1040: "array_macaddr",
            1041: "array_inet",
            1115: "array_timestamp",
            1182: "array_date",
            1183: "array_time",
            1185: "array_timestamptz",
            1187: "array_interval",
            1231: "array_numeric",
            1263: "array_cstring",
            1270: "array_timetz",
            1561: "array_bit",
            1563: "array_varbit",
            2201: "array_refcursor",
            2207: "array_regprocedure",
            2208: "array_regoper",
            2209: "array_regoperator",
            2210: "array_regclass",
            2211: "array_regtype",
            2951: "array_uuid",
            3614: "tsvector",
            3807: "array_jsonb",
            3904: "int4range",
            3905: "array_int4range",
            3906: "numrange",
            3907: "array_numrange",
            3908: "tsrange",
            3909: "array_tsrange",
            3910: "tstzrange",
            3911: "array_tstzrange",
            3912: "daterange",
            3913: "array_daterange",
            3926: "int8range",
            3927: "array_int8range"
        }

        for oid, typname in self.oids.items():
            typname = intern(typname)
            self.types[typname] = oid
            setattr(self, typname, oid)

    def addprop(self, oid, typname):
        if typname.startswith("_"):
            typname = "array" + typname
        typname = intern(typname)
        setattr(self, typname, oid)
        self.oids[oid] = typname
        self.types[typname] = oid

    def get(self, oid):
        return self.oids.get(oid)
