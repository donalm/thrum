#!/usr/bin/env python

from thrum import binary


def Box(object):

    def decode(self, data, offset, length):
        """
        Extract geometric box data from bytes. The result is returned as a
        pair of (x, y) tuples, representing diagonally opposite corners.

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A tuple of points ((x1, x2), (y1, y2))
        @rtype: L{tuple}
        """
        x1, y1, x2, y2 = binary.dddd_unpack(data[offset:offset+length])
        return self._decode(x1, y1, x2, y2)

    def _decode(self, x1, y1, x2, y2):
            return ((x1, y1), (x2, y2))
