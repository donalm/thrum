#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
A decoder maps OIDs to methods that unpack the type of data the OID represents.
The decoder should be specific to a connection because some things can vary
from one connection to the next:
 - Dates may be integers or doubles
 - A given OID is not guaranteed to map to a given data type

Note that because client code may be passing a hostname to the connect method,
depending on DNS setup, different connections in a pool may be talking to
different servers.

Those servers may have subtlely different schemas, etc.
"""

import re
import six
import sys
import json
import uuid
import struct
import decimal
import datetime
import calendar

try:
    import ipaddress
except ImportError:
    ipaddress = None

try:
    import pghstore
except ImportError:
    pghstore = None

import collections

from . import interval
from . import tsvector
from . import constants
from . import tzutc
from . import tz
from . import binary
from . import geometry


class Decode(object):
    """
    Translate binary data received over the wire from Postgres into Python
    objects.
    """
    def __init__(self, oids, client_encoding=b"utf-8"):
        # self.client_encoding must be bytes, e.g.    b"utf-8"
        # self.uclient_encoding must be unicode, e.g. u"utf-8"
        try:
            self.client_encoding = client_encoding.encode("utf-8")
            self.uclient_encoding = client_encoding
        except:
            self.client_encoding = client_encoding
            self.uclient_encoding = client_encoding.decode("utf-8")

        self.oids = oids

        self.FC_TEXT = constants.FC_TEXT  # 0
        self.FC_BINARY = constants.FC_BINARY  # 1

        self.utc = tzutc.UTC()
        self.epoch = datetime.datetime(2000, 1, 1)
        self.epoch_tz = self.epoch.replace(tzinfo=self.utc)
        self.base_epoch_seconds = calendar.timegm(self.epoch.timetuple())
        self.epoch_ordinal = self.epoch.toordinal()

        self.infinity_microseconds = 2 ** 63 - 1
        self.minus_infinity_microseconds = -1 * self.infinity_microseconds - 1

        self.datetime_max_tz = datetime.datetime.max.replace(tzinfo=self.utc)
        self.datetime_min_tz = datetime.datetime.min.replace(tzinfo=self.utc)

        self.true = six.b("\x01")
        self.false = six.b("\x00")
        self.null = binary.i_pack(-1)

        self.translation_table = dict(zip(map(ord, six.u('{}')), six.u('[]')))
        self.whitespace_regex_py3 = re.compile("\s+")
        self.whitespace_regex_py2 = re.compile(b"\s+")
        self.python_version = sys.version_info[0]

        self.glbls = {"Decimal": decimal.Decimal}

        if six.PY2:
            self._true_bytes = "\x01"
        else:
            self._true_bytes = 1

        self._establish_postgres_types()
        self.Unpacker = collections.namedtuple("Unpacker",
                                               ('length',
                                                'template',
                                                'method'))
        self.optimized_unpackers = {}
        self.optimized()
        self.char_unpacker = self.Unpacker(1, 'c', None)

    def _establish_postgres_types(self):
        """
        Connect the Postgres OID to the data type (text or binary) and the
        method we use to unpack that data
        """
        OID = self.oids
        pg_types = {
            OID.bool:            (self.FC_BINARY, self.bool_decode),
            OID.bytea:           (self.FC_BINARY, self.bytea_decode),
            OID.name:            (self.FC_BINARY, self.text_decode),
            OID.int8:            (self.FC_BINARY, self.int8_decode),
            OID.int2:            (self.FC_BINARY, self.int2_decode),
            OID.int2vector:      (self.FC_BINARY, self.vector_decode),
            OID.int4:            (self.FC_BINARY, self.int4_decode),
            OID.int4range:       (self.FC_BINARY, self.int4range_decode),
            OID.int8range:       (self.FC_BINARY, self.int8range_decode),
            OID.text:            (self.FC_BINARY, self.text_decode),
            OID.oid:             (self.FC_BINARY, self.uint4_decode),
            OID.xid:             (self.FC_BINARY, self.uint4_decode),
            OID.json:            (self.FC_BINARY, self.json_decode),
            OID.float4:          (self.FC_BINARY, self.float4_decode),
            OID.float8:          (self.FC_BINARY, self.float8_decode),
            OID.unknown:         (self.FC_BINARY, self.text_decode),
            OID.macaddr:         (self.FC_BINARY, self.macaddr_decode),
            OID.money:           (self.FC_BINARY, self.money_decode),
            OID.inet:            (self.FC_BINARY, self.inet_decode),
            OID.cidr:            (self.FC_BINARY, self.inet_decode),
            OID.array_bool:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_oid:       (self.FC_BINARY, self._binary_array_decode),
            OID.array_xid:       (self.FC_BINARY, self._binary_array_decode),
            OID.array_name:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_int2:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_int4:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_text:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_bpchar:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_varchar:   (self.FC_BINARY, self._binary_array_decode),
            OID.array_char:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_int8:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_float4:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_float8:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_uuid:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_bit:       (self.FC_BINARY, self._binary_array_decode),
            OID.array_varbit:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_date:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_time:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_timetz:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_timestamp: (self.FC_BINARY, self._binary_array_decode),
            OID.array_interval:  (self.FC_BINARY, self._binary_array_decode),
            OID.array_bytea:     (self.FC_BINARY, self._binary_array_decode),
            OID.array_macaddr:   (self.FC_BINARY, self._binary_array_decode),
            OID.array_xml:       (self.FC_BINARY, self._binary_array_decode),
            OID.array_json:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_jsonb:     (self.FC_BINARY, self._binary_array_decode),
            OID.array_point:     (self.FC_BINARY, self._binary_array_decode),
            OID.array_circle:    (self.FC_BINARY, self._binary_array_decode),
            OID.array_lseg:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_path:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_line:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_box:       (self.FC_BINARY, self._binary_array_decode),
            OID.array_polygon:   (self.FC_BINARY, self._binary_array_decode),
            OID.array_cidr:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_inet:      (self.FC_BINARY, self._binary_array_decode),
            OID.array_money:     (self.FC_BINARY, self._binary_array_decode),
            OID.array_cstring:   (self.FC_BINARY, self._binary_array_decode),
            OID.array_numeric:   (self.FC_BINARY, self._binary_array_decode),
            OID.polygon:         (self.FC_BINARY, self.polygon_decode),
            OID.box:             (self.FC_BINARY, self.box_decode),
            OID.path:            (self.FC_BINARY, self.path_decode),
            OID.line:            (self.FC_BINARY, self.line_decode),
            OID.point:           (self.FC_BINARY, self.point_decode),
            OID.lseg:            (self.FC_BINARY, self.lseg_decode),
            OID.circle:          (self.FC_BINARY, self.circle_decode),
            OID.bpchar:          (self.FC_BINARY, self.text_decode),
            OID.varchar:         (self.FC_BINARY, self.text_decode),
            OID.tsvector:        (self.FC_BINARY, self.tsvector_decode),
            OID.date:            (self.FC_BINARY, self.date_decode_binary),
            OID.time:            (self.FC_BINARY, self.time_decode_integer),
            OID.timetz:          (self.FC_BINARY, self.timetz_decode_integer),
            OID.timestamp:       (self.FC_BINARY, self.timestamp_decode_float),
            OID.timestamptz:     (self.FC_BINARY,
                                  self.timestamptz_decode_float),
            OID.interval:        (self.FC_BINARY,
                                  self.interval_decode_integer),
            OID.numeric:         (self.FC_BINARY, self.numeric_decode),
            OID.cstring:         (self.FC_BINARY, self.text_decode),
            OID.uuid:            (self.FC_BINARY, self.uuid_decode),
            OID.jsonb:           (self.FC_BINARY, self.jsonb_decode),
            OID.bit:             (self.FC_BINARY, self.bit_decode),
            OID.varbit:          (self.FC_BINARY, self.bit_decode),
        }

        if six.PY2:
            # python2 optimization
            pg_types[OID.bytea] = (self.FC_BINARY, buffer)

        self.pg_types = collections.defaultdict(
            lambda: (self.FC_BINARY, self.text_decode), pg_types
        )

    def optimized(self):
        """
        Set up some config to help with decoding binary data in the optimized
        path
        """
        OID = self.oids
        UP = self.Unpacker
        self.optimized_unpackers = {
            OID.bool:            UP(1,  '?', None),
            OID.int8:            UP(8,  'q', None),
            OID.int2:            UP(2,  'h', None),
            OID.int4:            UP(4,  'i', None),
            OID.oid:             UP(4,  'I', None),
            OID.xid:             UP(4,  'I', None),
            OID.float4:          UP(4,  'f', None),
            OID.float8:          UP(8,  'd', None),
            OID.macaddr:         UP(6,  'BBBBBB', self._macaddr_decode),
            OID.money:           UP(8,  '9', self._money_decode),
            OID.box:             UP(32, 'dddd', self._box_decode),
            OID.line:            UP(24, 'ddd', None),
            OID.point:           UP(16, 'dd', None),
            OID.lseg:            UP(32, 'dddd', None),
            OID.circle:          UP(24, 'ddd', self._circle_decode),
            OID.date:            UP(4,  'i', self._date_decode_binary),
            OID.time:            UP(8,  'q', self._time_decode),
            OID.timetz:          UP(12, 'qi', self._timetz_decode),
            OID.timestamp:       UP(8,  'q', self._timestamp_decode_integer),
            OID.timestamptz:     UP(8,  'q', self._timestamptz_decode_integer),
            OID.interval:        UP(16, 'qii', self._interval_decode_integer)
        }

    def update_client_encoding(self, encoding):
        """
        Update our client encoding (e.g. 'utf-8'), normally based on the value
        sent to us by the server as part of a PARAMETER_STATUS message
        a datetime.datetime with no timezone data

        @param data: A binary-packed string representation of a 64-bit integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance
        @rtype: L{datetime.datetime}
        """
        self.encodings = constants.pg_to_py
        self.client_encoding = constants.pg_to_py.get(encoding, encoding)

    def register_hstore(self, oid):
        self.pg_types[self.oids.hstore] = (self.FC_TEXT, self.hstore_decode)

    def timestamps_are_integers(self):
        """
        Modify the instance to expect Postgres to send time data as integers.
        This is the default.
        """
        OID = self.oids
        self.pg_types[OID.timestamp] = (self.FC_BINARY,
                                        self.timestamp_decode_integer)
        self.pg_types[OID.timestamptz] = (self.FC_BINARY,
                                          self.timestamptz_decode_integer)
        self.pg_types[OID.interval] = (self.FC_BINARY,
                                       self.interval_decode_integer)
        self.pg_types[OID.time] = (self.FC_BINARY,
                                   self.time_decode_integer)
        self.pg_types[OID.timetz] = (self.FC_BINARY,
                                     self.timetz_decode_integer)

        self.optimized_unpackers[OID.time] = \
            self.Unpacker(8, 'q', self._time_decode)
        self.optimized_unpackers[OID.timetz] = \
            self.Unpacker(12, 'qi', self._timetz_decode)
        self.optimized_unpackers[OID.timestamp] = \
            self.Unpacker(8,  'q', self._timestamp_decode_integer)
        self.optimized_unpackers[OID.timestamptz] = \
            self.Unpacker(8,  'q', self._timestamptz_decode_integer)
        self.optimized_unpackers[OID.interval] = \
            self.Unpacker(16, 'qii', self._interval_decode_integer)

    def timestamps_are_floats(self):
        """
        Modify the instance to expect Postgres to send time data as floats.
        This is deprecated by Postgres and is not well supported by Thrum.
        """
        OID = self.oids
        self.pg_types[OID.timestamp] = (self.FC_BINARY,
                                        self.timestamp_decode_float)
        self.pg_types[OID.timestamptz] = (self.FC_BINARY,
                                          self.timestamptz_decode_float)
        self.pg_types[OID.interval] = (self.FC_BINARY,
                                       self.interval_decode_float)
        self.pg_types[OID.time] = (self.FC_BINARY,
                                   self.time_decode_float)
        self.pg_types[OID.timetz] = (self.FC_BINARY,
                                     self.timetz_decode_float)

        self.optimized_unpackers[OID.time] = \
            self.Unpacker(8, 'd', self._time_decode)
        self.optimized_unpackers[OID.timetz] = \
            self.Unpacker(12, 'di', self._timetz_decode)
        self.optimized_unpackers[OID.timestamp] = \
            self.Unpacker(8,  'd', self._timestamp_decode_float)
        self.optimized_unpackers[OID.timestamptz] = \
            self.Unpacker(8,  'd', self._timestamptz_decode_float)
        self.optimized_unpackers[OID.interval] = \
            self.Unpacker(16, 'dii', self._interval_decode_float)

    def numerics_are_text(self):
        """
        Modify the instance to expect Postgres to send numeric data as text.
        Currently the default is 'binary', but it would make more sense to
        use text.

        Tom Lane writes:
        Personally I would suggest using text format instead of binary, at
        least for "numeric"-type values.  There really is not enough gain
        from dealing with the binary format to justify doing the work and
        dealing with possible future incompatibilities.  Binary format makes
        sense to me for simple integers and maybe for floats.
        https://goo.gl/n5kRjJ
        """
        self.pg_types[self.oids.numeric] = (self.FC_TEXT, self.numeric_decode)
        if six.PY2:
            self.pg_types[self.oids.array_numeric] = (self.FC_BINARY,
                                                      self.numeric_array_decode)
        else:
            self.pg_types[self.oids.array_numeric] = (self.FC_BINARY,
                                                      self.numeric_array_decode_py3)

    def numerics_are_binary(self):
        """
        Modify the instance to expect Postgres to send numeric data as binary.
        Currently the default is 'binary', but it would make more sense to
        use text. See the note above under 'numerics_are_text'
        """

        self.pg_types[self.oids.numeric] = (self.FC_BINARY,
                                            self.numeric_decode_binary)
        self.pg_types[self.oids.array_numeric] = (self.FC_BINARY,
                                                  self._binary_array_decode)

    def timestamp_decode_integer(self, data, offset, length):
        """
        Convert a binary-packed string representation of a 64-bit integer into
        a datetime.datetime with no timezone data

        @param data: A binary-packed string representation of a 64-bit integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance
        @rtype: L{datetime.datetime}
        """
        micros = binary.q_unpack(data, offset)[0]
        return self._timestamp_decode_integer(micros)

    def _timestamp_decode_integer(self, micros):
        try:
            return self.epoch + datetime.timedelta(microseconds=micros)
        except OverflowError as e:
            if micros == self.infinity_microseconds:
                return datetime.datetime.max
            elif micros == self.minus_infinity_microseconds:
                return datetime.datetime.min
            else:
                raise e

    def timestamp_decode_float(self, data, offset, length):
        """
        Convert a binary-packed string representation of a double-precision
        float to a datetime.datetime with no timezone data

        @param data: A binary-packed string representation of a double
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance
        @rtype: L{datetime.datetime}

        data is double-precision float representing seconds since 2000-01-01
        """
        data = binary.d_unpack(data, offset)[0]
        return self._timestamp_decode_float(data)

    def _timestamp_decode_float(self, data):
        ts = self.base_epoch_seconds + data
        return datetime.datetime.utcfromtimestamp(ts)

    def timestamptz_decode_integer(self, data, offset, length):
        """
        Convert a binary-packed string representation of a 64-bit integer into
        a datetime.datetime in the UTC timezone.

        @param data: A binary-packed string representation of a 64-bit integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance with UTC timezone
        @rtype: L{datetime.datetime}
        """
        micros = binary.q_unpack(data, offset)[0]
        return self._timestamptz_decode_integer(micros)

    def _timestamptz_decode_integer(self, micros):
        try:
            return self.epoch_tz + datetime.timedelta(microseconds=micros)
        except OverflowError as e:
            if micros == self.infinity_microseconds:
                return self.datetime_max_tz
            elif micros == self.minus_infinity_microseconds:
                return self.datetime_min_tz
            else:
                raise e

    def timestamptz_decode_float(self, data, offset, length):
        """
        Convert a binary-packed string representation of a double-precision
        float into a datetime.datetime in the UTC timezone.

        @param data: A binary-packed string representation of a double
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance with UTC timezone
        @rtype: L{datetime.datetime}
        """
        data = binary.d_unpack(data, offset)[0]
        return self._timestamptz_decode_float(data)

    def _timestamptz_decode_float(self, data):
        _datetime = self._timestamp_decode_float(data)
        return _datetime.replace(tzinfo=self.utc)

    def interval_decode_integer(self, data, offset, length):
        """
        Convert a binary-packed string representation of three integers
        (representing microseconds, days and months) into an interval.Interval
        instance

        @param data: A binary-packed string representation of three integers
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance
        @rtype: L{datetime.datetime}
        """
        microseconds, days, months = binary.qii_unpack(data, offset)
        return self._interval_decode_integer(microseconds, days, months)

    def _interval_decode_integer(self, microseconds, days, months):
        return interval.Interval(microseconds, days, months)

    def interval_decode_float(self, data, offset, length):
        """
        Convert a binary-packed string representation of a double and two
        integers (representing seconds, days and months) into an
        interval.Interval instance

        @param data: A binary-packed string representation of three integers
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A datetime.datetime instance
        @rtype: L{datetime.datetime}
        """
        seconds, days, months = binary.dii_unpack(data, offset)
        return self._interval_decode_float(seconds, days, months)

    def _interval_decode_float(self, seconds, days, months):
        microseconds = int(seconds * 1000000)
        return interval.Interval(microseconds, days, months)

    def int8_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of a Long integer
        to an int

        @param data: A binary-packed string representation of an integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: An integer
        @rtype: L{int}
        """
        return binary.q_unpack(data, offset)[0]

    def int2_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of an integer
        to an int

        @param data: A binary-packed string representation of an integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: An integer
        @rtype: L{int}
        """
        return binary.h_unpack(data, offset)[0]

    def int4_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of an integer
        to an int

        @param data: A binary-packed string representation of an integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: An integer
        @rtype: L{int}
        """
        return binary.i_unpack(data, offset)[0]

    def uint4_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of an unsigned
        integer to an int

        @param data: A binary-packed string representation of an integer
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: An integer
        @rtype: L{int}
        """
        return binary.I_unpack(data, offset)[0]

    def int4range_decode(self, data, offset, length):
        """
        Convert a binary-packed string to a pair of integers representing a
        range. Either or both may be None, meaning that the range has no upper
        or lower bound.

        @param data: A binary-packed string representation of a float
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Character length of the data to use
        @type length: L{int}

        @return: A tuple of (start, end), which may be integers or None
        @rtype: L{tuple}
        """
        if length == 1 and binary.b_unpack(data, offset)[0] == 24:
            return None, None

        if length == 9:
            prefix, a, b = binary.bii_unpack(data, offset)
            if prefix == 8:
                return None, b
            if prefix == 18:
                return b, None

        prefix, a, start, b, end = binary.biiii_unpack(data, offset)
        return start, end

    def int8range_decode(self, data, offset, length):
        """
        Convert a binary-packed string to a pair of integers representing a
        range. Either or both may be None, meaning that the range has no upper
        or lower bound.

        @param data: A binary-packed string representation of a float
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Character length of the data to use
        @type length: L{int}

        @return: A tuple of (start, end), which may be integers or None
        @rtype: L{tuple}
        """
        if length == 1 and binary.b_unpack(data, offset)[0] == 24:
            return None, None

        if length == 13:
            prefix, a, b = binary.biq_unpack(data, offset)
            if prefix == 8:
                return None, b
            if prefix == 18:
                return b, None

        prefix, a, start, b, end = binary.biqiq_unpack(data, offset)
        return start, end

    def float4_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of a float
        to a float

        @param data: A binary-packed string representation of a float
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A float
        @rtype: L{float}
        """
        return binary.f_unpack(data, offset)[0]

    def float8_decode(self, data, offset, length):
        """
        Convert a binary-packed string representation of a double
        to a float

        @param data: A binary-packed string representation of a double
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: A float
        @rtype: L{float}
        """
        return binary.d_unpack(data, offset)[0]

    def int_in(self, data, offset, length):
        """
        Extract an int from a string

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An int
        @rtype: L{int}
        """
        return int(data[offset: offset + length])

    def _to_dec_or_none(self, val):
        if val is None:
            return None
        return decimal.Decimal(val)

    def numeric_array_decode_py3(self, data, offset, length):
        """
        Extract an array of Decimals from bytes

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An list (perhaps nested) of Decimals
        @rtype: L{list}
        """
        nullchar = ":"
        nanchar = "!"
        odata = data[offset:offset+length]
        data = odata.upper()
        data = data.decode("utf-8").replace("NULL", nullchar).replace("NAN", nanchar)
        data = self.whitespace_regex_py3.sub("", data)

        cur_number = ''

        output = []
        stack = []
        cur_list = output
        stack.append(cur_list)

        if data[0] != '{':
            raise Exception("Unexpected value for array: %s" % (odata,))

        for index, char in enumerate(data[1:]):
            if char == '{':
                l = list()
                cur_list.append(l)
                stack.append(l)
                cur_list = l
            elif char in (',', '}'):
                cur_list.append(self._to_dec_or_none(cur_number))
                cur_number = ''
                if char == '}':
                    stack.pop()
                    if stack:
                        cur_list = stack[-1]
                elif char == ',':
                    pass
            elif char in '0123456789E.-':
                cur_number += char
            elif char == nullchar:
                cur_number = None
            elif char == nanchar:
                cur_number = 'NaN'

        return output

    def numeric_array_decode(self, data, offset, length):
        """
        Extract an array of Decimals from bytes

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An list (perhaps nested) of Decimals
        @rtype: L{list}
        """
        nullchar = b":"
        nanchar = b"!"
        odata = data[offset:offset+length]
        data = odata.upper()
        data = data.replace(b"NULL", nullchar).replace(b"NAN", nanchar)
        data = self.whitespace_regex_py2.sub(b"", data)

        cur_number = b''

        output = []
        stack = []
        cur_list = output
        stack.append(cur_list)

        if data[0] != b'{':
            raise Exception("Unexpected value for array: %s" % (odata,))

        for index, char in enumerate(data[1:]):
            if char == b'{':
                l = list()
                cur_list.append(l)
                stack.append(l)
                cur_list = l
            elif char in (b',', b'}'):
                cur_list.append(self._to_dec_or_none(cur_number))
                cur_number = b''
                if char == b'}':
                    stack.pop()
                    if stack:
                        cur_list = stack[-1]
                elif char == b',':
                    pass
            elif char in b'0123456789E.-':
                cur_number += char
            elif char == nullchar:
                cur_number = None
            elif char == nanchar:
                cur_number = b"NaN"

        return output

    def bytea_decode(self, data, offset=0, length=0):
        """
        Cast received data from bytes to bytes

        @param data: Bytes
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of bytes to return
        @type length: L{int}

        @return: Bytes
        @rtype: L{bytes}
        """
        return data[offset:offset + length]

    def text_decode(self, data, offset, length):
        """
        Decode received data using client encoding and return resulting
        Unicode object

        @param data: Bytes
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: Unicode object
        @rtype: L{unicode}
        """
        try:
            return six.text_type(data[offset: offset + length],
                                 self.uclient_encoding)
        except Exception as e:
            raise

    def json_decode(self, data, offset, length):
        """
        Decode received data using client encoding and parse resulting json
        data into a Python object

        @param data: Bytes
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of bytes of data to extract
        @type length: L{int}

        @return: Python object
        @rtype: L{object}
        """
        json_data = data[offset:offset+length].decode(self.uclient_encoding)
        return json.loads(json_data)

    def bool_decode(self, data, offset, length):
        """
        Interpret the single character at 'offset' in a bytestring as a
        boolean object

        @param data: Bytes
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Ignored
        @type length: L{int}

        @return: Boolean object
        @rtype: L{bool}
        """
        return data[offset] == self._true_bytes

    def jsonb_decode(self, data, offset, length):
        """
        Extract json data from bytes

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An inflated JSON object: str, list or dict
        @rtype: L{dict}
        """
        # We can ignore the json_version for now. There is only one. The value
        # is provided to future-proof the protocol
        # json_version = data[offset]
        json_data = data[offset+1:offset+length].decode(self.uclient_encoding)
        return json.loads(json_data)

    def circle_decode(self, data, offset, length):
        """
        Extract geometric circle data from bytes

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A nested tuple: ((x, y), radius)
        @rtype: L{tuple}
        """
        x, y, radius = binary.ddd_unpack(data, offset)
        return self._circle_decode(x, y, radius)

    def _circle_decode(self, x, y, radius):
        return ((x, y), radius)

    def line_decode(self, data, offset, length):
        """
        Extract geometric line data from bytes. The Postgres docs note that
        lines are represented by the linear equation Ax + By + C = 0,
        where A and B are not both zero.

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A triple of integers (a, b, c)
        @rtype: L{tuple}
        """
        return binary.ddd_unpack(data, offset)

    def polygon_decode(self, data, offset, length):
        """
        Extract geometric polygon data from bytes. The result is returned as a
        list of (x, y) tuples.

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A list of points (x, y)
        @rtype: L{list}
        """
        pointcount = binary.i_unpack(data, offset)[0]
        offset += 4
        points = []
        for i in range(pointcount):
            x, y = binary.dd_unpack(data, offset)
            points.append((x, y))
            offset += 16

        return points

    def box_decode(self, data, offset, length):
        """
        Extract geometric box data from bytes. The result is returned as a
        pair of (x, y) tuples, representing diagonally opposite corners.

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A tuple of points ((x1, x2), (y1, y2))
        @rtype: L{tuple}
        """
        x1, y1, x2, y2 = binary.dddd_unpack(data, offset)
        return self._box_decode(x1, y1, x2, y2)

    def _box_decode(self, x1, y1, x2, y2):
        return ((x1, y1), (x2, y2))

    def path_decode(self, data, offset, length):
        """
        Extract geometric path data from bytes. The result is returned as a
        Path object, which is just a list of (x, y) tuples, representing
        points on the path, with one additional 'closed' attribute which
        may be True or False

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A list of points [(x1, x2), ... (yN, yN)]
        @rtype: L{list}
        """
        endpoint = offset + length
        closed = data[offset] in (1, b'\x01')
        points = binary.i_unpack(data, offset+1)[0]

        offset = offset + 5
        path = geometry.Path(closed)

        for index in range(points):
            pointx, pointy = binary.dd_unpack(data, offset)
            path.append((pointx, pointy))
            offset += 16
            if offset >= endpoint:
                break

        return path

    def point_decode(self, data, offset, length):
        """
        Extract geometric point data from bytes. The result is returned as a
        tuple (x, y)

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A tuple (x, y)
        @rtype: L{tuple}
        """
        return binary.dd_unpack(data, offset)

    def lseg_decode(self, data, offset, length):
        """
        Extract line segment point data from bytes. The result is returned as
        four double precision floats

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A tuple (x1, y1, x2, y2)
        @rtype: L{tuple}
        """
        return binary.dddd_unpack(data, offset)

    def tsvector_decode(self, data, offset, length):
        """
        Extract tsvector data from bytes. The result is returned as
        a TsVector object.

        @param data: A string including an integer value
        @type data: L{str}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A TsVector object
        @rtype: L{TsVector}
        """
        count = binary.i_unpack(data, offset)[0]
        tsv = tsvector.TsVector()
        offset += 4
        weights = (b'D', b'C', b'B', b'A')
        for index in range(count):
            null_pos = data[offset:].index(b'\x00')
            word = data[offset:offset+null_pos].decode(self.client_encoding)
            offset = offset + null_pos + 1
            npositions = binary.h_unpack(data, offset)[0]
            offset += 2
            for p in range(npositions):
                extended_data = binary.H_unpack(data, offset)[0]
                offset += 2
                position = extended_data & 16383
                weight = weights[extended_data >> 14]
                tsv.normalize_and_add(word, position=position, weight=weight)
            if npositions == 0:
                tsv.add_entry(word)

        return tsv

    def timetz_decode_float(self, data, offset, length):
        """
        Extract a datetime from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A datetime object
        @rtype: L{datetime.datetime}
        """

        micros, timeoffset = binary.di_unpack(data, offset)
        return self._timetz_decode(micros, timeoffset)

    def timetz_decode_integer(self, data, offset, length):
        """
        Extract a datetime from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A datetime object
        @rtype: L{datetime.datetime}
        """
        micros, timeoffset = binary.qi_unpack(data, offset)
        return self._timetz_decode(micros, timeoffset)

    def _timetz_decode(self, micros, timeoffset):
        micros = int(micros)
        tzinfo = tz.PgTimeZone(seconds_west_of_UTC=timeoffset)
        hours = micros // 3600000000
        minutes = (micros % 3600000000) // 60000000
        seconds = (micros % 60000000) // 1000000
        microseconds = micros % 1000000
        return datetime.time(hours, minutes, seconds, microseconds, tzinfo)

    def time_decode_float(self, data, offset, length):
        """
        Extract a datetime from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A datetime object
        @rtype: L{datetime.datetime}
        """

        micros = int(binary.d_unpack(data, offset)[0])
        return self._time_decode(micros)

    def time_decode_integer(self, data, offset, length):
        """
        Extract a datetime from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A datetime object
        @rtype: L{datetime.datetime}
        """

        micros = binary.q_unpack(data, offset)[0]
        return self._time_decode(micros)

    def _time_decode(self, micros):
        hours = micros // 3600000000
        minutes = (micros % 3600000000) // 60000000
        seconds = (micros % 60000000) // 1000000
        microseconds = micros % 1000000
        return datetime.time(hours, minutes, seconds, microseconds)

    def date_decode_binary(self, data, offset, length):
        """
        Extract a date from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A date object
        @rtype: L{datetime.date}
        """
        offset_ordinal = binary.i_unpack(data, offset)[0]
        return self._date_decode_binary(offset_ordinal)

    def _date_decode_binary(self, offset_ordinal):
        date_ordinal = offset_ordinal + self.epoch_ordinal
        return datetime.date.fromordinal(date_ordinal)

    def date_decode_text(self, data, offset, length):
        """
        Extract a date from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A date object
        @rtype: L{datetime.date}
        """
        year_str = data[offset:offset + 4].decode(self.client_encoding)
        if year_str == 'infi':
            return datetime.date.max
        elif year_str == '-inf':
            return datetime.date.min
        else:
            return datetime.date(
                int(year_str), int(data[offset + 5:offset + 7]),
                int(data[offset + 8:offset + 10]))

    def numeric_decode(self, data, offset, length):
        """
        Extract a Decimal from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A Decimal object
        @rtype: L{decimal.Decimal}
        """
        return decimal.Decimal(
            data[offset: offset + length].decode(self.uclient_encoding))

    def numeric_decode_binary(self, data, offset, length):
        """
        Extract a Decimal from binary-encoded parameters

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A Decimal object
        @rtype: L{decimal.Decimal}
        """

        val = data[offset: offset + length]

        # If the result is NaN we can return immediately
        sign = struct.unpack("!h", val[4:6])[0]
        if sign == constants.NUMERIC_NAN:
            return decimal.Decimal("NaN")

        if sign == constants.NUMERIC_NEG:
            sign = 1
        elif sign == constants.NUMERIC_POS:
            sign = 0
        else:
            raise Exception("Unexpected value for sign '%s'" % (sign,))

        ndigits = struct.unpack("!h", val[0:2])[0]
        weight = struct.unpack("!h", val[2:4])[0]
        dscale = struct.unpack("!h", val[6:8])[0]

        try:
            unpacker = binary.Cache.unpack_shorts[ndigits]
        except KeyError:
            unpacker = binary.Cache._get_unpack_shorts_for_index(ndigits)

        unpacked = unpacker(val[8:])

        digits = []
        element = unpacked[0]

        total_digit_count = 0
        if element > 999:
            total_digit_count = 4
            digits.extend((element // 1000,
                          (element % 1000) // 100,
                          (element % 100) // 10,
                          element % 10))
        elif element > 99:
            total_digit_count = 3
            digits.extend(((element % 1000) // 100,
                          (element % 100) // 10,
                          element % 10))
        elif element > 9:
            total_digit_count = 2
            digits.extend(((element % 100) // 10,
                          element % 10))
        else:
            total_digit_count = 1
            digits.append(element % 10)

        total_digit_count += weight * 4
        digilen = total_digit_count + dscale

        for index, element in enumerate(unpacked[1:]):
            digits.extend((element // 1000,
                          (element % 1000) // 100,
                          (element % 100) // 10,
                          element % 10))

        digits = tuple(digits[0:digilen])
        d = decimal.Decimal((sign, digits, dscale*-1))
        return d

    def hstore_decode(self, data, offset, length):
        """
        Extract a dict from binary-encoded hstore data

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A dict
        @rtype: L{dict}
        """
        if pghstore is None:
            return data[offset:offset+length]
        return pghstore.loads(data[offset:offset+length])

    def vector_decode(self, data, offset, length):
        """
        Extract a list of int2 from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A list
        @rtype: L{list}
        """
        return self._binary_array_decode(data, offset, length,
                                         self.int2_decode)

    def bit_decode(self, data, offset, length):
        """
        Extract a binary string from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A string of 1s and 0s, e.g. '10011001'
        @rtype: L{str}
        """
        lval = binary.i_unpack(data, offset)[0]

        ends = offset + length
        offset = offset + 4
        length = length - 4
        if six.PY2:
            Bs = 'B' * length
            chars = struct.unpack_from('!'+Bs, data, offset)
        else:
            # In a Python3 interpreter, when we slice a byte from a bytes
            # instance, Python already interprets that as an integer, so
            # we must skip the B_unpack step
            chars = data[offset:ends]
            #chars = [c for c in dataset]

        return ''.join(["{0:08b}".format(c) for c in chars])[0:lval]

    def uuid_decode(self, data, offset, length):
        """
        Extract a UUID from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A UUID
        @rtype: L{uuid.UUID}
        """
        return uuid.UUID(bytes=data[offset:offset+length])

    def money_decode(self, data, offset, length):
        """
        Extract a money value from a bytes, returning the value as a
        (dollars, cents) tuple

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A tuple
        @rtype: L{tuple}
        """
        data = binary.q_unpack(data, offset)[0]
        return self._money_decode(data)

    def _money_decode(self, data):
        dollars = data // 100
        cents = data % 100
        return dollars, cents

    def inet_decode(self, data, offset, length):
        """
        Extract an IP host or network address from bytes. Return one of:
        ipaddress.IPv4Network
        ipaddress.IPv6Network
        ipaddress.IPv4Address
        ipaddress.IPv6Address

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An ipaddress object
        @rtype: L{ipaddress.ip_network}
        """

        elements = binary.BBBB_unpack(data, offset)
        family, prefixlen, is_cidr, addrlen = elements

        if family == constants.PGSQL_AF_INET6:
            addr = binary.HHHHHHHH_unpack(data, offset+4)
            straddr = u':'.join([u"%0.4X" % x for x in addr])
            if is_cidr == 0:
                return ipaddress.IPv6Address(straddr)

            return ipaddress.IPv6Network(u"%s/%s" % (straddr, prefixlen,))
        else:
            addr = binary.BBBB_unpack(data, offset+4)
            straddr = u'.'.join([six.text_type(x) for x in addr])
            if is_cidr == 0:
                return ipaddress.IPv4Address(straddr)
            return ipaddress.IPv4Network(u"%s/%s" % (straddr, prefixlen,))

    def inet_in(self, data, offset, length):
        """
        Extract an ip address from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: An ip_network or ip_address instance
        @rtype: C{object}
        """
        inet_str = data[offset: offset + length].decode(self.client_encoding)
        if '/' in inet_str:
            return ipaddress.ip_network(inet_str, False)
        else:
            return ipaddress.ip_address(inet_str)

    def macaddr_decode(self, data, offset, length):
        """
        Extract a mac address from bytes

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A string representing an IP address
        @rtype: C{str}
        """
        a, b, c, d, e, f = binary.BBBBBB_unpack(data, offset)
        return self._macaddr_decode(a, b, c, d, e, f)

    def _macaddr_decode(self, a, b, c, d, e, f):
        return "%0.2X:%0.2X:%0.2X:%0.2X:%0.2X:%0.2X" % (a, b, c, d, e, f,)

    def _binary_array_decode(self, data, offset, length, decoder=None):
        """
        Extract an array from bytes. The bytes data will contain an OID for
        the datatype that the array contains. Individual array elements will
        be decoded and returned in the (possibly nested) list return value.

        @param data: A bytestring
        @type data: L{bytes}

        @param offset: Character offset into the data
        @type offset: L{int}

        @param length: Number of characters to extract
        @type length: L{int}

        @return: A string representing an IP address
        @rtype: C{str}
        """

        # Unpack 20 bytes into some integers
        try:
            ndimensions, data_offset, oid, dimensions, lower_bounds = \
                binary.iiiii_unpack(data, offset)
        except Exception as e:
            dimensions_bytes = data[offset:offset+4]
            if dimensions_bytes == b'\x00\x00\x00\x00':
                # An array with zero dimensions is the empty list
                return []
            raise

        offset += 20

        if decoder is None:
            decoder = self.pg_types[oid][1]

        dims = [dimensions]
        element_count = dimensions
        for i in range(ndimensions - 1):
            dimensions = binary.i_unpack(data, offset)[0]
            dims.append(dimensions)
            element_count = element_count * dimensions
            '''
            We don't care about 'lower_bounds'?
            lower_bounds = binary.i_unpack(data, offset+4)[0]
            '''
            offset += 8

        base = []
        next_location = [0] * ndimensions
        reverse_index = list(reversed(range(ndimensions)))

        for _ in range(element_count):

            parent = base
            for key in next_location[0:-1]:
                try:
                    parent = parent[key]
                except IndexError:
                    parent.append([])
                    parent = parent[-1]

            for i in reverse_index:
                element = next_location[i] + 1
                if element < dims[i]:
                    next_location[i] += 1
                    break
                next_location[i] = 0

            l = binary.i_unpack(data, offset)[0]
            offset += 4
            if l == -1:
                # NULL is represented as a length of -1 with no data
                parent.append(None)
                continue

            parent.append(decoder(data, offset, l))
            offset += l

        return base
