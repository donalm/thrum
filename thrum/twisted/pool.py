#!/usr/bin/env pypy

"""
The ConnectionPool class manages a pool of
thrum.twisted.protocol.PostgresProtocol objects. This class offers low level
interaction with a Postgres system, so that you can prepare statements, and
execute them many times with different arguments, etc.

The state is managed with a dict of connections whose keys are increasing
powers of 2, so 1, 2 4, 8, 16, 32 and so forth. The self._available is
modified using bit logic.

In theory the class should prefer to return lower-numbered connections, so that
if the system is not too busy the same connections should get used over and
over again. This should minimize context switching on the server side.

"""

import traceback
import collections
from twisted.internet import defer
from twisted.internet import reactor
from twisted.logger import Logger

from thrum import pypy
from . import connection

def log(l):
    fh = open("/tmp/thrum.log", "a")
    fh.write("%s\n" % (l,))
    fh.close()

def eb(f):
    log(f.getBriefTraceback())

class ConnectionPool(object):

    factory = defer.Deferred()
    log = Logger()

    def __init__(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs
        self._connections = {}
        self._connection_count = 0
        self._queue = collections.deque()
        self._listeners = {}
        self._listener_connection = None
        self._listener_keepalive_interval = 300
        self._listener_looping_call = None
        self._df = defer.Deferred
        self._reset()

    def shutdown(self, permanent=True):
        self._shutting_down = True

        _min = self.getmin()
        _max = self.getmax()

        self.setmin(0)
        self.setmax(0)

        disconnection_deferreds = []

        # Ensure that the connection is not 'available':
        for index, _connection in self._connections.items():
            self._available ^= index
            disconnection_deferreds.append(
                _connection.disconnect()
            )

        if self._listener_connection is not None:
            self._listener_connection.disconnect()

        disconnection_deferreds_result = defer.gatherResults(disconnection_deferreds)

        if permanent:
            # If we are shutting down permanently we should error the queries in our
            # queue so that errors can be correctly returned to (for example) RPC
            # clients.
            for df in self._queue:
                try:
                    df.errback(Exception("Thrum pool shutdown requested."))
                except Exception:
                    # If a method in a errback chain raises an exception, we should not
                    # allow that to prevent us calling .errback on other pending queries
                    # or continuing with our orderly shutdown, so we just 'pass'
                    # TODO: Log this exception
                    pass
            clear_queue = True

        # Reset this instance so that the user can safely call .start() again
        self._reset(_min, _max, clear_queue=clear_queue)

        return disconnection_deferreds_result

    def _reset(self, minval=1, maxval=8, clear_queue=False):
        self._available = 0
        self._connections.clear()
        self._connection_count = 0
        self._min = minval
        self._max = maxval
        self._shutting_down = False
        self._df = defer.Deferred

        # If we hope to start up again and resume processing the queries in our
        # queue, we should not clear it. If we are shutting down, we should
        # clear the queue
        if clear_queue:
            self._queue.clear()

    def getmin(self):
        return self._min

    def setmin(self, value):
        if value > self.max:
            msg = "min value %s is greater than current max value %s"
            raise Exception(msg % (value, self.max,))
        self._min = value

    def getmax(self):
        return self._max

    def setmax(self, value):
        if value < self.min:
            msg = "max value %s is less than current min value %s"
            raise Exception(msg % (value, self.min,))
        self._max = value

    def delmin(self):
        raise Exception("can't delete min")

    def delmax(self):
        raise Exception("can't delete max")

    min = property(getmin, setmin, delmin, "min connections")
    max = property(getmax, setmax, delmax, "max connections")

    def start(self, min_connections=None, max_connections=None):
        self.min = min_connections or self._min
        self.max = max_connections or self._max
        l = []
        for x in range(self.min):
            #log("start: make conn: %s" % (x,))
            df = self.add_connection()
            df.addCallback(self.release_connection)
            l.append(df)
        df = defer.gatherResults(l, consumeErrors=True)
        df.addCallback(self.rself)
        return df

    def rself(self, _):
        #log("RETURN SELF")
        return self

    def add_connection(self, index=None):
        #log("add_connection: %s" % (1,))
        if self._connection_count >= self.max:
            msg = "max connections (%s) already reached"
            #log(msg % (self.max,))
            raise Exception(msg % (self.max,))

        if index is None:
            index = self.next_index()
        self._connections[index] = None
        self._connection_count = len(self._connections)
        conn_df = self.make_connection()
        conn_df.addCallback(self._register_connection, index)
        return conn_df

    def make_connection(self):
        conn = connection.Connection()
        conn_df = conn.connect(*self._args, **self._kwargs)
        conn_df.addErrback(eb)
        return conn_df

    @defer.inlineCallbacks
    def replace_bad_connection(self, connection):
        index = connection.pool_key
        self._available = self._available & ~index
        # Although connection.disconnect returns a deferred, we don't need to
        # wait for it to succeed. We can add an errback to it though.
        try:
            df = connection.disconnect()
            df.addErrback(eb)
        except Exception:
            pass
        replacement_connection = yield self.add_connection(index=index)
        return self.release_connection(replacement_connection)

    def _register_connection(self, connection, index):
        #log("register_connection: %s" % (connection,))
        connection.pool_key = index
        self._connections[index] = connection
        return connection

    def release_connection(self, connection):
        try:
            if not connection.connected:
                # The connection dropped for some reason
                if not self._shutting_down:
                    self.replace_bad_connection(connection)
                return
        except Exception as e:
            self.log.error("Bad connection: {exc!r}", exc=e)
            return

        if self._shutting_down:
            return

        if self._queue:
            try:
                df = self._queue.popleft()
                df.callback(connection)
            except Exception as e:
                self.log.error("Unexpected error: {exc!r}", exc=e)
            return None

        self._available |= connection.pool_key

    def next_index(self):
        try:
            l = max(self._connections.keys())
            return l * 2
        except ValueError:
            return 1

    def acquire_connection(self):
        if self._available == 0:
            df = self._df()
            self._queue.append(df)
            try:
                xdf = self.add_connection()
                xdf.addCallback(self.release_connection)
            except Exception:
                pass
            return df

        value = self._available
        count = 0
        try:
            while 0 == value & 1:
                value >>= 1
                count += 1
            index = 1 << count
        except Exception:
            pypy.lprint(traceback.format_exc())

        self._available = self._available & ~index
        return defer.succeed(self._connections[index])

    '''
    Send or receive Asynchronous Notifications.
    '''
    @defer.inlineCallbacks
    def listen(self, channel, callback, ignore_local_notifications=False):

        empty_dict = {}
        channel_listeners = self._listeners.setdefault(channel, empty_dict)

        if channel_listeners == empty_dict:
            # It seems that we're not yet listening for this channel
            # so we must issue the query to postgres. First we create
            # a custom connection for listening for notifications.
            if self._listener_connection is None:
                self._listener_connection = yield self.make_connection()
                self._listener_connection.disconnected.addCallback(
                    self.reinstate_listeners
                )

            yield self._listener_connection.listen(
                channel,
                callback,
                ignore_local_notifications=ignore_local_notifications
            )

        channel_listeners[callback] = ignore_local_notifications
        defer.returnValue(None)

    @defer.inlineCallbacks
    def unlisten(self, channel, callback):
        channel_listeners = self._listeners.get(channel)
        if channel_listeners is None or callback not in channel_listeners:
            defer.returnValue(None)

        channel_listeners.pop(callback)
        yield self._listener_connection.unlisten(channel, callback)
        defer.returnValue(None)

    @defer.inlineCallbacks
    def notify(self, channel, value):
        if self._listener_connection is None:
            self._listener_connection = yield self.make_connection()
            self._listener_connection.disconnected.addCallback(
                self.reinstate_listeners
            )

        result = yield self._listener_connection.notify(channel, value)
        defer.returnValue(None)

    def reinstate_listeners(self, result=None):
        """Ensure all our 'listen' queries are re-issued after a disconnect.

        """
        if self._shutting_down:
            return

        old_listeners = self._listeners
        self._listeners = {}
        self._listener_connection = None

        def return_result(_):
            return result

        df_list = []
        for channel, channel_listener in old_listeners.items():
            for callback, ignore_local_notifications in channel_listener.items():
                df_list.append(
                    self.listen(channel, callback, ignore_local_notifications=ignore_local_notifications)
                )

        df = defer.gatherResults(df_list)
        df.addCallback(return_result)
        return df

    '''
    TxPostgres compatibility methods
    '''
    @defer.inlineCallbacks
    def runQuery(self, query, *args, **kwargs):
        """Get a connection from the pool and run a query.

        Args:
            connection_pool (pool.ConnectionPool): The connection pool.
            query (str): The query.

        Returns:
            list: A list of NamedTuples representing database records.
        """
        connection = yield self.acquire_connection()
        result = yield connection.runQuery(query, *args, **kwargs)
        reactor.callLater(0, self.release_connection, connection)
        defer.returnValue(result)

    @defer.inlineCallbacks
    def runOperation(self, query, *args, **kwargs):
        """Get a connection from the pool and run a query.

        Args:
            connection_pool (pool.ConnectionPool): The connection pool.
            query (str): The query.

        Returns:
            list: A list of NamedTuples representing database records.
        """
        connection = yield self.acquire_connection()
        result = yield connection.runOperation(query, *args, **kwargs)
        reactor.callLater(0, self.release_connection, connection)
        defer.returnValue(result)

    @defer.inlineCallbacks
    def runInteraction(self, interaction, *args, **kwargs):
        """Get a connection from the pool and run an interaction.

        Args:
            connection_pool (pool.ConnectionPool): The connection pool.
            interaction (callable): Method which takes a cursor as its 1st argument.

        Returns:
            list: A list of NamedTuples representing database records.
        """
        connection = yield self.acquire_connection()
        result = yield connection.runInteraction(interaction, *args, **kwargs)
        reactor.callLater(0, self.release_connection, connection)
        defer.returnValue(result)
