#!/usr/bin/env python

import re
import six
import time
import weakref
import traceback
import collections

from hashlib import md5

import scramp

from twisted.internet import defer
from twisted.python import threadpool
from twisted.logger import Logger
from termcolor import cprint

from collections import namedtuple, OrderedDict

try:
    from pg8000 import dbapi as pg8000_dbapi
except ImportError:
    pg8000_dbapi = None

from thrum import sanitize
from thrum import startup
from thrum import constants
from thrum import errors
from thrum import binary
from thrum import oid
from thrum import pypy
from thrum import encode
from thrum import decode
from . import pgbytesproto
from . import statementfactory
from . import cursor as pgcursor


class TP(object):
    tp = None

    @classmethod
    def init(cls):
        cls.tp = threadpool.ThreadPool(minthreads=2, maxthreads=2)
        cls.tp.start()


class PreparedStatementRef(weakref.ref):
    def __init__(self, prepared_statement, callback):
        self.bytes_name = prepared_statement.bytes_name
        weakref.ref.__init__(self, prepared_statement, callback)


class PostgresProtocol(pgbytesproto.PostgresBytesProtocol):

    factory = None
    cc = 0
    log = Logger()
    identifier_regex = re.compile("^\w+$")

    def __init__(self, user, password, database, message_length_bytes=4):
        pgbytesproto.PostgresBytesProtocol.__init__(self, message_length_bytes)

        self.user = user
        self.password = password
        self.database = database
        self.oids = oid.OID()
        self.encode = encode.Encode(self.oids)
        self.decode = decode.Decode(self.oids)
        self.id = None
        self.pool_key = None

        self.turnarounds = []  # Instrumentation
        self.lastprefix = None  # Debug
        self.rowcount = 0
        self.queue = collections.deque()
        self.descriptions_received = 0
        self.channel_notification_deferred = defer.Deferred()
        self.describe_pending = {}  # defer.Deferred()
        self.disconnected = defer.Deferred()
        self.in_transaction = False
        self.connected = False
        self.executing = False
        self.ready_for_query = defer.Deferred()
        self.client_encoding = "utf-8"
        self.backend_pid = None

        self.date_style_order = "DMY"
        self.date_style_format = "ISO"
        self.is_superuser = False
        self.server_encoding = "UTF8"
        self.session_authorization = None
        self.standard_confirming_strings = True
        self.timezone = "GB"
        self.backend_secret_key = None
        self.listeners = {}

        self.message_broker = {
            constants.NOTICE_RESPONSE: self.handle_notice_response,
            constants.AUTHENTICATION_REQUEST:
                self.handle_authentication_request,
            constants.PARAMETER_STATUS: self.handle_parameter_status,
            constants.BACKEND_KEY_DATA: self.handle_backend_key_data,
            constants.READY_FOR_QUERY: self.handle_ready_for_query,
            constants.ROW_DESCRIPTION: self.handle_row_description,
            constants.ERROR_RESPONSE: self.handle_error_response,
            constants.EMPTY_QUERY_RESPONSE: self.handle_empty_query_response,
            constants.DATA_ROW: self.handle_data_row,
            constants.COMMAND_COMPLETE: self.handle_command_complete,
            constants.PARSE_COMPLETE: self.handle_parse_complete,
            constants.BIND_COMPLETE: self.handle_bind_complete,
            constants.CLOSE_COMPLETE: self.handle_close_complete,
            constants.PORTAL_SUSPENDED: self.handle_portal_suspended,
            constants.NO_DATA: self.handle_no_data,
            constants.PARAMETER_DESCRIPTION: self.handle_parameter_description,
            constants.NOTIFICATION_RESPONSE: self.handle_notification_response,
            constants.COPY_DONE: self.handle_copy_done,
            constants.COPY_DATA: self.handle_copy_data,
            constants.COPY_IN_RESPONSE: self.handle_copy_in_response,
            constants.COPY_OUT_RESPONSE: self.handle_copy_out_response
        }

        self.i_pack = binary.i_pack
        b4 = self.i_pack(4)
        self.flush_message = constants.FLUSH + b4
        self.sync_message = constants.SYNC + b4
        self.terminate_message = constants.TERMINATE + b4
        self.copy_done_message = constants.COPY_DONE + b4
        self._transaction_values = {b'I': False,
                                    b'T': True,
                                    b'E': True,
                                    69: True,
                                    73: False,
                                    84: True}

        self.prepared_statements = {}
        self.prepared_statements_closing = collections.defaultdict(list)

        self.field_tuple = namedtuple('Field', ["name",
                                                "name_sanitized",
                                                "preferred_format_code",
                                                "decoder",
                                                "notnull",
                                                "table_oid",
                                                "column_attrnum",
                                                "type_oid",
                                                "type_size",
                                                "type_modifier",
                                                "format_code"])

    def eb(self, f):
        pypy.lprint(f.getBriefTraceback(), color='red')
        return f

    def pg_flush(self):
        pypy.lprint("%s MESSAGE TX:   pg_flush", (self.id,), color='magenta')
        self.transport.write(self.flush_message)

    def pg_sync(self):
        pypy.lprint("%s MESSAGE TX:   pg_sync", (self.id,), color='magenta')
        self.queue.append("sync")
        self.transport.write(self.sync_message)

    def pg_terminate(self):
        pypy.lprint("%s MESSAGE TX:   pg_terminate", (self.id,),
                    color='magenta')
        self.transport.write(self.terminate_message)

    def pg_copy_done(self):
        pypy.lprint("%s MESSAGE TX:   pg_copy_done", (self.id,),
                    color='magenta')
        self.transport.write(self.copy_done_message)

    def pg_close_portal_and_sync(self, portal):
        msg = "%s MESSAGE TX:   pg_close_portal: %s \\" + \
        "\n%s MESSAGE TX:   pg_sync \\" + \
        "\n%s MESSAGE TX:   pg_flush"
        pypy.lprint(msg, (self.id, portal.name, self.id, self.id),
                    color='magenta')
        self.queue.extend((("close_portal", portal), "sync"))
        msg = b"".join((portal.close_message, self.sync_message, self.flush_message))
        self.transport.write(msg)

    def pg_close_portal(self, portal):
        msg = "%s MESSAGE TX:   pg_close_portal: %s -"
        pypy.lprint(msg, (self.id, portal.name,), color='magenta')
        self.queue.append(("close_portal", portal))
        self.transport.write(portal.close_message)

    def pg_close_statement_proxy(self, statement_proxy):
        pypy.lprint("%s MESSAGE TX:   pg_close_statement_proxy: %s",
                    (self.id, statement_proxy.bytes_name,),
                    color='magenta')

        name = statement_proxy.bytes_name

        df = defer.Deferred()
        self.queue.append(("close_statement_proxy", name,))
        self.prepared_statements.pop(name, None)
        self.prepared_statements_closing[name].append(df)
        self._send_message_and_flush(constants.CLOSE, b'S' + name)
        return df

    def pg_close_statement(self, statement):
        pypy.lprint("%s MESSAGE TX:   pg_close_statement: %s",
                    (self.id, statement.bytes_name,),
                    color='magenta')

        name = statement.bytes_name

        df = defer.Deferred()
        self.queue.append(("close_statement", name,))
        self.prepared_statements.pop(name, None)
        self.prepared_statements_closing[name].add(df)
        self._send_message_and_flush(constants.CLOSE, b'S' + name)
        return df

    def connectionMade(self):
        pypy.lprint("%s PostgresProtocol.connectionMade: %s", (self.id, self),
                    color='cyan')
        """
        Sucessful connection to server. Send a startup message.
        """
        self.connected = True
        self.disconnected = defer.Deferred()
        #self.transport.setTcpNoDelay(True)
        return self.send_startup_message()

    def send_startup_message(self):
        self.queue.append("sync")
        message = startup.StartupMessage.construct(user=self.user,
                                                   database=self.database)
        pypy.lprint("%s MESSAGE TX:   startup", (self.id,), color='magenta')
        self.transport.write(message)

    def handle_notice_response(self, message):
        pypy.lprint("%s PostgresProtocol.handle_notice_response: %s",
                    (self.id, message), color='cyan')
        raise Exception("Not yet implemented handle_notice_response")
        pass

    def handle_authentication_request(self, message):
        """
        So far, only these auth codes and methods are suppored:
         0: No password required
         3: Cleartext password
         5: md5 password
        """
        auth_code = binary.i_unpack(message)[0]
        if auth_code == 0:
            # No password required
            return

        if self.password is None:
            raise errors.ThrumError(
                "server requesting password authentication, but no "
                "password was provided")

        if auth_code == 3:
            # Plain text password
            self._send_message_and_flush(constants.PASSWORD,
                               self.password + self.encode.null_byte)

        elif auth_code == 5:
            return self.send_hashed_password(message)

        elif auth_code == 10:
            # AuthenticationSASL
            body = message[4:-2]
            mechanism_list = [value.decode("utf-8") for value in body.split(self.encode.null_byte)]

            scram_client = self.scram_client = scramp.ScramClient(
                mechanism_list,
                self.user,
                self.password,
                channel_binding=None
            )
            init = scram_client.get_client_first().encode("utf8")
            mech = scram_client.mechanism_name.encode("ascii") + self.encode.null_byte
            response = mech + self.i_pack(len(init)) + init
            self._send_message(constants.PASSWORD, response)
        elif auth_code == 11:
            try:
                self.scram_client.set_server_first(message[4:].decode("utf8"))
            except Exception as e:
                pypy.lprint(f"E: {e}")
                raise
            msg = self.scram_client.get_client_final().encode("utf8")
            try:
                self._send_message(constants.SASLRESPONSE, msg)
            except Exception as e:
                pypy.lprint(f"E2: {e}")
                raise
        elif auth_code == 12:
            self.scram_client.set_server_final(message[4:].decode("utf8"))
        else:
            raise errors.ThrumError(
                "Authentication method " + str(auth_code) +
                " not supported by Thrum.")

    def send_hashed_password(self, message):
        msg = "%s PostgresProtocol.send_hashed_password: %s"
        pypy.lprint(msg, (self.id, self.password,), color='cyan')
        salt = b"".join(binary.cccc_unpack(message, 4))
        encoded_password = self.get_encoded_password(salt)
        return self._send_message_and_flush(constants.PASSWORD,
                                  encoded_password + self.encode.null_byte)

    def md5sum(self, data):
        if type(data) == six.text_type:
            return md5(data.encode("utf8")).hexdigest().encode("ascii")
        else:
            return md5(data).hexdigest().encode("ascii")

    def get_encoded_password(self, salt):
        checksum_0 = self.md5sum(self.password + self.user)
        checksum_1 = self.md5sum(checksum_0 + salt)
        return b"md5" + checksum_1

    def handle_parameter_status(self, message):
        pos = message.find(self.encode.null_byte)
        key, value = message[:pos], message[pos + 1:-1]

        if key == b"client_encoding":
            encoding = value.decode("ascii").lower()
            self.client_encoding = encoding
            self.encode.update_client_encoding(encoding)
            self.decode.update_client_encoding(encoding)
        elif key == b"integer_datetimes":
            if value == b'on':
                self.encode.timestamps_are_integers()
                self.decode.timestamps_are_integers()
            else:
                self.encode.timestamps_are_floats()
                self.decode.timestamps_are_floats()
        elif key == b"server_version":
            return self.set_server_version(value.decode('ascii'))
        elif key == b"DateStyle":
            pass
        elif key == b"IntervalStyle":
            pass
        elif key == b"is_superuser":
            self.is_superuser = {b"on": True, b"off": False}.get(value, False)
        elif key == b"server_encoding":
            self.server_encoding = value
        elif key == b"session_authorization":
            self.session_authorization = value
        elif key == b"standard_confirming_strings":
            self.standard_confirming_strings = \
                {b"on": True, b"off": False}.get(value, True)
        elif key == b"TimeZone":
            self.timezone = value

    def set_server_version(self, version_string):
        self._server_version = version_string

    def handle_backend_key_data(self, message):
        pypy.lprint("%s PostgresProtocol.handle_backend_key_data", (self.id,),
                    color='cyan')
        pid, secret = binary.ii_unpack(message)
        self.backend_pid = pid
        self.backend_secret_key = secret

    def handle_ready_for_query(self, message):
        pypy.lprint("%s PostgresProtocol.handle_ready_for_query", (self.id,),
                    color='cyan')
        self.executing = False
        self.in_transaction = self._transaction_values[message[-1]]
        event = self.queue.popleft()
        assert event == "sync"
        try:
            rfq = self.ready_for_query
            self.ready_for_query = defer.Deferred()
            rfq.callback(self)
        except defer.AlreadyCalledError:
            msg = "%s defer.AlreadyCalledError: %s"
            cprint(msg % (self.id, traceback.format_exc(),))
            pass
        except Exception as e:
            msg = "%s PostgresProtocol.handle_ready_for_query: EX: %s"
            cprint(msg % (self.id, e,), color='cyan')

    def execute(self, statement, parameters=None, row_cache_size=100,
                row_unpacker=None, per_row_callable=None):
        pypy.lprint('%s PostgresProtocol.execute', (self.id,), color='cyan')
        prepared_statement = self.prepare_statement(statement)
        df = self.parse_statement(prepared_statement)
        df.addCallback(self.execute_parsed_statement, parameters,
                       row_cache_size, row_unpacker, per_row_callable)
        return df

    def prepare_statement(self, statement, row_cache_size=None):
        pypy.lprint('%s PostgresProtocol.prepare_statement', (self.id,),
                    color='cyan')

        prepared_statement = statementfactory.PreparedStatement(
            statement,
            name=None,
            row_cache_size=row_cache_size,
            encoding=self.client_encoding
        )
        self.prepared_statements[prepared_statement.bytes_name] = \
            PreparedStatementRef(prepared_statement,
                                 self.pg_close_statement_proxy)
        return prepared_statement

    def _chain_deferred(self, result, deferred):
        try:
            deferred.callback(result)
        except Exception:
            pass
        return result

    def parse_statement(self, prepared_statement):
        pypy.lprint("%s PostgresProtocol.parse_statement", (self.id,),
                    color='cyan')

        """
        Avoid re-parsing the same query. How did we get here?
        """
        if prepared_statement.parsed.get(self.id):
            return defer.succeed(prepared_statement)

        """
        Avoid stampedes
        """
        if prepared_statement.bytes_name in self.describe_pending:
            pending_df = self.describe_pending[prepared_statement.bytes_name]
            df = defer.Deferred()
            pending_df.addCallback(self._chain_deferred, df)
            return df

        if prepared_statement.bytes_name in self.prepared_statements_closing:
            def zed(_):
                return self.parse_statement(prepared_statement)
            df = self.prepared_statements_closing[prepared_statement.bytes_name][-1]
            df.addCallback(zed)

        #self.queue.append(("close_statement",))
        #self._send_message_and_flush(constants.CLOSE, b'S' + prepared_statement.bytes_name)
        self.queue.append(("parse", prepared_statement))
        self._send_message_and_flush(constants.PARSE, prepared_statement.parse_message)

        df = defer.Deferred()
        df.addCallback(prepared_statement.describe_complete)
        df.addErrback(self.eb)
        self.describe_pending[prepared_statement.bytes_name] = df

        # We add 't' to the queue twice because Postgres will return two
        # responses to our 'describe' request: A parameter description and
        # a row description.
        t = ("describe", prepared_statement)
        self.queue.extend((t, t))

        self._send_message_and_flush(constants.DESCRIBE,
                           constants.STATEMENT + prepared_statement.bytes_name)

        return self.describe_pending[prepared_statement.bytes_name]

    def execute_parsed_statement(self, prepared_statement, parameters,
                                 row_cache_size=100, row_unpacker=None,
                                 per_row_callable=None):
        pypy.lprint('%s PostgresProtocol.execute_parsed_statement', (self.id,),
                    color='cyan')
        portal = prepared_statement.new_portal(self, parameters,
                                               row_cache_size, row_unpacker,
                                               per_row_callable)
        return self.bind_and_execute(portal)

    def make_portal(self, prepared_statement, parameters=None,
                    row_cache_size=100, row_unpacker=None,
                    per_row_callable=None):
        pypy.lprint('%s PostgresProtocol.make_portal', (self.id,),
                    color='cyan')
        return prepared_statement.new_portal(self, parameters, row_cache_size,
                                             row_unpacker, per_row_callable)

    def bind_and_execute(self, portal, getall=False):
        """Bind parameters to a prepared statement and execute it.

        Normally we would never bind a statement without executing it, so we
        have a single method to send the bind and execute messages to Postgres
        in one hit.

        For TxPostgres emulation, this is not ideal, so we also have the
        bind_cursor method in this class that bind parameters to a prepared
        statement so that the user can call 'execute' on that cursor
        explicitly.
        """
        msg = "%s PostgresProtocol.bind_and_execute: %s"
        pypy.lprint(msg, (self.id, portal.name,), color='cyan')

        portal.begin = time.time()
        rval = defer.Deferred()
        bind_message = portal.bind_message()
        m = "%s MESSAGE TX: bind  |%s|\n%s MESSAGE TX: execute\n" + \
            "%s MESSAGE TX: flush"
        pypy.lprint(m, (self.id, constants.BIND, bind_message, self.id),
                        color='magenta')
        self.transport.write(bind_message + portal.execute_message +
                             self.flush_message)
        self.queue.extend((("bind", portal),
                           ("execute", portal, rval, getall)))
        return rval

    def resume(self, portal):
        msg = "%s PostgresProtocol.resume: %s"
        pypy.lprint(msg, (self.id, portal.name,), color='cyan')

        if self.executing not in (False, portal.name):
            msg = "%s ALREADY EXECUTING: %s"
            raise Exception(msg % (self.id, self.executing))

        try:
            self.executing = portal.name
            self.transport.write(portal.execute_message + self.flush_message)
            if len(self.queue) > 1:
                self.queue.append(self.queue.popleft())
        except Exception:
            msg = "%s PostgresProtocol.resume: EX2: %s"
            cprint(msg % (self.id, traceback.format_exc(),), color='red')

    def execute_portal(self, portal, execute_message):
        msg = "%s PostgresProtocol.execute_portal: %s"
        pypy.lprint(msg, (self.id, portal.name,), color='cyan')

        if self.executing not in (False, portal.name):
            msg = "%s ALREADY EXECUTING: %s"
            raise Exception(msg % (self.id, self.executing))

        try:
            rval = defer.Deferred()
            self.executing = portal.name
            self._send_message_and_flush(constants.EXECUTE, execute_message)
            self.queue.append(("execute", portal, rval, False))
        except Exception:
            msg = "%s PostgresProtocol.execute_portal: EX2: %s"
            cprint(msg % (self.id, traceback.format_exc(),), color='red')
        return rval

    def create_parse_message(self, statement_name_bin, bytes_statement,
                             parameters=False):
        return b'%s%s%s%s', (statement_name_bin, bytes_statement,
                             constants.NULL_BYTE, binary.h_pack(0))

    def retrieve_field_metadata(self, prepared_statement):
        msg = "%s PostgresProtocol.retrieve_field_metadata 1"
        pypy.lprint(msg, (self.id,), color='cyan')
        fields = prepared_statement.fields[self.id]
        query = """
                    SELECT
                        attrelid,
                        attnum,
                        attnotnull
                    FROM
                        pg_attribute
                    WHERE
                        attrelid IN ($1)
                """
        params = tuple(set([field.table_oid for field in fields]))
        df = self.execute(query, parameters=params)
        df.addCallback(self._retrieve_field_metadata, prepared_statement)
        return df

    def _retrieve_field_metadata(self, result, prepared_statement):
        pypy.lprint("%s PostgresProtocol._retrieve_field_metadata: %s",
                    args=(self.id, result,), color="white")
        rows = result[0]
        tables = collections.defaultdict(dict)
        fields = prepared_statement.fields[self.id]

        for row in rows:
            table, field, notnull = row
            tables[table][field] = notnull == False  # noqa

        new_fields = []
        for index, field in enumerate(fields):
            try:
                notnull = tables[field.table_oid][field.column_attrnum]
            except Exception:
                notnull = False
            f = self.field_tuple(field.name,
                                 field.name_sanitized,
                                 field.preferred_format_code,
                                 field.decoder,
                                 notnull,
                                 field.table_oid,
                                 field.column_attrnum,
                                 field.type_oid,
                                 field.type_size,
                                 field.type_modifier,
                                 field.format_code)
            new_fields.append(f)

        return new_fields

    def _send_message(self, code, data):
        m = code + self.i_pack(len(data) + 4) + data
        pypy.lprint(f"send: {m}")
        msg = "%s MESSAGE TX: %3s  |%s|"
        pypy.lprint(msg, (self.id, code, m.split(self.encode.null_byte),),
                    color='magenta')
        self.transport.write(m)

    def _send_message_and_flush(self, code, data):
        m = code + self.i_pack(len(data) + 4) + data
        pypy.lprint(f"send: {m}")
        msg = "%s MESSAGE TX: %3s  |%s|"
        pypy.lprint(msg, (self.id, code, m.split(self.encode.null_byte),),
                    color='magenta')
        self.transport.write(m)
        self.transport.write(self.flush_message)

    def handle_row_description(self, message):
        pypy.lprint("%s PostgresProtocol.handle_row_description 1", (self.id,),
                    color='cyan')
        event = self.queue.popleft()
        assert event[0] == "describe"

        notnull = None  # We can't find this out unti later
        try:
            prepared_statement = event[1]

            count = binary.h_unpack(message)[0]
            idx = 2

            fields = pypy.newlist_hint(count)
            for i in range(count):
                end = message.find(constants.NULL_BYTE, idx)
                name = message[idx:end].decode("utf-8")
                idx += len(name) + 1
                field_data = binary.ihihih_unpack(message, idx)
                idx += 18

                type_oid = field_data[2]
                format_code, unpack_func = self.decode.pg_types[type_oid]

                field = self.field_tuple(name, sanitize.fieldname(name),
                                         format_code, unpack_func,
                                         notnull, *field_data)
                fields.append(field)

            prepared_statement.update_row_description(self, fields)
            self.description_received(prepared_statement)
        except Exception:
            pypy.lprint(traceback.format_exc(), (), color='red')
            raise

    def handle_error_response(self, message):
        pypy.lprint("%s PostgresProtocol.handle_error_response", (self.id,),
                    color='cyan')

        error_data = OrderedDict(
            (s[:1], s[1:].decode(self.decode.client_encoding)) for s in
            message.split(self.encode.null_byte) if s != b'')

        for key, value in error_data.items():
            print("%30s: %s" % (key, value,))

        if error_data[b'C'] == b"42P05":
            #
            print("RETURNING")
            for item in self.queue:
                print("    %s" % (item,))

            print("==================\n")
            event = self.queue.popleft()
            assert event[0] == "parse"
            return
        else:
            print("\nerrC: %s" % (error_data[b'C'],))

        self.ready_for_query = defer.Deferred()

        last_event = self.queue[0]
        if last_event[0] == 'execute':
            df = last_event[2]
        else:
            df = None

        # Reset the queue since we've just lost everything
        self.queue = collections.deque()
        self.pg_sync()

        try:

            raise(errors.PostgresError(error_data))
        except Exception as ex:
            print(ex)
            if df:
                df.errback(ex)
            else:
                raise

            pypy.lprint("%s ERROR MSG: %s", (self.id, ex.message,))
            pypy.lprint("%s ERROR DTL: %s", (self.id, ex.detail,))

    def handle_empty_query_response(self, message):
        msg = "%s PostgresProtocol.handle_empty_query_response:"
        pypy.lprint(msg, (self.id,), color='cyan')
        event, portal, df, getall = self.queue.popleft()
        assert event[0] == "execute"

        self.pg_close_portal_and_sync(portal)
        df.callback(None, None, None)
        self.turnarounds.append(time.time() - portal.begin)
        pass

    def handle_data_row(self, message):
        assert self.queue[0][0] == "execute"
        portal = self.queue[0][1]
        portal.handle_row(message)

    def handle_command_complete(self, message):
        self.executing = False

        try:
            msg = "%s PostgresProtocol.handle_command_complete: %s "
            pypy.lprint(msg, (self.id, self.ready_for_query.called,),
                        color='cyan')
            event, portal, df, getall = self.queue.popleft()
            pypy.lprint(msg, (self.id, df,), color="yellow")
            assert event == "execute"

            try:
                self.pg_close_portal_and_sync(portal)
            except Exception as e:
                pypy.lprint(msg, (self.id, e,), color="red")
                raise

            metadata = message.rstrip(b'\x00').split()
            pypy.lprint("metadata: %s", (metadata,) , color='cyan')
            try:
                affected_rows = int(metadata[-1])
            except ValueError:
                affected_rows = None
            command_tag = metadata[0]

            msg = "%s PostgresProtocol.handle_command_complete callbacks: %s "
            pypy.lprint(msg, (self.id, df.callbacks,), color='cyan')
            try:
                df.callback((portal.rows, command_tag, affected_rows))
                msg = "%s PostgresProtocol.handle_command_complete ok "
                pypy.lprint(msg, (self.id, ), color='cyan')
            except Exception as e:
                self.log.err("callback failed: %s" % (e,))
            finally:
                portal.rows = None

            try:
                portal.handle_command_complete(command_tag, affected_rows)
            except Exception as e:
                self.log.err("portal.handle_command_complete failed: %s" % (e,))
                raise

        except Exception as e:
            pypy.lprint("something failed: %s", (e,), color='red')
            pypy.lprint("%s %s", (self.id, traceback.format_exc(),),
                        color='red')

        self.turnarounds.append(time.time() - portal.begin)
        msg = "%s PostgresProtocol.handle_command_complete: done: %s "
        pypy.lprint(msg, (self.id, self.ready_for_query.called,), color='cyan')

    def handle_portal_suspended(self, message):
        pypy.lprint("%s PostgresProtocol.handle_portal_suspended", (self.id,),
                    color='cyan')

        event, portal, df, getall = self.queue[0]
        assert event == "execute"
        if getall is True:
            return self.resume(portal)
        self.queue.popleft()
        portal.handle_portal_suspended()

    def handle_parse_complete(self, message):
        """
        Really useless message. Not sure it tells us anything at all.
        """
        pypy.lprint("%s PostgresProtocol.handle_parse_complete", (self.id,),
                    color='cyan')
        event = self.queue.popleft()
        assert event[0] == "parse"

    def handle_bind_complete(self, message):
        pypy.lprint("%s PostgresProtocol.handle_bind_complete", (self.id,),
                    color='cyan')
        event, portal = self.queue.popleft()
        assert event == "bind"
        return

    def handle_close_complete(self, message):
        pypy.lprint("%s handle_close_complete: %s", (self.id, self.queue[0]),
                    color='cyan')
        event = self.queue.popleft()
        assert event[0] in ("close_portal",
                            "close_statement",
                            "close_statement_proxy")

        if event[0] == "close_portal":
            # We need to handle statement closures
            return

        for ev in self.queue:
            if ev[0] in ("close_statement", "close_statement_proxy"):
                # We still have a 'close_statement*' event in the queue
                # Don't invoke callbacks until ALL statements are closed
                # because close order is not guaranteed
                return

        statement_name = event[1]
        prepped = self.prepared_statements_closing.pop(statement_name, None)
        if not prepped:
            return

        for df in prepped:
            try:
                df.callback(True)
            except Exception as e:
                self.log.error("Failed to callback for query '{q!r}': {e!r}",
                               q=statement_name,
                               e=e)

    def handle_no_data(self, message):
        """
        This message is received when the query will return no data
        and so therefore we should not expect to receive a row
        description. We can simply recognise 'description_received'
        so that our deferred will get fired.
        """
        event = self.queue.popleft()
        assert event[0] == "describe"
        prepared_statement = event[1]
        self.description_received(prepared_statement)

    def description_received(self, prepared_statement):
        msg = "%s PostgresProtocol.description_received: %s"
        pypy.lprint(msg, (self.id, self.descriptions_received,), color='cyan')
        if self.descriptions_received == 1:
            self.descriptions_received = 0
            msg = "%s PostgresProtocol.describe_complete.callback"
            pypy.lprint(msg, (self.id,), "cyan")
            """
            After we issue the 'describe' message, we issue a pg_sync. That
            will trigger a 'ReadyForQuery' message from Postgres. Once we
            receive that ReadyForQuery we can bind and execute.
            """
            df = self.describe_pending.pop(prepared_statement.bytes_name)
            df.callback(self)
        else:
            self.descriptions_received = 1

    def handle_parameter_description(self, message):
        msg = "%s PostgresProtocol.handle_parameter_description: %s"
        pypy.lprint(msg, (self.id, 0,), color='cyan')

        event = self.queue.popleft()
        assert event[0] == "describe"

        try:
            prepared_statement = event[1]

            parameter_count = binary.h_unpack(message)[0]
            if not parameter_count:
                return self.description_received(prepared_statement)

            unpack_func = \
                binary.Cache.get_unpack_ints_for_index(parameter_count)
            oids = unpack_func(message, 2)
        except Exception:
            msg = "%s PostgresProtocol.handle_parameter_description: 2: %s"
            cprint(msg % (self.id, traceback.format_exc(),))
            raise

        msg = "%s PostgresProtocol.handle_parameter_description: 2"
        pypy.lprint(msg, (self.id,), color='cyan')
        param_funcs = []
        param_format_codes = []
        for i, field_oid in enumerate(oids):

            if field_oid == self.oids.unknown:
                field_oid = self.oids.text

            field_type = self.oids.get(field_oid)
            method, format_code = self.encode.encoders[field_type]
            param_funcs.append(method)
            param_format_codes.append(format_code)

        try:
            prepared_statement.update_parameter_data(self.id,
                                                     param_funcs,
                                                     param_format_codes)
        except Exception:
            msg = "%s PostgresProtocol.handle_parameter_description: %s"
            pypy.lprint(msg, (self.id, traceback.format_exc(),), color='red')

        return self.description_received(prepared_statement)

    def handle_notification_response(self, message):
        backend_pid = binary.i_unpack(message[0:4])[0]
        channel_name, payload = \
            message[4:].rstrip(self.encode.null_byte)\
            .split(self.encode.null_byte)

        channel_name = channel_name.decode(self.decode.client_encoding)
        payload = payload.decode(self.decode.client_encoding)
        channel_listeners = self.listeners.get(channel_name)
        if channel_listeners is None:
            return

        for callback, ignore_local_notifications in channel_listeners.items():
            try:
                local = backend_pid == self.backend_pid
                if ignore_local_notifications and local:
                    continue
                callback(
                    channel_name,
                    payload=payload,
                    backend_pid=backend_pid,
                    local=local,
                )
            except Exception:
                pass

    def handle_copy_done(self, message):
        pass

    def handle_copy_data(self, message):
        pass

    def handle_copy_in_response(self, message):
        pass

    def handle_copy_out_response(self, message):
        pass

    def messageReceived(self, prefix, message):
        '''
        if prefix == b'D':
            self.rowcount += 1
        elif self.lastprefix == b'D':
            if self.rowcount > 1:
                msg = "%s MESSAGE RX: %s |%s| (%s more data rows)"
                pypy.lprint(msg, (self.id, 'D', len(message),
                                  self.rowcount-1), color='yellow')
            self.rowcount = 0

        if prefix == b'N':
            msg = "%s MESSAGE RX: %s |%s| (notice response)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'n':
            msg = "%s MESSAGE RX: %s |%s| (no data)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b's':
            msg = "%s MESSAGE RX: %s |%s| (portal suspended)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'T':
            msg = "%s MESSAGE RX: %s |%s| (row description)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b't':
            msg = "%s MESSAGE RX: %s |%s| (parameter description)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'E':
            msg = "%s MESSAGE RX: %s |%s| (error response)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'I':
            msg = "%s MESSAGE RX: %s |%s| (empty query response)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'C':
            msg = "%s MESSAGE RX: %s |%s| (command complete)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'1':
            msg = "%s MESSAGE RX: %s |%s| (parse complete)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'2':
            msg = "%s MESSAGE RX: %s |%s| (bind complete)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'3':
            msg = "%s MESSAGE RX: %s |%s| (close complete)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'Z':
            msg = "%s MESSAGE RX: %s |%s| (ready for query)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')
        elif prefix == b'D' and self.lastprefix != b'D':
            msg = "%s MESSAGE RX: %s |%s| (data row)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='yellow')
        elif prefix == b'D':
            pass
        else:
            msg = "%s MESSAGE RX: %s |%s| (other)"
            pypy.lprint(msg, (self.id, prefix, len(message),), color='green')

        self.lastprefix = prefix
        '''
        try:
            return self.message_broker[prefix](message)
        except Exception:
            cprint("%s %s" % (self.id, traceback.format_exc()), color='red')

    def connectionLost(self, reason):
        msg = "%s PostgresProtocol.connectionLost: %s"
        pypy.lprint(msg, (self.id, reason), color='red')
        self.connected = False
        for name, prepared_statement_ref in self.prepared_statements.items():
            prepared_statement = prepared_statement_ref()
            if prepared_statement:
                prepared_statement.lose_protocol(self)
        self.prepared_statements = {}

        self.disconnected.callback(1)

    def disconnect(self):
        try:
            self.pg_sync()
            self.ready_for_query.addCallback(self._disconnect)
            return self.disconnected
        except Exception:
            msg = "%s PostgresProtocol.disconnect: %s"
            pypy.lprint(msg, (self.id, traceback.format_exc()), color='red')

    def _disconnect(self, _):
        pypy.lprint("%s PostgresProtocol.disconnect", (self.id,), color='red')
        self.pg_terminate()
        self.transport.loseConnection()
        return self.disconnected

    '''
    Manage Notifications received from PostgreSQL.
    '''
    @defer.inlineCallbacks
    def unlisten(self, channel, callback):
        channel_listeners = self.listeners.get(channel)
        if channel_listeners is None or callback not in channel_listeners:
            defer.returnValue(None)

        channel_listeners.pop(callback)
        _ = yield self._unlisten(channel)
        defer.returnValue(None)

    def _unlisten(self, channel):
        self.ensure_identifier(channel)
        quoted_channel_name = channel.replace('"', '""')
        query = f"""UNLISTEN "{quoted_channel_name}"  """
        return self.execute(query)

    @defer.inlineCallbacks
    def listen(self, channel, callback, ignore_local_notifications=False):
        self.ensure_identifier(channel)
        empty_dict = {}
        channel_listeners = self.listeners.setdefault(channel, empty_dict)
        if channel_listeners == empty_dict:

            quoted_channel_name = channel.replace('"', '""')
            query = f"""LISTEN "{quoted_channel_name}"  """
            _ = yield self.execute(query)

        channel_listeners[callback] = ignore_local_notifications
        defer.returnValue(None)

    @defer.inlineCallbacks
    def notify(self, channel, payload=None):
        self.ensure_identifier(channel)
        payload_bytes = payload.encode(self.client_encoding)
        payload_length = len(payload_bytes)
        if payload_length > 8000:
            raise ValueError(
                "Notify payload length '{payload_length} bytes' "
                "is greater than the maximum '8000 bytes'"
            )

        query = """SElECT pg_notify($1, $2)"""
        params = (channel, payload,)
        _ = yield self.execute(query, parameters=params)
        defer.returnValue(None)

    def ensure_identifier(self, value):
        if not self.identifier_regex.match(value):
            raise ValueError(f"'{value}' is not a valid PostgreSQL Identifier")

    '''
    TxPostgres compatibility methods
    '''
    def _prepare_dbapi_query(self, query, *args, **kwargs):
        if "%" not in query:
            return query, ()

        if pg8000_dbapi is None:
            raise RuntimeError(
                "Can't translate query from DB-API to PostgreSQL "
                "format because pg8000 could not be imported"
            )

        if args and kwargs:
            raise RuntimeError(
                "query methods can't accept both *args and **kwargs arguments"
            )

        if args:
            query_args = args
        else:
            query_args = kwargs

        style = "pyformat"
        return pg8000_dbapi.convert_paramstyle(style, query, query_args)

    def bind_cursor(self, cursor):
        msg = "%s PostgresProtocol.bind_cursor: %s"
        pypy.lprint(msg, (self.id, cursor.portal.name,), color='cyan')

        cursor.portal.begin = time.time()
        rval = defer.Deferred()
        bind_message = cursor.portal.bind_message()
        m = "%s MESSAGE TX: bind  |%s|\n%s MESSAGE TX: execute\n" + \
            "%s MESSAGE TX: flush"
        pypy.lprint(m, (self.id, constants.BIND, bind_message, self.id),
                        color='magenta')
        self.transport.write(bind_message)
        self.queue.append(("bind", cursor.portal))

    def execute_cursor(self, cursor, getall=False):
        msg = "%s PostgresProtocol.execute_cursor: %s"
        pypy.lprint(msg, (self.id, cursor.portal.name,), color='cyan')
        rval = defer.Deferred()
        self.transport.write(cursor.portal.execute_message + self.flush_message)
        self.queue.append(
            ("execute", cursor.portal, rval, getall)
        )
        return rval

    @defer.inlineCallbacks
    def runQuery(self, query, *args, **kwargs):
        query, query_args = self._prepare_dbapi_query(query, *args, **kwargs)

        prepared_statement = self.prepare_statement(query)
        prepared_statement = yield self.parse_statement(prepared_statement)
        # yield prepared_statement.compile_custom_unpacker(self)
        result = []
        portal = self.make_portal(
            prepared_statement,
            parameters=query_args,
            row_cache_size=5000,
            per_row_callable=result.append,
        )

        yield self.bind_and_execute(portal, getall=True)
        defer.returnValue(result)

    def noop(self, result):
        return result

    @defer.inlineCallbacks
    def runOperation(self, query, *args, **kwargs):
        query, query_args = self._prepare_dbapi_query(query, *args, **kwargs)

        prepared_statement = self.prepare_statement(query)
        prepared_statement = yield self.parse_statement(prepared_statement)
        # yield prepared_statement.compile_custom_unpacker(self)
        portal = self.make_portal(
            prepared_statement,
            parameters=query_args,
            row_cache_size=5000,
            per_row_callable=self.noop
        )

        yield self.bind_and_execute(portal, getall=True)
        defer.returnValue(None)

    def runInteraction(self, interaction, *args, **kwargs):
        cursor = self.cursor()
        df = cursor.execute("BEGIN TRANSACTION")
        df.addCallback(self._interaction, cursor, interaction, *args, **kwargs)
        return df

    def _interaction(self, result, cursor, interaction, *args, **kwargs):
        df = defer.maybeDeferred(interaction, cursor, *args, **kwargs)
        df.addCallback(self._commit_transaction, cursor)
        df.addErrback(self._rollback_transaction, cursor)
        return df

    def _discard(self, result, return_value):
        """Discard what would be the result and return return_value instead.

        We want to avoid using a lambda for performance reasons.
        """
        return return_value

    def _commit_transaction(self, result, cursor):
        df = cursor.execute("COMMIT TRANSACTION")
        df.addCallback(self._discard, result)
        return df

    def _rollback_transaction(self, f, cursor):
        # maybeDeferred in case cursor.execute raises a synchronous
        # exception
        df = defer.maybeDeferred(cursor.execute, "ROLLBACK TRANSACTION")

        def justPanic(rf):
            log.err(rf)
            return defer.fail(RollbackFailed(self, f))

        # if rollback failed panic
        df.addErrback(justPanic)
        # otherwise reraise the original failure
        return df.addCallback(self._discard, f)

    def cursor(self):
        pypy.lprint('%s PostgresProtocol.cursor', (self.id,), color='blue')
        return pgcursor.Cursor(self)
