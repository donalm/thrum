#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Implementation of the byte-length wire protocol that underlies the Postgres
message-based protocol. This code breaks the data received from Postgres into
a message prefix and a message, passing them into the messageReceived method.

https://www.postgresql.org/docs/9.6/static/protocol-flow.html
https://www.postgresql.org/docs/9.6/static/protocol-message-formats.html
"""

import struct
from twisted.logger import Logger
from twisted.internet import protocol


class PostgresBytesProtocol(protocol.Protocol):

    log = Logger()

    def __init__(self, message_length_bytes=4):
        """
        Each message consists of one byte of text followed by a 32bit (4 byte)
        integer representing the message length. This limits the message length
        to 2GB, so it will conceivably be changed in the future :-S

        The message_length value includes the bytes used to encode itself,
        which will normally be 4. It doesn't include the first 'prefix' byte of
        the message.

        @param message_length_bytes: Number of bytes to represent message len
        @type message_length_bytes: L{int}
        """
        self.prefix_boundary = message_length_bytes + 1
        self.prefix = None

        """
        We maintain a buffer of data we've received from the server, and an
        index, which is our current position in that buffer.

        For efficiency, we do not modify the contents of the buffer until we
        can't avoid it. We just modify our index.
        """
        self.buffer = ""
        self.index = 0
        self.message_length = 0
        self.maxmessages = 0
        self.maxbuf = 0

        if message_length_bytes == 4:
            # In the current releases, this number is always 4
            self._unpack_from = struct.Struct("!ci").unpack_from
        elif message_length_bytes == 8:
            # In a hypothetical future release, this number could be 8
            self._unpack_from = struct.Struct("!cq").unpack_from

    def dataReceived(self, data):
        """
        Minimize string creation

        In one call to this method, 'data' might contain hundreds of rows.
        """
        if self.index > 0:
            self.buffer = self.buffer[self.index:] + data
            self.index = 0
        else:
            self.buffer = data

        """
        Some accounting data.

        'messages' is the number of Postgres messages that are contained in
        this one buffer. We will use it to update self.maxmessages if
        appropriate.
        """
        messages = 0

        """
        The length of the current buffer, which may have been generated over
        multiple calls to dataReceived, if for example we are receiving one
        very large message.

        These values will help us to maintain our 'pointer' position in the
        buffer.
        """
        remaining_bufflen = base_bufflen = len(self.buffer)

        """
        We store the maximum buffer size we've seen to help us understand
        performance and memory usage.

        The 'if' clause here is faster than calling the 'max' method.
        """
        if base_bufflen > self.maxbuf:
            self.maxbuf = base_bufflen

        message_body_index = self.index + self.prefix_boundary
        while True:
            if remaining_bufflen < self.prefix_boundary:
                # We don't have enough data to form one message
                break

            if self.message_length == 0:
                """
                We just finished processing a message and are ready to start
                a new one.
                """
                self.prefix, self.message_length = \
                    self._unpack_from(self.buffer, offset=self.index)

            if remaining_bufflen <= self.message_length:
                """
                When this is true, we have to wait for more data to arrive
                from postgres (calling this method again) so that we can
                unpack our row.

                Remember that self.message_length does not include the byte
                that prefixes each message.
                """
                break

            messages += 1
            msg_boundary = self.index + self.message_length + 1

            message = self.buffer[message_body_index:msg_boundary]

            prefix = self.prefix
            self.message_length = 0

            try:
                self.messageReceived(prefix, message)
            except Exception as e:
                try:
                    msg = "{prefix!r}: {message!r}: {err!r}"
                    self.log.error(
                        msg,
                        prefix=prefix,
                        message=message,
                        err=e
                    )
                except Exception:
                    pass


            """
            Check to see if there is more data in the buffer to process.
            If not - reset our attributes and break.
            """
            if msg_boundary == base_bufflen:
                self.index = 0
                self.buffer = None
                break

            self.index = msg_boundary
            remaining_bufflen = base_bufflen - self.index
            message_body_index = self.index + self.prefix_boundary

        # The 'if' clause here is faster than calling the 'max' method.
        if messages > self.maxmessages:
            self.maxmessages = messages

    def messageReceived(self, prefix, message):
        pass

    def stopProducing(self):
        pass

    def pauseProducing(self):
        pass

    def resumeProducing(self):
        pass
