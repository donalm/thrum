#!/usr/bin/env python

import traceback
import base64
from twisted.internet import defer
from collections import deque
from twisted.logger import Logger

from thrum import constants
from thrum import binary
from thrum import pypy


class Portal(object):
    """
    Postgres doesn't really have cursors on the client side. It has portals,
    which are a very similar concern. Cursors must return data but portals
    can also be used for inserts, updates, deletes, etc.
    """
    log = Logger()

    count = 0
    NULL = constants.NULL
    CLOSE = constants.CLOSE
    BIND = constants.BIND

    one_bin = binary.i_pack(1)
    twok_bin = binary.i_pack(2000)
    infinity = float("inf")
    make_df = defer.Deferred

    __slots__ = ('name',
                 'parameters',
                 'row_cache_size',
                 'row_cache_size_bin',
                 'statement',
                 'cached_bind_message',
                 'per_row_callable',
                 'custom_callable',
                 'execute_message',
                 'close_message',
                 'rowindex',
                 'rows',
                 'begin',
                 'protocol',
                 'unpack_row',
                 'ranges')

    def __init__(self, protocol, portal_name, parameters, row_cache_size,
                 statement, row_unpacker=None, per_row_callable=None):
        self.protocol = protocol
        self.name = portal_name
        pypy.lprint("%s Portal.__init__ %s", args=(protocol.id, row_cache_size,), color="white")
        self.row_cache_size = row_cache_size
        self.row_cache_size_bin = binary.i_pack(row_cache_size)
        self.statement = statement

        execute_message = self.name + self.row_cache_size_bin
        message_len = self.protocol.i_pack(len(execute_message) + 4)
        self.execute_message = constants.EXECUTE + message_len + \
            execute_message

        Portal.count += 1

        data = b'P' + self.name
        self.close_message = self.CLOSE + binary.i_pack(len(data) + 4) + data

        self.begin = 0
        self.rowindex = -1
        self.ranges = None
        self.parameters = parameters
        self.cached_bind_message = None
        self.unpack_row = row_unpacker or self._unpack_row

        if per_row_callable is None:
            self.custom_callable = False
            self.rows = []
            self.per_row_callable = self.rows.append
        else:
            self.custom_callable = True
            self.rows = None
            self.per_row_callable = per_row_callable

    def reset(self, parameters, portal_name, row_cache_size, row_unpacker=None,
              per_row_callable=None):

        self.name = portal_name
        self.cached_bind_message = None
        data = b'P' + self.name
        self.close_message = self.CLOSE + binary.i_pack(len(data) + 4) + data

        if row_cache_size != self.row_cache_size:
            self.row_cache_size = row_cache_size
            self.row_cache_size_bin = binary.i_pack(row_cache_size)

        execute_message = self.name + self.row_cache_size_bin
        message_len = self.protocol.i_pack(len(execute_message) + 4)
        self.execute_message = constants.EXECUTE + message_len + \
            execute_message
        if parameters != self.parameters:
            self.parameters = parameters

        self.ranges = None
        self.begin = 0
        self.rowindex = -1
        self.unpack_row = row_unpacker or self._unpack_row

        if per_row_callable is None:
            self.custom_callable = False
            self.rows = []
            self.per_row_callable = self.rows.append
        else:
            self.custom_callable = True
            self.rows = None
            self.per_row_callable = per_row_callable

    def cleanup(self):
        self.rows = None
        self.ranges = None
        self.unpack_row = None
        self.per_row_callable = None

    def __del__(self):
        pypy.lprint("%s DEL %s", args=(self.protocol.id, self.name,),
                    color='blue')

        self.cleanup()
        try:
            self.statement.reap(self)
        except Exception:
            print(traceback.format_exc())

    def add_custom_row_unpacker(self, unpacker):
        if not callable(unpacker):
            msg = "Supplied value for unpacker '%s' is type '%s'. Expected " \
                  "callable"
            raise Exception(msg % (unpacker, type(unpacker)))
        self.unpack_row = unpacker

    def _unpack_row(self, data):
        data_idx = 2
        fields = self.statement.fields[self.protocol.id]
        row = pypy.newlist_hint(len(fields))
        for field in fields:
            vlen = binary.i_unpack(data, data_idx)[0]
            data_idx += 4
            if vlen == -1:
                row.append(None)
            else:
                row.append(field.decoder(data, data_idx, vlen))
                data_idx += vlen

        return self.statement.result_record_namedtuple(*row)

    def handle_portal_suspended(self):
        if self.ranges is None:
            self._fetchall()
            return

        """
        Check to see if the current range is a fetchall:
         - The upper index is infinity
         - We have already received some rows for this range

        If we have not already received rows for this range, it is likely that
        a preceding range has just been popleft-ed from the self.ranges deque
        and that an execute is already in progress for that range.
        """
        r = self.ranges[0]
        if r[2] and r[1] == self.infinity:
            self._fetchall()

    def handle_command_complete(self, command_tag, row_count):
        """
        Postgres has notified us that it doesn't have any more rows for this
        query. We may have a range (for fetchall or fetchmany) that is waiting
        for more rows, so we close and return it.
        We may also have other requests after that... requests that will never
        receive rows, so we return the empty list for those.
        """
        while self.ranges:
            vmin, vmax, rows, df = self.ranges.popleft()
            try:
                df.callback(rows)
            except Exception as e:
                print(e)

    def handle_row(self, data):
        """
        Receives bytes from postgres. There are two steps:
        1:  Unpack the bytes into Python data structures
        2:  Push the Python data to the correct location
        """
        self.rowindex += 1
        try:
            row = self.unpack_row(data)
            '''
            try:
                row2 = self._unpack_row(data)
            except Exception as e:
                print("Failed to unpack {row}: {exc!r}".format(row=row, exc=e))

            if not row == row2:
                print("ROW MISMATCH: \n%s\n%s" % (row, row2,))
            '''
        except Exception as e:
            b64row = base64.b64encode(data)
            self.log.error("Failed to unpack {row}: {exc!r}",
                           row=b64row, exc=e)
            print("Failed to unpack {row}: {exc!r}".format(row=b64row, exc=e))
            raise

        if self.ranges is None:
            # The simple case where we're not servicing a cursor
            try:
                self.per_row_callable(row)
                return
            except Exception as e:
                m = "Failed to execute callable {method!r} on {row!r}: {exc!r}"
                self.log.error(m, method=self.per_row_callable, row=row, exc=e)
                raise

        try:
            vmin, vmax, rows, df = self.ranges[0]
            if not vmin <= self.rowindex <= vmax:
                raise Exception("Found a range that should have been popped")

            try:
                if self.custom_callable:
                    self.per_row_callable(row)
                    if not rows:
                        rows.append(0)
                else:
                    rows.append(row)
            except Exception as e:
                msg = "Failed to handle row {r!r} with method {m!r}: {e!r}"
                self.log.error(msg, m=self.per_row_callable, r=row, e=e)
                raise

            if self.rowindex == vmax:
                self.ranges.popleft()
                if self.custom_callable:
                    df.callback(None)
                else:
                    df.callback(rows)
        except Exception as e:
            m = "Failed to handle row {row!r} with method {method!r}: {exc!r}"
            self.log.error(m, method=self.per_row_callable, row=row, exc=e)
            raise

    def bind_message(self):
        if self.cached_bind_message:
            return self.cached_bind_message

        id = self.protocol.id
        bind_message = pypy.BytesBuilder()
        bind_message.append(self.name)
        bind_message.append(self.statement.bind_message_prefix[id])

        if self.parameters:
            for func, parameter in zip(self.statement.parameter_functions[id],
                                       self.parameters):
                if parameter is None:
                    val = self.NULL
                else:
                    val = func(parameter)
                    bind_message.append(binary.i_pack(len(val)))
                bind_message.append(val)
        bind_message.append(self.statement.bind_message_suffix[id])
        bind_message_bytes = bind_message.build()
        self.cached_bind_message = self.BIND + \
            binary.i_pack(len(bind_message_bytes) + 4) + bind_message_bytes
        return self.cached_bind_message

    def _makerange(self, increment):
        """
        The user may issue fetchone, fetchmany and fetchall without waiting for
        results, so when we receive rows, we need to know which fetchX call the
        row is for, and which deferred should (eventually) be called with that
        record in the result.
        So for each fetchX call, we create a range tuple and add it to the
        self.ranges deque.
        The range tuple looks like this:
         (start_index,
          end_index,
          [],
          df)
        Each received row is added to the list, and when the correct number of
        rows have been received, we call df.callback(<list>)
        A query that just expects one row in the result might call fetchone,
        which would result in this range tuple:

         (0,
          0,
          [],
          df)
        In the special case of fetchone, we will do:
        df.callback(<list>[0])
        See the handle_row method
        """
        df = self.make_df()

        # The last row range requested ended at row 'lastindex'
        if self.ranges:
            lastindex = self.ranges[-1][1]
        else:
            lastindex = self.rowindex  # -1
            self.ranges = deque()

        self.ranges.append((lastindex+1,
                            lastindex+increment,
                            [],
                            df))
        return df

    def fetchall(self):
        df = self._makerange(self.infinity)
        self._fetchall()
        return df

    def _fetchall(self, ignored=None):
        """
        Request rows from the database in batches of 2000
        """
        pypy.lprint("%s Portal._fetchall %s", args=(self.protocol.id, self.name,), color="yellow")
        execute_message = self.name + self.twok_bin
        df = self.execute_cursor(execute_message)
        df.addErrback(self.eb)
        return df

    def _fetchone(self, records):
        return records[0]

    def fetchone(self):
        execute_message = self.name + self.one_bin
        df = self._makerange(1)
        df.addCallback(self._fetchone)
        self.execute_cursor(execute_message)
        return df

    def fetchmany(self, count):
        execute_message = self.name + binary.i_pack(count)
        df = self._makerange(count)
        self.execute_cursor(execute_message)
        return df

    def execute_cursor(self, execute_message):
        return self.protocol.execute_portal(self, execute_message)

    def eb(self, f):
        print(f.getBriefTraceback())
