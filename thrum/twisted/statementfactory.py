#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Create and cache prepared statements, keyed on a checksum of the query, the
row_cache_size and the encoding
"""

import six
import hashlib
import weakref

from . import statement as pgstatement


class PreparedStatementFactory(object):
    cache = weakref.WeakValueDictionary()
    row_cache_size = 100

    @classmethod
    def create(cls, statement, name=None, row_cache_size=None,
               encoding="utf-8"):

        statement_data = cls.evaluate_statement(statement,
                                                name,
                                                row_cache_size,
                                                encoding)

        bytes_name, row_cache_size = statement_data

        if bytes_name not in cls.cache:
            cls.cache[bytes_name] = pgstatement.PreparedStatement(
                statement,
                bytes_name,
                row_cache_size,
                encoding
            )

        return cls.cache[bytes_name]

    @classmethod
    def evaluate_statement(cls, statement, name=None, row_cache_size=None,
                           encoding="utf-8"):

        if row_cache_size is None:
            row_cache_size = cls.row_cache_size

        if name is None:
            fingerprint = u"%s_%s_%s" % (statement, row_cache_size, encoding)
            bytes_md5 = cls.md5sum(fingerprint.encode(encoding))
            bytes_name = bytes_md5 + b"_ps\x00"
        else:
            bytes_name = name.encode(encoding) + b'\x00'

        return (bytes_name, row_cache_size)

    @classmethod
    def md5sum(cls, data):
        return hashlib.md5(data).hexdigest().encode("ascii")


def PreparedStatement(statement, name=None, row_cache_size=None,
                      encoding="utf-8"):
    return PreparedStatementFactory.create(statement, name, row_cache_size,
                                           encoding)
