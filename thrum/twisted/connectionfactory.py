#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

"""
Twisted factory for creating PostgresProtocol instances on a successful
connection to Postgres
"""

import getpass
import os
import re
import collections
from twisted.internet import protocol as txprotocol
from twisted.internet import defer
from twisted.internet import reactor
from twisted.python import log
from twisted.logger import Logger

from thrum import pypy
from . import protocol as pgprotocol


class PostgresProtocolFactoryCache(object):
    cache = {}

    @classmethod
    def get(cls, *args, **kwargs):
        l = []
        for key in sorted(kwargs.keys()):
            l.append((key, kwargs[key]))
        key = tuple(l)
        if key not in cls.cache:
            cls.cache[key] = _PostgresProtocolFactory(*args, **kwargs)
        return cls.cache[key]

    @classmethod
    def clear_cache(cls):
        cls.cache = {}


class _PostgresProtocolFactory(txprotocol.ClientFactory):

    protocol = pgprotocol.PostgresProtocol
    socket_regex = re.compile(".s.PGSQL.(\d+)$")
    log = Logger()

    def __init__(self, dsn=None, *, user=None, password=None, dbname=None,
                 host="127.0.0.1", port=5432,
                 message_length_bytes=4,
                 min_connections=3, max_connections=16):

        default_credentials = self.get_default_credentials()
        if dsn is not None:
            import pgconnstr
            parsed_connstr = dict(pgconnstr.ConnectionString(dsn))
            default_credentials.update(parsed_connstr)

        self.user = default_credentials.get("user", user)
        self.password = default_credentials.get("password", password)
        self.database = default_credentials.get("dbname", dbname)
        self.host = default_credentials.get("host", host)
        self.port = default_credentials.get("port", port)
        self.message_length_bytes = message_length_bytes

        self._count = 0
        self._queue = collections.deque()
        self._connections = []
        self._pending = 0
        self._min_connections = min_connections
        self._max_connections = max_connections

    def get_default_credentials(self):
        user = getpass.getuser()

        default_pghost = os.getenv("PGHOST")
        default_pghostaddr = os.getenv("PGHOSTADDR")
        default_pgport = os.getenv("PGPORT", 5432)
        default_pgpassword = os.getenv("PGPASSWORD")
        user = os.getenv("PGUSER", user)
        database = os.getenv("PGDATABASE", user)

        if default_pghostaddr is not None:
            default_host = default_pghostaddr
        elif default_pghost is not None:
            default_host = default_pghost
        else:
            default_host = self.find_socket(default_pgport)

        return {
            "user": user,
            "password": default_pgpassword,
            "database": database,
            "host": default_host,
            "port": default_pgport,
        }

    def find_socket(self, port):
        """If no host was specified, return a path to the unix domain socket.

        Args:
            port (int): TCP connection port.

        Returns:
            str: Absolute filepath of the unix domain socket.
        """
        unix_socket_directory = "/var/run/postgresql"
        try:
            results = os.listdir(unix_socket_directory)
        except FileNotFoundError:
            return None

        viable_candidates = []
        for candidate in results:
            candidate = os.path.join(unix_socket_directory, candidate)
            suffix = candidate.rsplit(".", 1)[-1]
            if suffix.isdigit():
                try:
                    if int(suffix) == port:
                        return candidate
                except Exception:
                    # Failed to cast suffix to int? Weird.
                    pass
                viable_candidates.append(candidate)

        if len(viable_candidates) == 1:
            return viable_candidates[0]

        # Guessing
        return viable_candidates[0]

    def startedConnecting(self, connector):
        txprotocol.ClientFactory.startedConnecting(self, connector)

    def buildProtocol(self, addr):
        p = self.protocol(self.user, self.password, self.database,
                          self.message_length_bytes)
        p.factory = self
        self._connections.append(p)
        p.id = self._count
        self._count += 1

        connect_df = self._queue.popleft()
        p.ready_for_query.chainDeferred(connect_df)

        return p

    def clientConnectionLost(self, connector, reason):
        pypy.lprint("_PostgresProtocolFactory.clientConnectionLost: %s", (reason,), color='red')
        txprotocol.ClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        pypy.lprint("_PostgresProtocolFactory.clientConnectionFailed: %s", (reason,), color='red')
        txprotocol.ClientFactory.clientConnectionFailed(self, connector, reason)

    def connect(self):
        pypy.lprint("_PostgresProtocolFactory.connect: %s", (self.host,), color='red')
        if '/' in self.host:
            socketpath = None
            try:
                match = self.socket_regex.search(self.host)
                if match:
                    socketpath = self.host
                else:
                    candidates = os.listdir(self.host)
                    for candidate in candidates:
                        match = self.socket_regex.search(candidate)
                        if match:
                            socketpath = os.path.join(self.host, candidate)
                            break
            except Exception as e:
                self.log.error("Bad socket path '{path!r}': {exc!r}",
                               path=self.host,
                               exc=e)
                raise

            if not socketpath:
                msg = "Failed to find a socket path from '%s'" % (self.host,)
                self.log.error(msg)
                raise Exception(msg)
            reactor.connectUNIX(socketpath, self)
        else:
            reactor.connectTCP(self.host, self.port, self)

        df = defer.Deferred()
        self._queue.append(df)
        return df

    def stopFactory(self):
        """
        This will be called before I stop listening on all Ports/Connectors.
        This can be overridden to perform 'shutdown' tasks such as
        disconnecting database connections, closing files, etc.
        It will be called, for example, before an application shuts down, if it
        was connected to a port. User code should not call this function
        directly.
        """
        txprotocol.ClientFactory.stopFactory(self)

    def shutdown(self):
        """
        Call when you want to close all connections
        """
        self.stopTrying()
        for connection in self._connections:
            try:
                connection.disconnect()
            except Exception as e:
                self.log.err(e)


def PostgresProtocolFactory(*args, **kwargs):
    return PostgresProtocolFactoryCache.get(*args, **kwargs)

