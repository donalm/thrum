#!/usr/bin/env python

import weakref
import collections

from twisted.internet import defer

from thrum import constants
from thrum import binary
from thrum import pypy
from thrum import dynamic
from . import portal as pgportal


class PreparedStatement(object):
    """
    In the event of a rolling schema change applied across a cluster of
    databases, each connection to ostensibly the same database could return
    different datatypes for a given field. For example if an int4 is changed to
    an int8. For that reason, we keep a version of some fields for each
    protocol instance.

    These fields are:
    bind_message_suffix
    bind_message_prefix
    parameter_functions

    It is also possible that any custom row-handler or per-row-callback would
    need to be modified to accomodate the different data type, but as those
    are already supplied on a per-portal basis, that is left as a problem for
    implementers.
    """

    IDLE = 0
    PARSING = 1
    READY = 2

    unpacker_count = 0
    row_cache_size = 1000
    zero = binary.h_pack(0)
    double_0 = zero + zero
    portal_count = 0

    __slots__ = ('__weakref__',
                 'bind_message_prefix',
                 'bind_message_suffix',
                 'bytes_name',
                 'bytes_statement',
                 'client_encoding',
                 'protocols',
                 'compiled_method',
                 'protocol_method_cache',
                 'default_row_cache_size',
                 'fields',
                 'id',
                 'parameter_functions',
                 'parse_message',
                 'parsed',
                 'portal_base_name',
                 'portal_cache',
                 'result_record_class',
                 'result_record_namedtuple',
                 'row_unpack_methods'
                 )

    def __init__(self, statement, bytes_name, default_row_cache_size,
                 client_encoding):

        self.client_encoding = client_encoding
        self.bytes_statement = statement.encode(client_encoding)
        self.bytes_name = bytes_name
        self.portal_base_name = b"%09d_" + self.bytes_name
        self.default_row_cache_size = default_row_cache_size

        self.bind_message_suffix = collections.defaultdict(
            lambda: self.zero)
        self.bind_message_prefix = collections.defaultdict(
            lambda: self.bytes_name + self.double_0)
        self.parameter_functions = collections.defaultdict(tuple)
        self.parse_message = b''.join((self.bytes_name, self.bytes_statement,
                                       constants.NULL_BYTE, self.zero))
        self.portal_cache = collections.defaultdict(collections.deque)
        self.fields = {}
        self.parsed = {}
        self.row_unpack_methods = {}
        self.protocol_method_cache = {}
        self.compiled_method = None
        self.protocols = weakref.WeakValueDictionary()
        self.result_record_class = None
        self.result_record_namedtuple = None

    def reap(self, portal):
        """
        When a portal is about to be deleted it will call this method so that
        it can be re-added to the cache instead
        """
        self.portal_cache[portal.protocol.id].append(portal)
        return

    def describe_complete(self, protocol):
        self.parsed[protocol.id] = True
        return self

    def close(self):
        for protocol in self.protocols.values():
            protocol.pg_close_statement(self)

    def lose_protocol(self, protocol):
        try:
            del self.portal_cache[protocol.id]
        except KeyError:
            pass

        try:
            del self.fields[protocol.id]
        except KeyError:
            pass

        try:
            del self.protocols[protocol.id]
        except KeyError:
            pass

        try:
            del self.protocol_method_cache[protocol.id]
        except KeyError:
            pass

        try:
            del self.parsed[protocol.id]
        except KeyError:
            pass

    def update_row_description(self, protocol, fields):
        '''
        "name"
        "preferred_format_code"
        "decoder"
        "table_oid"
        "column_attrnum"
        "type_oid"
        "type_size"
        "type_modifier"
        "format_code"
        '''

        unique_names = collections.defaultdict(int)
        """
        Ensure that all the sanitized names are unique
        """
        for index, field in enumerate(fields):
            if field.name_sanitized not in unique_names:
                unique_names[field.name_sanitized] = 0
                continue

            unique_names[field.name_sanitized] += 1
            name_sanitized = "%s_%02d" % (field.name_sanitized,
                                          unique_names[field.name_sanitized])

            field = protocol.field_tuple(field.name,
                                         name_sanitized,
                                         field.preferred_format_code,
                                         field.decoder,
                                         field.notnull,
                                         field.table_oid,
                                         field.column_attrnum,
                                         field.type_oid,
                                         field.type_size,
                                         field.type_modifier,
                                         field.format_code)
            fields[index] = field

        self.fields[protocol.id] = tuple(fields)
        self.protocols[protocol.id] = protocol

        fieldnames = [field.name_sanitized for field in fields]
        self.result_record_namedtuple = collections.namedtuple("Record",
                                                               fieldnames)

        output_fc = [field.preferred_format_code for field in fields]
        packer = binary.Cache.get_pack_shorts_for_index(len(output_fc))
        self.bind_message_suffix[protocol.id] = \
            binary.h_pack(len(output_fc)) + packer(*output_fc)

    def compile_custom_unpacker(self, protocol):
        df = self.retrieve_field_metadata(protocol)
        df.addCallback(self._compile_custom_unpacker, protocol)
        return df

    def retrieve_field_metadata(self, protocol):
        msg = "%s PreparedStatement.retrieve_field_metadata 1"
        pypy.lprint(msg, (protocol.id,), color='cyan')

        if protocol.id not in self.fields:
            # The query doesn't return any data, so we did not receive a
            # RowDescription message for this query.
            # The query is probably a delete/insert/update.
            return defer.succeed([])

        fields = self.fields[protocol.id]
        query = """
                    SELECT
                        attrelid,
                        attnum,
                        attnotnull
                    FROM
                        pg_attribute
                    WHERE
                        attrelid IN ($1)
                """
        params = tuple(set([field.table_oid for field in fields]))
        df = protocol.execute(query, parameters=params)
        df.addCallback(self._retrieve_field_metadata, protocol)
        return df

    def _retrieve_field_metadata(self, result, protocol):
        pypy.lprint("%s PreparedStatement._retrieve_field_metadata: %s",
                    args=(protocol.id, result,), color="white")

        rows = result[0]
        tables = collections.defaultdict(dict)
        fields = self.fields[protocol.id]

        for row in rows:
            table, field, notnull = row
            tables[table][field] = notnull  # noqa

        new_fields = []
        for index, field in enumerate(fields):
            try:
                notnull = tables[field.table_oid][field.column_attrnum]
            except Exception:
                notnull = False
            f = protocol.field_tuple(field.name,
                                     field.name_sanitized,
                                     field.preferred_format_code,
                                     field.decoder,
                                     notnull,
                                     field.table_oid,
                                     field.column_attrnum,
                                     field.type_oid,
                                     field.type_size,
                                     field.type_modifier,
                                     field.format_code)
            new_fields.append(f)

        self.fields[protocol.id] = new_fields
        return new_fields

    def _compile_custom_unpacker(self, fields, protocol):
        self.update_row_description(protocol, fields)

        method_key = tuple(
            [(field.name, field.type_oid, field.notnull) for field in fields]
        )

        if method_key in self.row_unpack_methods:
            self.protocol_method_cache[protocol.id] = method_key
            return self

        row_method = dynamic.row_unpacker(fields,
                                          protocol,
                                          self.result_record_namedtuple)

        self.compiled_method = row_method
        self.row_unpack_methods[method_key] = row_method
        self.protocol_method_cache[protocol.id] = method_key
        return self

    def update_parameter_data(self, protocol_id, functions, format_codes):
        self.parameter_functions[protocol_id] = tuple(functions)

        param_count = len(format_codes)
        try:
            packer = binary.Cache.pack_shorts[param_count]
        except Exception:
            packer = binary.Cache.get_pack_shorts_for_index(param_count)
        packed_format_codes = packer(*format_codes)

        bytes_param_count = binary.h_pack(param_count)
        self.bind_message_prefix[protocol_id] = self.bytes_name + \
            bytes_param_count + \
            packed_format_codes + \
            bytes_param_count

    def new_portal(self, protocol, parameters=None, row_cache_size=None,
                   row_unpacker=None, per_row_callable=None):


        try:
            if row_unpacker is None:
                method_key = self.protocol_method_cache[protocol.id]
                row_unpacker = self.row_unpack_methods[method_key]
        except KeyError:
            pass

        PreparedStatement.portal_count = PreparedStatement.portal_count + 1
        if PreparedStatement.portal_count > 999999999:
            PreparedStatement.portal_count = 0

        bytes_portal_name = \
            self.portal_base_name % (PreparedStatement.portal_count,)

        try:
            portal = self.portal_cache[protocol.id].popleft()
            oldname = portal.name
            portal.reset(parameters, bytes_portal_name, row_cache_size,
                         row_unpacker, per_row_callable)
            msg = "%s statement.new_portal: RECYCLED: %s old:%s new:%s"
            pypy.lprint(msg, (protocol.id, portal, oldname, portal.name,),
                        color="yellow")
            return portal
        except IndexError:
            pass

        if row_cache_size is None:
            row_cache_size = self.row_cache_size

        portal = pgportal.Portal(protocol, bytes_portal_name, parameters,
                                 row_cache_size, self, row_unpacker,
                                 per_row_callable)
        pypy.lprint("%s statement.new_portal: %s", (protocol.id, portal),
                    color="yellow")

        return portal
