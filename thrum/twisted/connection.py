#!/usr/bin/env python

from twisted.internet import defer
from twisted.internet import reactor
from . import connectionfactory


class Connection(object):

    connectionFactory = staticmethod(connectionfactory.PostgresProtocolFactory)

    def __init__(self):

        # The pool_key value is set by the ConnectionPool. It uniquely
        # identfies this connection within the pool.
        self.pool_key = None

        self._protocol = None
        self._lock = defer.DeferredLock()
        reactor.addSystemEventTrigger('before', 'shutdown', self.close)

    def connect(self, *args, **kwargs):

        if self._protocol and not self._protocol.closed:
            raise Exception("Already connected")

        factory = self.connectionFactory(*args, **kwargs)
        df = factory.connect()
        df.addCallback(self._register_protocol)
        return df

    def _register_protocol(self, protocol):
        self._protocol = protocol
        return protocol

    @property
    def closed(self):
        try:
            return not self._protocol.connected
        except Exception:
            return True

    def close(self):
        return self._protocol.disconnect()
