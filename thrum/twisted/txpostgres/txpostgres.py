# -*- coding: utf-8 -*-
# Copyright (c) 2010-2012, Jan Urbanski.
# See LICENSE for details.
"""
txpostgres is a library for accessing a PostgreSQL_ database from the Twisted_
framework. It builds upon asynchronous features of the Psycopg_ database
library, which in turn exposes the asynchronous features of libpq_, the
PostgreSQL C library.

It requires a version of Psycopg that includes support for `asynchronous
connections`_ (versions 2.2.0 and later) and a reasonably recent Twisted (it
has been tested with Twisted 10.2 onward). Alternatively, psycopg2cffi_ or
psycopg2-ctypes_ can be used in lieu of Psycopg.

txpostgres tries to present an interface that will be familiar to users of both
Twisted and Psycopg. It features a :class:`~txpostgres.txpostgres.Cursor`
wrapper class that mimics the interface of a Psycopg :psycopg:`cursor
<cursor.html#cursor>` but returns :d:`Deferred` objects. It also provides a
:class:`~txpostgres.txpostgres.Connection` class that is meant to be a drop-in
replacement for Twisted's :tm:`adbapi.Connection
<enterprise.adbapi.Connection>` with some small differences regarding
connection establishing.

The main advantage of txpostgres over Twisted's built-in database support is
non-blocking connection building and complete lack of thread usage.

The library is distributed under the MIT License, see the LICENSE file for
details. You can contact the author, Jan Urbański, at wulczer@wulczer.org. Feel
free to download the source_, file bugs in the `issue tracker`_ and consult the
documentation_

.. _PostgreSQL: http://www.postgresql.org/
.. _Twisted: http://twistedmatrix.com/
.. _Psycopg: http://initd.org/psycopg/
.. _Python: http://www.python.org/
.. _libpq: http://www.postgresql.org/docs/current/static/libpq-async.html
.. _`asynchronous connections`:
    http://initd.org/psycopg/docs/advanced.html#async-support
.. _psycopg2cffi: https://github.com/chtd/psycopg2cffi
.. _psycopg2-ctypes: http://pypi.python.org/pypi/psycopg2ct
.. _source: https://github.com/wulczer/txpostgres
.. _issue tracker: https://github.com/wulczer/txpostgres/issues
.. _documentation: http://txpostgres.readthedocs.org/
"""

from twisted.internet import main, defer, task
from twisted.python import failure, log

from thrum.twisted import pool as thrum_pool
from thrum.twisted import connection as thrum_connection


__all__ = ['Connection', 'Cursor', 'ConnectionPool',
           'AlreadyConnected', 'RollbackFailed'
           ]


class _CancelInProgress(Exception):
    """
    A query cancellation is in progress.
    """


class Cursor():
    """
    A wrapper for a psycopg2 asynchronous cursor.

    The wrapper will forward almost everything to the wrapped cursor, so the
    usual DB-API interface can be used, but it will return :d:`Deferred`
    objects that will fire with the DB-API results.

    Remember that the PostgreSQL protocol does not support concurrent
    asynchronous queries execution, so you need to take care not to execute a
    query while another is still being processed.

    In most cases you should just use the
    :class:`~txpostgres.txpostgres.Connection` methods that will handle the
    locking necessary to prevent concurrent query execution.
    """

    def __init__(self, cursor, connection):
        self.reactor = connection.reactor
        self.prefix = "cursor"

        self._connection = connection
        self._cursor = cursor

    def execute(self, query, params=None):
        """
        A regular DB-API execute, but returns a :d:`Deferred`.

        The caller must be careful not to call this method twice on cursors
        from the same connection without waiting for the previous execution to
        complete.

        :return: A :d:`Deferred` that will fire with the results of the
            DB-API execute.
        """
        return self._doit('execute', query, params)

    def callproc(self, procname, params=None):
        """
        A regular DB-API callproc, but returns a :d:`Deferred`.

        The caller must be careful not to call this method twice on cursors
        from the same connection without waiting for the previous execution to
        complete.

        :return: A :d:`Deferred` that will fire with the results of the
            DB-API callproc.
        """
        raise NotImplementedError()

    def close(self):
        """
        Close the cursor.

        Once closed, the cursor cannot be used again.

        :returns: :class:`None`
        """
        return self._cursor.close()


class AlreadyConnected(Exception):
    """
    The database connection is already open.
    """


class RollbackFailed(Exception):
    """
    Rolling back the transaction failed, the connection might be in an unusable
    state.

    :var connection: The connection that failed to roll back its transaction.
    :vartype connection: :class:`~txpostgres.txpostgres.Connection`

    :var originalFailure: The failure that caused the connection to try to
        roll back the transaction.
    :vartype originalFailure: a Twisted :tm:`Failure <python.failure.Failure>`
    """

    def __init__(self, connection, originalFailure):
        self.connection = connection
        self.originalFailure = originalFailure

    def __str__(self):
        return "<RollbackFailed, original error: %s>" % self.originalFailure


class Connection(thrum_connection.Connection):
    """
    A wrapper for a psycopg2 asynchronous connection.

    The wrapper forwards almost everything to the wrapped connection, but
    provides additional methods for compatibility with :tm:`adbapi.Connection
    <enterprise.adbapi.Connection>`.

    :param reactor: A Twisted reactor or :class:`None`, which means the current
        reactor

    :param cooperator: A Twisted :tm:`Cooperator <internet.task.Cooperator>` to
        process :pg:`NOTIFY <notify>` events or :class:`None`, which means
        using :tm:`task.cooperate <internet.task.cooperate>`

    :var connectionFactory: The factory used to produce connections, defaults
        to :psycopg:`psycopg2.connect <module.html#psycopg2.connect>`
    :vartype connectionFactory: any callable

    :var cursorFactory: The factory used to produce cursors, defaults to
        :class:`~txpostgres.txpostgres.Cursor`
    :vartype cursorFactory: a callable accepting two positional arguments, a
        :psycopg:`psycopg2.cursor <cursor.html#cursor>` and a
        :class:`~txpostgres.txpostgres.Connection`
    """

    cursorFactory = Cursor

    def __init__(self, reactor=None, cooperator=None, detector=None):
        thrum_connection.Connection.__init__(self)

        self.reactor = reactor
        self.cooperator = cooperator
        self.detector = detector
        self.prefix = "connection"

    def runQuery(self, *args, **kwargs):
        """
        Execute an SQL query and return the result.

        An asynchronous cursor will be created and its
        :meth:`~txpostgres.txpostgres.Cursor.execute` method will be invoked
        with the provided arguments. After the query completes the results will
        be fetched and the returned :d:`Deferred` will fire with the result.

        The connection is always in autocommit mode, so the query will be run
        in a one-off transaction. In case of errors a :tm:`Failure
        <python.failure.Failure>` will be returned.

        It is safe to call this method multiple times without waiting for the
        first query to complete.

        :return: A :d:`Deferred` that will fire with the return value of the
            cursor's :meth:`fetchall` method.
        """
        return self._protocol.runQuery(*args, **kwargs)

    def runOperation(self, *args, **kwargs):
        """
        Execute an SQL query and discard the result.

        Identical to :meth:`~txpostgres.txpostgres.Connection.runQuery`, but
        the result won't be fetched and instead :class:`None` will be
        returned. It is intended for statements that do not normally return
        values, like INSERT or DELETE.

        It is safe to call this method multiple times without waiting for the
        first query to complete.

        :return: A :d:`Deferred` that will fire :class:`None`.
        """
        return self._protocol.runOperation(*args, **kwargs)

    def runInteraction(self, interaction, *args, **kwargs):
        """
        Run commands in a transaction and return the result.

        :obj:`interaction` should be a callable that will be passed a
        :class:`~txpostgres.txpostgres.Cursor` object. Before calling
        :obj:`interaction` a new transaction will be started, so the callable
        can assume to be running all its commands in a transaction. If
        :obj:`interaction` returns a :d:`Deferred` processing will wait for it
        to fire before proceeding. You should not close the provided
        :class:`~txpostgres.txpostgres.Cursor`.

        After :obj:`interaction` finishes work the transaction will be
        automatically committed. If it raises an exception or returns a
        :tm:`Failure <python.failure.Failure>` the connection will be rolled
        back instead.

        If committing the transaction fails it will be rolled back instead and
        the failure obtained trying to commit will be returned.

        If rolling back the transaction fails the failure obtained from the
        rollback attempt will be logged and a
        :exc:`~txpostgres.txpostgres.RollbackFailed` failure will be
        returned. The returned failure will contain references to the original
        failure that caused the transaction to be rolled back and to the
        :class:`~txpostgres.txpostgres.Connection` in which that happened, so
        the user can take a decision whether she still wants to be using it or
        just close it, because an open transaction might have been left open in
        the database.

        It is safe to call this method multiple times without waiting for the
        first query to complete.

        :param interaction: A callable whose first argument is a
            :class:`~txpostgres.txpostgres.Cursor`.
        :type interaction: any callable

        :return: A :d:`Deferred` that will fire with the return value of
            :obj:`interaction`.
        """
        return self._protocol.runInteraction(*args, **kwargs)

    def cancel(self, d):
        """
        Cancel the current operation. The cancellation does not happen
        immediately, because the PostgreSQL protocol requires that the
        application waits for confirmation after the query has been cancelled.
        Be careful when cancelling an interaction, because if the interaction
        includes sending multiple queries to the database server, you can't
        really be sure which one are you cancelling.

        :param d: a :d:`Deferred` returned by one of
            :class:`~txpostgres.txpostgres.Connection` methods.
        """
        raise NotImplementedError()

    def checkForNotifies(self):
        """
        Check if :pg:`NOTIFY <notify>` events have been received and if so,
        dispatch them to the registered observers, using the :tm:`Cooperator
        <internet.task.Cooperator>` provided in the constructor. This is done
        automatically, user code should never need to call this method.
        """
        raise NotImplementedError()

    def addNotifyObserver(self, observer):
        """
        Add an observer function that will get called whenever a :pg:`NOTIFY
        <notify>` event is delivered to this connection. Any number of
        observers can be added to a connection. Adding an observer that's
        already been added is ignored.

        Observer functions are processed using the :tm:`Cooperator
        <internet.task.Cooperator>` provided in the constructor to avoid
        blocking the reactor thread when processing large numbers of events. If
        an observer returns a :d:`Deferred`, processing waits until it fires or
        errbacks.

        There are no guarantees as to the order in which observer functions are
        called when :pg:`NOTIFY <notify>` events are delivered. Exceptions in
        observers are logged and discarded.

        :param observer: A callable whose first argument is a
            :psycopg:`psycopg2.extensions.Notify
            <extensions.html#psycopg2.extensions.Notify>`.
        :type observer: any callable
        """
        raise NotImplementedError()

    def removeNotifyObserver(self, observer):
        """
        Remove a previously added observer function. Removing an observer
        that's never been added will be ignored.

        :param observer: A callable that should no longer be called on
            :pg:`NOTIFY <notify>` events.
        :type observer: any callable
        """
        raise NotImplementedError()

    def getNotifyObservers(self):
        """
        Get the currently registered notify observers.

        :return: A set of callables that will get called on :pg:`NOTIFY
            <notify>` events.
        :rtype: :class:`set`
        """
        raise NotImplementedError()


class ConnectionPool(object):
    """
    A poor man's pool of :class:`~txpostgres.txpostgres.Connection` instances.

    :var min: The amount of connections that will be open when
        :meth:`~ConnectionPool.start` is called. The pool never opens or closes
        connections on its own after starting. Defaults to 3.
    :vartype min: int

    :var connectionFactory: The factory used to produce connections, defaults
        to :class:`~txpostgres.txpostgres.Connection`.
    :vartype connectionFactory: any callable

    :var reactor: The reactor passed to :attr:`.connectionFactory`.
    :var cooperator: The cooperator passed to :attr:`.connectionFactory`.
    """

    min = 3
    connectionFactory = Connection
    reactor = None
    cooperator = None

    def __init__(self, _ignored, *connargs, **connkw):
        """
        Create a new connection pool.

        Any positional or keyword arguments other than the first one and a
        :obj:`min` keyword argument are passed to :attr:`connectionFactory`
        when connecting. Use these arguments to pass database names, usernames,
        passwords, etc.

        :param _ignored: Ignored, for :tm:`adbapi.ConnectionPool
            <enterprise.adbapi.ConnectionPool>` compatibility.
        :type _ignored: any object
        """

        self.thrum_pool = thrum_pool.ConnectionPool(
            *connargs,
            **connkw
        )
        '''
        if not self.reactor:
            from twisted.internet import reactor
            self.reactor = reactor
        # for adbapi compatibility, min can be passed in kwargs
        if 'min' in connkw:
            self.min = connkw.pop('min')
        self.connargs = connargs
        self.connkw = connkw
        self.connections = set(
            [self.connectionFactory(self.reactor, self.cooperator)
             for _ in range(self.min)])

        # to avoid checking out more connections than there are pooled in total
        self._semaphore = defer.DeferredSemaphore(self.min)
        '''

    def start(self):
        """
        Start the connection pool.

        This will create as many connections as the pool's :attr:`min` variable
        says.

        :return: A :d:`Deferred` that fires when all connection have succeeded.
        """
        return self.thrum_pool.start()

    def close(self):
        """
        Stop the pool.

        Disconnects all connections.

        :return: A :d:`Deferred` that fires when all connections have closed.
        """
        return self.thrum_pool.shutdown()

    def remove(self, connection):
        """
        Remove a connection from the pool.

        Provided to be able to remove broken connections from the pool. The
        caller should make sure the removed connection does not have queries
        pending.

        :param connection: The connection to be removed.
        :type connection: an object produced by the pool's
            :attr:`connectionFactory`
        """
        return self.thrum_pool.replace_bad_connection(connection)

    def add(self, connection):
        """
        Add a connection to the pool.

        Provided to be able to extend the pool with new connections.

        :param connection: The connection to be added.
        :type connection: an object compatible with those produced
            by the pool's :attr:`connectionFactory`
        """
        raise NotImplementedError()

    def runQuery(self, *args, **kwargs):
        """
        Execute an SQL query using a pooled connection and return the result.

        One of the pooled connections will be chosen, its
        :meth:`~txpostgres.txpostgres.Connection.runQuery` method will be
        called and the resulting :d:`Deferred` will be returned.

        :return: A :d:`Deferred` obtained by a pooled connection's
            :meth:`~txpostgres.txpostgres.Connection.runQuery`
        """
        return self.thrum_pool.runQuery(*args, **kwargs)

    def runOperation(self, *args, **kwargs):
        """
        Execute an SQL query using a pooled connection and discard the result.

        One of the pooled connections will be chosen, its
        :meth:`~txpostgres.txpostgres.Connection.runOperation` method will be
        called and the resulting :d:`Deferred` will be returned.

        :return: A :d:`Deferred` obtained by a pooled connection's
            :meth:`~txpostgres.txpostgres.Connection.runOperation`
        """
        return self.thrum_pool.runOperation(*args, **kwargs)

    def runInteraction(self, interaction, *args, **kwargs):
        """
        Run commands in a transaction using a pooled connection and return the
        result.

        One of the pooled connections will be chosen, its
        :meth:`~txpostgres.txpostgres.Connection.runInteraction` method will be
        called and the resulting :d:`Deferred` will be returned.

        :param interaction: A callable that will be passed to
            :meth:`Connection.runInteraction
            <txpostgres.Connection.runInteraction>`
        :type interaction: any callable

        :return: A :d:`Deferred` obtained by a pooled connection's
            :meth:`Connection.runInteraction
            <txpostgres.Connection.runInteraction>`
        """
        return self.thrum_pool.runInteraction(interaction, *args, **kwargs)
