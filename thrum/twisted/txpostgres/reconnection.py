"""
Clone of the txpostgres module with the code stubbed out. Provided so that
curious users can test thrum by just updating the txpostgres imports in their
Python code.

Originally: Reconnection support for txpostgres.
"""

from twisted.python import log


class ConnectionDead(Exception):
    """
    The connection is dead.
    """


def defaultDeathChecker(f):
    """
    Checker function suitable for use with
    :class:`.DeadConnectionDetector`.
    """
    pass


def defaultReconnectionIterator():
    """
    A function returning sane defaults for a reconnection iterator, for use
    with :class:`.DeadConnectionDetector`.

    The defaults have maximum reconnection delay capped at 10 seconds and no
    limit on the number of retries.
    """
    pass


class DeadConnectionDetector(object):
    """
    A class implementing reconnection strategy. When the connection is
    discovered to be dead, it will start the reconnection process.

    The object being reconnected should proxy all operations through the
    detector's :meth:`.callChecking` which will automatically fail them if the
    connection is currently dead. This is done to prevent sending requests to a
    resource that's not currently available.

    When an instance of :class:`~txpostgres.txpostgres.Connection` is passed a
    :class:`.DeadConnectionDetector` it automatically starts using it to
    provide reconnection.

    Another way of using this class is manually calling
    :meth:`.checkForDeadConnection` passing it a :tm:`Failure
    <python.failure.Failure>` instance to trigger reconnection. This is useful
    to handle initial connection errors, for example::

        conn = txpostgres.Connection(detector=DeadConnectionDetector())
        d = conn.connect('dbname=test')
        d.addErrback(conn.detector.checkForDeadConnection)

    :var reconnectable: An object to be reconnected. It should provide a
        `connect` and a `close` method.
    :vartype reconnectable: :class:`object`

    :var connectionIsDead: If the connection is currently believed to be dead.
    :vartype connectionIsDead: :class:`bool`
    """

    def __init__(self, deathChecker=None,
                 reconnectionIterator=None, reactor=None):
        """
        Create a new detector.

        :param deathChecker: A one-argument callable that will be called with a
            failure instance and should return True if reconnection should be
            triggered. If :class:`None` then :func:`.defaultDeathChecker` will
            be used.
        :type deathChecker: callable

        :param reconnectionIterator: A zero-argument callable that should
            return a iterator yielding reconnection delay periods. If
            :class:`None` then :func:`.defaultReconnectionIterator` will be
            used.
        :type reconnectionIterator: callable

        :param reactor: A Twisted reactor or :class:`None`, which means
            the current reactor.
        """
        log.err(
            "DeadConnectionDetector has been instanced - this is just stub"
            "code in thrum.twisted.txpostgres"
        )
