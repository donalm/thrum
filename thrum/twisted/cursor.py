#!/usr/bin/env python

import traceback
from collections import deque
from thrum import binary
from thrum import pypy
from twisted.internet import defer


try:
    from pg8000 import dbapi as pg8000_dbapi
except ImportError:
    pg8000_dbapi = None


class Cursor(object):
    twok_bin = binary.i_pack(2000)

    def __init__(self, protocol):
        pypy.lprint('%s Cursor', (protocol.id,), color='blue')
        self.connection = protocol
        self._reset()

    def _reset(self):
        self.portal = None
        self.arraysize = 2000
        self.query = None
        self.rowindex = 0
        self.bind_sent = False
        self.results = []
        self.closed = False
        self.scrollable = True
        self.withhold = True

    def setoutputsize(self, size, column=None):
        pass

    def setinputsizes(self, sizes):
        pass

    def nextset(self):
        raise NotImplementedError()

    def cast(self, oid, s):
        raise NotImplementedError()

    def scroll(self, value, mode="relative"):
        # The max index is actually one greater than the true maximum index
        # of self.results, e.g. if self.results == [0,1,2,3] then max_index
        # is 4, however we should not allow the user to scroll to that
        # index since no result can be retrieved with the 'fetch*' methods
        # at that index.
        max_index = len(self.results) - 1

        if mode == "relative":
            new_index = self.rowindex + value
        elif mode == "absolute":
            new_index = value
        else:
            raise ValueError(
                f"mode must be either 'relative' or 'absolute'; not '{mode}'"
            )

        if new_index < 0 or new_index > max_index:
            errmsg = (
                f"position {new_index} is outside of range 0 - {max_index}."
            )
            if mode == "relative" and value > 0 and value < max_index:
                errmsg += " Did you mean to request mode='absolute'?"
            raise IndexError(errmsg)
        self.rowindex = value

    def mogrify(self, *args, **kwargs):
        raise NotImplementedError()

    def callproc(self, *args, **kwargs):
        raise NotImplementedError()

    def copy_to(self, *args, **kwargs):
        raise NotImplementedError()

    def copy_expert(self, *args, **kwargs):
        raise NotImplementedError()

    def copy_from(self, *args, **kwargs):
        raise NotImplementedError()

    def close(self):
        self.closed = True
        self.results = []

    @property
    def tzinfo_factory(self):
        raise NotImplementedError()

    @property
    def description(self):
        raise NotImplementedError()

    @property
    def statusmessage(self):
        raise NotImplementedError()

    @property
    def rownumber(self):
        if self.results:
            return self.rowindex
        return None

    @property
    def rowcount(self):
        if self.results:
            return len(self.results)
        return -1

    @property
    def name(self):
        return None

    def _prepare_dbapi_query(self, query, *args, **kwargs):
        """Translate query and params from DB-API2 format to PostgreSQL format.

        Translate placeholders from Python DB-API2 format to PostgreSQL native
        format, and ensure that params (which may legitimately be a dict for
        DB-API2) is converted to a list.
        """
        if "%" not in query:
            return query, ()

        if pg8000_dbapi is None:
            raise RuntimeError(
                "Can't translate query from DB-API to PostgreSQL "
                "format because pg8000 could not be imported"
            )

        if args and kwargs:
            raise RuntimeError(
                "query methods can't accept both *args and **kwargs arguments"
            )

        if args:
            query_args = args
        else:
            query_args = kwargs

        style = "pyformat"
        return pg8000_dbapi.convert_paramstyle(style, query, query_args)

    @defer.inlineCallbacks
    def executemany(self, query, vars_list, row_cache_size=None,
                    row_unpacker=None, **kwargs):
        """Execute a statement against all parameters in vars_list.

        The function is mostly useful for commands that update the database.
        Any result set returned by the query is discarded.
        """
        statement, args = self._prepare_dbapi_query(query, vars_list[0])
        prepared_statement = yield self._prepare(statement)

        for params in vars_list:
            # Translate placeholders from Python DB-API2 format to PostgreSQL
            # native format, and ensure that params (which may legitimately be
            # a dict for DB-API2) is converted to a list.
            _ = yield self._execute(prepared_statement, params=args,
                                    row_cache_size=row_cache_size,
                                    row_unpacker=row_unpacker)

        return None

    def execute(self, query, params=None, row_cache_size=None,
                row_unpacker=None, **kwargs):
        """
        This method parses the query and binds the parameters, creating a
        Portal. It then executes that Portal.
        """
        self.results = []
        if row_cache_size is None:
            row_cache_size = self.arraysize

        # Translate placeholders from Python DB-API2 format to PostgreSQL
        # native format, and ensure that params (which may legitimately be
        # a dict for DB-API2) is converted to a list.
        statement, args = self._prepare_dbapi_query(query, params)
        df = self._prepare(statement)
        df.addCallback(self._execute, params=args,
                       row_cache_size=row_cache_size,
                       row_unpacker=row_unpacker)
        return df

    def per_row(self, row):
        self.results.append(row)
        return row

    def _prepare(self, query):
        """
        This method parses the query and calls "describe" on the server.
        """
        pypy.lprint('%s Cursor._prepare: %s', (self.connection.id, query),
                    color='white')

        self.query = query
        self.portal = None
        self.bind_sent = False

        try:
            prepared_statement = self.connection.prepare_statement(query)
        except Exception as x:
            try:
                pypy.lprint("EX: %s", (traceback.format_exc(),), color='red')
            except Exception:
                pypy.lprint("prepare_statement failed and I can't tell you why")
            raise x

        return self.connection.parse_statement(prepared_statement)

    def _execute(self, prepared_statement, params=None, row_cache_size=None, row_unpacker=None):
        self._reset()
        if row_cache_size is None:
              row_cache_size = self.arraysize
        portal = self.connection.make_portal(
            prepared_statement,
            params,
            row_cache_size=row_cache_size,
            row_unpacker=row_unpacker,
            per_row_callable=self.results.append
        )
        self.add_portal(portal)
        self.connection.bind_cursor(self)
        self.bind_sent = True
        df = self.connection.execute_cursor(self, getall=True)
        return df

    def add_portal(self, portal):
        pypy.lprint('%s Cursor.add_portal: %s', (self.connection.id, portal),
                    color='red')
        self.portal = portal
        return self

    def fetchall(self):
        pypy.lprint('%s Cursor.fetchall', (self.connection.id,), color='blue')
        if not self.bind_sent:
            msg = "Cannot fetch until the execute operation has completed."
            raise RuntimeError(msg)

        if self.rowindex == 0:
            rval = self.results
        else:
            rval = self.results[self.rowindex:]

        self.rowindex = len(self.results)
        return rval

    def fetchone(self):
        pypy.lprint('%s Cursor.fetchone', (self.connection.id,), color='blue')
        if not self.bind_sent:
            msg = "Cannot fetch until the execute operation has completed."
            raise RuntimeError(msg)

        try:
            rval = self.results[self.rowindex]
            self.rowindex += 1
            return rval
        except IndexError:
            return None

    def fetchmany(self, count=None):
        pypy.lprint('%s Cursor.fetchmany %s', (self.connection.id, count,),
                    color='blue')
        if not self.bind_sent:
            msg = "Cannot fetch until the execute operation has completed."
            raise RuntimeError(msg)

        # psycopg2 docs say count should default to cursor.arraysize
        if count is None:
            count = self.arraysize

        new_index = self.rowindex + count
        rval = self.results[self.rowindex:new_index]

        # The max index is actually one greater than the true maximum index
        # of self.results, e.g. if self.results == [0,1,2,3] then max_index
        # is 4
        max_index = len(self.results)
        self.rowindex = min(max_index, new_index)
        return rval

    def eb(self, f):
        print(f.getBriefTraceback())
