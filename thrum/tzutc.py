#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# Copyright (c) Dónal McMullan
# See LICENSE for details.

import datetime


class _UTC(datetime.tzinfo):
    zero_delta = datetime.timedelta(0)

    def utcoffset(self, dt):
        return _UTC.zero_delta

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return _UTC.zero_delta


class Factory(object):
    instance = None

    @classmethod
    def utc(cls):
        if cls.instance is None:
            cls.instance = _UTC()
        return cls.instance


def UTC():
    return Factory.utc()
