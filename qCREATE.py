#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import time
import struct
import traceback

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task

from thrum.twisted import connection
from thrum import binary
from thrum import pypy


try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2 
LIMIT 40'''
ARGS = (-1, True)

QUERY="CREATE TABLE _bytes(b bytea)"
QUERY2 = "COMMIT"
ARGS=None
credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)



def connect_and_parse(_, conn):
    #print(" -- connect_and_parse: conn: %s" % (conn,))
    df = conn.runQuery(QUERY, ARGS, row_cache_size=100)
    df.addCallback(show, conn)
    df.addErrback(eb)
    return df

def commit(_, conn):
    df = conn.runQuery(QUERY2, ARGS)
    df.addCallback(show, df)
    df.addErrback(eb)
    return df

def show(r, df):
    print(" -- Done: ")#%s " % (df.callbacks,))
    try:
        for x in r:
            print("   %s" % (x,))
    except Exception as e:
        print(e)

def done(r):
    return reactor.callLater(0.100001, reactor.stop)
    reactor.stop()

def ok_go():
    conn = connection.Connection(reactor)
    df = conn.connect(**credentials)
    df.addCallback(connect_and_parse, conn)
    #df.addCallback(connect_and_parse, conn)
    df.addCallback(done)
    df.addErrback(eb)
    return df

def eb(f):
    print(" -- Traceback: %s" % (f.getBriefTraceback(),))
    reactor.stop()

def txmain():
    df = ok_go()
    df.addErrback(eb)
    return df

reactor.callWhenRunning(txmain)
reactor.run()
