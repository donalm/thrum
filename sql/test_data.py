#!/usr/bin/env python

import ipaddress
import datetime
import decimal
import uuid

from thrum import interval
from thrum import tzutc
utc = tzutc.UTC()

data = {
    "bool_table": {
        "bool_field": [True, False, None]
    },
    "bytea_table": {
        "bytea_field": [b'\x00\x01',
                        b'\xff\xff\xff\xff']
    },
    "char_table": {
        "char_field": [b'A', b'-', b'o']
    },
    "name_table": {
        "name_field": [u'A 64 bytes text field, not often used by the public', "Thé"]
    },
    "int8_table": {
        "int8_field": [9223372036854775807, -9223372036854775808, 0, -1, 1]
    },
    "int2_table": {
        "int2_field": [-32768, 32767, 0, -1, 1]
    },
    "int4_table": {
        "int4_field": [-2147483648, 2147483647, 0, -1, 1]
    },
    "text_table": {
        "text_field": [u"This contains text",
                       u"北 ICBM撃たず米と対立回避か",
                       u'''Hamilton : "Mes excuses auprès de l'équipe"''',
                       u"정치] `야당후보 비방광고' 지만원씨 벌금 100만원"]
    },
    "oid_table": {
        "oid_field": [0, 1, 4294967295]
    },
    "tid_table": {
        "tid_field": [0, 1, 4294967295]
    },
    "xid_table": {
        "xid_field": [0, 1, 4294967295]
    },
    "cid_table": {
        "cid_field": [0, 1, 4294967295]
    },
    "xml_table": {
        "xml_field": ['<rss version="2.0"><channel><title>RSS Title</title></channel></rss>',
                      '''<?xml version="1.0"?>
<catalog>
   <book id="bk101">
      <author>Gambardella, Matthew</author>
      <title>XML Developer's Guide</title>
      <genre>Computer</genre>
      <price>44.95</price>
      <publish_date>2000-10-01</publish_date>
      <description>An in-depth look at creating applications
      with XML.</description>
   </book>
   <book id="bk102">
      <author>Ralls, Kim</author>
      <title>Midnight Rain</title>
      <genre>Fantasy</genre>
      <price>5.95</price>
      <publish_date>2000-12-16</publish_date>
      <description>A former architect battles corporate zombies,
      an evil sorceress, and her own childhood to become queen
      of the world.</description>
   </book>
   <book id="bk103">
      <author>Corets, Eva</author>
      <title>Maeve Ascendant</title>
      <genre>Fantasy</genre>
      <price>5.95</price>
      <publish_date>2000-11-17</publish_date>
      <description>After the collapse of a nanotechnology
      society in England, the young survivors lay the
      foundation for a new society.</description>
   </book>
   <book id="bk104">
      <author>Corets, Eva</author>
      <title>Oberon's Legacy</title>
      <genre>Fantasy</genre>
      <price>5.95</price>
      <publish_date>2001-03-10</publish_date>
      <description>In post-apocalypse England, the mysterious
      agent known only as Oberon helps to create a new life
      for the inhabitants of London. Sequel to Maeve
      Ascendant.</description>
   </book>
   <book id="bk105">
      <author>Corets, Eva</author>
      <title>The Sundered Grail</title>
      <genre>Fantasy</genre>
      <price>5.95</price>
      <publish_date>2001-09-10</publish_date>
      <description>The two daughters of Maeve, half-sisters,
      battle one another for control of England. Sequel to
      Oberon's Legacy.</description>
   </book>
   <book id="bk106">
      <author>Randall, Cynthia</author>
      <title>Lover Birds</title>
      <genre>Romance</genre>
      <price>4.95</price>
      <publish_date>2000-09-02</publish_date>
      <description>When Carla meets Paul at an ornithology
      conference, tempers fly as feathers get ruffled.</description>
   </book>
   <book id="bk107">
      <author>Thurman, Paula</author>
      <title>Splish Splash</title>
      <genre>Romance</genre>
      <price>4.95</price>
      <publish_date>2000-11-02</publish_date>
      <description>A deep sea diver finds true love twenty
      thousand leagues beneath the sea.</description>
   </book>
   <book id="bk108">
      <author>Knorr, Stefan</author>
      <title>Creepy Crawlies</title>
      <genre>Horror</genre>
      <price>4.95</price>
      <publish_date>2000-12-06</publish_date>
      <description>An anthology of horror stories about roaches,
      centipedes, scorpions  and other insects.</description>
   </book>
   <book id="bk109">
      <author>Kress, Peter</author>
      <title>Paradox Lost</title>
      <genre>Science Fiction</genre>
      <price>6.95</price>
      <publish_date>2000-11-02</publish_date>
      <description>After an inadvertant trip through a Heisenberg
      Uncertainty Device, James Salway discovers the problems
      of being quantum.</description>
   </book>
   <book id="bk110">
      <author>O'Brien, Tim</author>
      <title>Microsoft .NET: The Programming Bible</title>
      <genre>Computer</genre>
      <price>36.95</price>
      <publish_date>2000-12-09</publish_date>
      <description>Microsoft's .NET initiative is explored in
      detail in this deep programmer's reference.</description>
   </book>
   <book id="bk111">
      <author>O'Brien, Tim</author>
      <title>MSXML3: A Comprehensive Guide</title>
      <genre>Computer</genre>
      <price>36.95</price>
      <publish_date>2000-12-01</publish_date>
      <description>The Microsoft MSXML3 parser is covered in
      detail, with attention to XML DOM interfaces, XSLT processing,
      SAX and more.</description>
   </book>
   <book id="bk112">
      <author>Galos, Mike</author>
      <title>Visual Studio 7: A Comprehensive Guide</title>
      <genre>Computer</genre>
      <price>49.95</price>
      <publish_date>2001-04-16</publish_date>
      <description>Microsoft Visual Studio 7 is explored in depth,
      looking at how Visual Basic, Visual C++, C#, and ASP+ are
      integrated into a comprehensive development
      environment.</description>
   </book>
</catalog>''',
                     '''<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend!</body>
</note>''',
                     '''
<?xml version="1.0" encoding="UTF-8"?>
<CATALOG>
  <CD>
    <TITLE>Empire Burlesque</TITLE>
    <ARTIST>Bob Dylan</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Hide your heart</TITLE>
    <ARTIST>Bonnie Tyler</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS Records</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
  <CD>
    <TITLE>Greatest Hits</TITLE>
    <ARTIST>Dolly Parton</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>RCA</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1982</YEAR>
  </CD>
  <CD>
    <TITLE>Still got the blues</TITLE>
    <ARTIST>Gary Moore</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Virgin records</COMPANY>
    <PRICE>10.20</PRICE>
    <YEAR>1990</YEAR>
  </CD>
  <CD>
    <TITLE>Eros</TITLE>
    <ARTIST>Eros Ramazzotti</ARTIST>
    <COUNTRY>EU</COUNTRY>
    <COMPANY>BMG</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1997</YEAR>
  </CD>
  <CD>
    <TITLE>One night only</TITLE>
    <ARTIST>Bee Gees</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Polydor</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1998</YEAR>
  </CD>
  <CD>
    <TITLE>Sylvias Mother</TITLE>
    <ARTIST>Dr.Hook</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>CBS</COMPANY>
    <PRICE>8.10</PRICE>
    <YEAR>1973</YEAR>
  </CD>
  <CD>
    <TITLE>Maggie May</TITLE>
    <ARTIST>Rod Stewart</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Pickwick</COMPANY>
    <PRICE>8.50</PRICE>
    <YEAR>1990</YEAR>
  </CD>
  <CD>
    <TITLE>Romanza</TITLE>
    <ARTIST>Andrea Bocelli</ARTIST>
    <COUNTRY>EU</COUNTRY>
    <COMPANY>Polydor</COMPANY>
    <PRICE>10.80</PRICE>
    <YEAR>1996</YEAR>
  </CD>
  <CD>
    <TITLE>When a man loves a woman</TITLE>
    <ARTIST>Percy Sledge</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Atlantic</COMPANY>
    <PRICE>8.70</PRICE>
    <YEAR>1987</YEAR>
  </CD>
  <CD>
    <TITLE>Black angel</TITLE>
    <ARTIST>Savage Rose</ARTIST>
    <COUNTRY>EU</COUNTRY>
    <COMPANY>Mega</COMPANY>
    <PRICE>10.90</PRICE>
    <YEAR>1995</YEAR>
  </CD>
  <CD>
    <TITLE>1999 Grammy Nominees</TITLE>
    <ARTIST>Many</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Grammy</COMPANY>
    <PRICE>10.20</PRICE>
    <YEAR>1999</YEAR>
  </CD>
  <CD>
    <TITLE>For the good times</TITLE>
    <ARTIST>Kenny Rogers</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Mucik Master</COMPANY>
    <PRICE>8.70</PRICE>
    <YEAR>1995</YEAR>
  </CD>
  <CD>
    <TITLE>Big Willie style</TITLE>
    <ARTIST>Will Smith</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Columbia</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1997</YEAR>
  </CD>
  <CD>
    <TITLE>Tupelo Honey</TITLE>
    <ARTIST>Van Morrison</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Polydor</COMPANY>
    <PRICE>8.20</PRICE>
    <YEAR>1971</YEAR>
  </CD>
  <CD>
    <TITLE>Soulsville</TITLE>
    <ARTIST>Jorn Hoel</ARTIST>
    <COUNTRY>Norway</COUNTRY>
    <COMPANY>WEA</COMPANY>
    <PRICE>7.90</PRICE>
    <YEAR>1996</YEAR>
  </CD>
  <CD>
    <TITLE>The very best of</TITLE>
    <ARTIST>Cat Stevens</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Island</COMPANY>
    <PRICE>8.90</PRICE>
    <YEAR>1990</YEAR>
  </CD>
  <CD>
    <TITLE>Stop</TITLE>
    <ARTIST>Sam Brown</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>A and M</COMPANY>
    <PRICE>8.90</PRICE>
    <YEAR>1988</YEAR>
  </CD>
  <CD>
    <TITLE>Bridge of Spies</TITLE>
    <ARTIST>T'Pau</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Siren</COMPANY>
    <PRICE>7.90</PRICE>
    <YEAR>1987</YEAR>
  </CD>
  <CD>
    <TITLE>Private Dancer</TITLE>
    <ARTIST>Tina Turner</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>Capitol</COMPANY>
    <PRICE>8.90</PRICE>
    <YEAR>1983</YEAR>
  </CD>
  <CD>
    <TITLE>Midt om natten</TITLE>
    <ARTIST>Kim Larsen</ARTIST>
    <COUNTRY>EU</COUNTRY>
    <COMPANY>Medley</COMPANY>
    <PRICE>7.80</PRICE>
    <YEAR>1983</YEAR>
  </CD>
  <CD>
    <TITLE>Pavarotti Gala Concert</TITLE>
    <ARTIST>Luciano Pavarotti</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>DECCA</COMPANY>
    <PRICE>9.90</PRICE>
    <YEAR>1991</YEAR>
  </CD>
  <CD>
    <TITLE>The dock of the bay</TITLE>
    <ARTIST>Otis Redding</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>Stax Records</COMPANY>
    <PRICE>7.90</PRICE>
    <YEAR>1968</YEAR>
  </CD>
  <CD>
    <TITLE>Picture book</TITLE>
    <ARTIST>Simply Red</ARTIST>
    <COUNTRY>EU</COUNTRY>
    <COMPANY>Elektra</COMPANY>
    <PRICE>7.20</PRICE>
    <YEAR>1985</YEAR>
  </CD>
  <CD>
    <TITLE>Red</TITLE>
    <ARTIST>The Communards</ARTIST>
    <COUNTRY>UK</COUNTRY>
    <COMPANY>London</COMPANY>
    <PRICE>7.80</PRICE>
    <YEAR>1987</YEAR>
  </CD>
  <CD>
    <TITLE>Unchain my heart</TITLE>
    <ARTIST>Joe Cocker</ARTIST>
    <COUNTRY>USA</COUNTRY>
    <COMPANY>EMI</COMPANY>
    <PRICE>8.20</PRICE>
    <YEAR>1987</YEAR>
  </CD>
</CATALOG>''']
    },
    "point_table": {
        "point_field": [(1, 2), (1.22, 3.349), (1e-6, -1e9)]
    },
    "lseg_table": {
        "lseg_field": [((1.0,2.0),(3.0,4.330)),
                       ((33.333333, 99), (4096, 8192))]
    },
    "path_table": {
        "path_field": [((3.333333333,2.0),(3.0,903899),(5.4,99),(1.0,-1e-6)),
                       ((1.0,2.0),(3.0,4.0),(3.4,99),(4.0,2.0))]
    },
    "box_table": {
        "box_field": [((1.0,2.0),(3.0,4.330)),
                      ((33.333333, 99), (4096, 8192))]
    },
    "polygon_table": {
        "polygon_field": [((1.0,2.0),(3.0,4),(5.4,99),(1.0,2.0)),
                          ((61.0,62.0),(63.0,64),(65.4,699),(61.0,62.0))]
    },
    "line_table": {
        "line_field": [(1.0320000000001033, -1.0, 0.9679999999998967),
                       (1.0320000000001,-1,0.967999999999897)]
    },
    "cidr_table": {
        "cidr_field": [ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128"),
                       ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f2/128"),
                       ipaddress.IPv4Network(u"192.168.100.128/25"),
                       ipaddress.IPv4Network(u"255.255.255.255/32"),
                       ipaddress.IPv4Network(u"127.0.0.0/8"),
                       ipaddress.IPv4Network(u"127.1.0.0/16"),
                       ipaddress.IPv4Network(u"127.1.0.0/18"),
                       ipaddress.IPv4Network(u"10.0.0.0/32"),
                       ipaddress.IPv4Network(u"0.0.0.0/0"),
                       ipaddress.IPv6Network(u"ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff/128"),
                       ipaddress.IPv6Network(u"::1/128"),
                       ipaddress.IPv6Network(u"::/0")]
    },
    "float4_table": {
        "float4_field": [0.0, 0.1, 1.0, 100, 1e6, 333.333333333, 66.0, -0.1, -1.1, -7, -1e-6, -1e9]
    },
    "float8_table": {
        "float8_field": [0.0, 0.1, 1.0, 100, 1e6, 333.333333333, 66.0, -0.1, -1.1, -7, -1e-6, -1e9]
    },
    "circle_table": {
        "circle_field": [((1.1,2.1),2),
                         ((2.1,4.1),3),]
    },
    "money_table": {
        "money_field": [1.23, 66.66, 20.21, 1090990009.99, 33333333.33]
    },
    "macaddr_table": {
        "macaddr_field": ["00:00:00:00:00:00",
                          "11-aa-22-bb-33-cc",
                          "00:11:22:33:44:55",
                          "aabbccddeeff",
                          "ff:ff:ff:ff:ff:ff"]
    },
    "inet_table": {
        "inet_field": [ipaddress.IPv4Address("0.0.0.0"),
                       ipaddress.IPv4Network(u"192.168.100.128/25"),
                       ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128")
                       ipaddress.IPv6Address("::1"),
                       ipaddress.IPv6Address("::"),
                       ipaddress.IPv4Address("255.255.255.255")]
    },
    "bpchar_table": {
        "bpchar_field": [u"née", u"two", u"three", u"لوحة المفاتيح العربية", u"كيبورد للكتابة بالعربي"]
    },
    "varchar_table": {
        "varchar_field": [u"née", u"two", u"three", u"لوحة المفاتيح العربية", u"كيبورد للكتابة بالعربي"]
    },
    "date_table": {
        "date_field": [datetime.date(1, 1, 1),
                       datetime.date(2000, 1, 1),
                       datetime.date(2400, 12, 31),
                       datetime.date(datetime.MAXYEAR, 12, 31)]
    },
    "time_table": {
        "time_field": [datetime.time(0,0)
                       datetime.time(16, 20),
                       datetime.time(23, 59, 59, 999999)]
    },
    "timestamp_table": {
        "timestamp_field": [datetime.datetime.min,
                            datetime.datetime(2017, 4, 16, 22, 3, 4, 814075),
                            datetime.datetime(2400, 12, 31, 23, 59, 59, 999999),
                            datetime.datetime.max]
    },
    "timestamptz_table": {
        "timestamptz_field": [datetime.datetime(2017, 4, 16, 22, 3, 4, 814075, utc),
                              datetime.datetime(2400, 12, 31, 23, 59, 59, 999999, utc)]
    },
    "interval_table": {
        "interval_field": [interval.Interval(0, 0, 0)
                           interval.Interval(0, 0, 1),
                           interval.Interval(0, 1, 0),
                           interval.Interval(0, 1, 1),
                           interval.Interval(1, 0, 0),
                           interval.Interval(1, 0, 1),
                           interval.Interval(1, 1, 0),
                           interval.Interval(1, 1, 1)]
    },
    "timetz_table": {
        "timetz_field": [datetime.time(23, 59, 59, 999999, utc),
                         datetime.time(16, 20, 0, 0, utc),
                         datetime.time(0, 0, 0, 0, utc)]
    },
    "bit_table": {
        "bit_field": [b"101010101010101010",
                      b"101"
                      b"110010001",
                      b"10010001",
                      b"11110001"]
    },
    "varbit_table": {
        "varbit_field": [b"101010101010101010",
                         b"101"
                         b"110010001",
                         b"10010001",
                         b"11110001"]
    },
    "numeric_table": {
        "numeric_field": [-(2 ** 64),
                          2 ** 64,
                          -(2 ** 128),
                          2 ** 128,
                          -1, 0, 1,
                          decimal.Decimal("0.00000000000000"),
                          decimal.Decimal("1.00000000000000"),
                          decimal.Decimal("-1.00000000000000"),
                          decimal.Decimal("-2.00000000000000"),
                          decimal.Decimal("1000000000000000.00000000000000"),
                          decimal.Decimal("-0.00000000000000"),
                          decimal.Decimal(1234),
                          decimal.Decimal(-1234),
                          decimal.Decimal("1234000000.00088883231"),
                          decimal.Decimal(str(1234.00088883231)),
                          decimal.Decimal("3123.23111"),
                          decimal.Decimal("-3123000000.23111"),
                          decimal.Decimal("3123.2311100000"),
                          decimal.Decimal("-03123.0023111"),
                          decimal.Decimal("3123.23111"),
                          decimal.Decimal("3123.23111"),
                          decimal.Decimal("10000.23111"),
                          decimal.Decimal("100000.23111"),
                          decimal.Decimal("1000000.23111"),
                          decimal.Decimal("10000000.23111"),
                          decimal.Decimal("100000000.23111"),
                          decimal.Decimal("1000000000.23111"),
                          decimal.Decimal("1000000000.3111"),
                          decimal.Decimal("1000000000.111"),
                          decimal.Decimal("1000000000.11"),
                          decimal.Decimal("100000000.0"),
                          decimal.Decimal("10000000.0"),
                          decimal.Decimal("1000000.0"),
                          decimal.Decimal("100000.0"),
                          decimal.Decimal("10000.0"),
                          decimal.Decimal("1000.0"),
                          decimal.Decimal("100.0"),
                          decimal.Decimal("100"),
                          decimal.Decimal("100.1"),
                          decimal.Decimal("100.12"),
                          decimal.Decimal("100.123"),
                          decimal.Decimal("100.1234"),
                          decimal.Decimal("100.12345"),
                          decimal.Decimal("100.123456"),
                          decimal.Decimal("100.1234567"),
                          decimal.Decimal("100.12345679"),
                          decimal.Decimal("100.123456790"),
                          decimal.Decimal("100.123456790000000000000000"),
                          decimal.Decimal("1.0"),
                          decimal.Decimal("0.0"),
                          decimal.Decimal("-1.0"),
                          decimal.Decimal("1.0E-1000"),
                          decimal.Decimal("1.0E1000"),
                          decimal.Decimal("0.000000000000000000000000001"),
                          decimal.Decimal("0.000000000000010000000000001"),
                          decimal.Decimal("0.00000000000000000000000001"),
                          decimal.Decimal("0.00000000100000000000000001"),
                          decimal.Decimal("0.0000000000000000000000001"),
                          decimal.Decimal("0.000000000000000000000001"),
                          decimal.Decimal("0.00000000000000000000001"),
                          decimal.Decimal("0.0000000000000000000001"),
                          decimal.Decimal("0.000000000000000000001"),
                          decimal.Decimal("0.00000000000000000001"),
                          decimal.Decimal("0.0000000000000000001"),
                          decimal.Decimal("0.000000000000000001"),
                          decimal.Decimal("0.00000000000000001"),
                          decimal.Decimal("0.0000000000000001"),
                          decimal.Decimal("0.000000000000001"),
                          decimal.Decimal("0.00000000000001"),
                          decimal.Decimal("0.0000000000001"),
                          decimal.Decimal("0.000000000001"),
                          decimal.Decimal("0.00000000001"),
                          decimal.Decimal("0.0000000001"),
                          decimal.Decimal("0.000000001"),
                          decimal.Decimal("0.00000001"),
                          decimal.Decimal("0.0000001"),
                          decimal.Decimal("0.000001"),
                          decimal.Decimal("0.00001"),
                          decimal.Decimal("0.0001"),
                          decimal.Decimal("0.001"),
                          decimal.Decimal("0.01"),
                          decimal.Decimal("0.1")]
    },
    "refcursor_table": {
        "refcursor_field": [u"Just a string value"]
    },
    "uuid_table": {
        "uuid_field": [uuid.UUID('00000000-0000-0000-0000-000000000000'),
                       uuid.UUID('12345678-abcd-1234-cdef-0123456789af'),
                       uuid.UUID('ffffffff-ffff-ffff-ffff-ffffffffffff')]
    },
    "json_table": {
        "json_field": [{"a":"A", "b":"B", "c":[1,2,3,4]},
                       u"Just a string",
                       [u"Just", u"a", u"list"]]
    },
    "jsonb_table": {
        "jsonb_field": [{"a":"A", "b":"B", "c":[1,2,3,4]},
                        u"Just a string",
                        [u"Just", u"a", u"list"]]
    },
    "array_xml_table": {
        "array_xml_field": [['<rss version="2.0"><channel><title>RSS Title</title></channel></rss>',
                             '<rss version="2.0"><channel><title>RSS Title</title></channel></rss>']]
    },
    "array_json_table": {
        "array_json_field": [[{"a":"A", "b":"B", "c":[1,2,3,4]},
                              u"Just a string",
                              [u"Just", u"a", u"list"]]]
    },
    "array_line_table": {
        "array_line_field": [[(1.0320000000001033, -1.0, 0.9679999999998967),
                              (1.0320000000001,-1,0.967999999999897)]]
    },
    "array_cidr_table": {
        "array_cidr_field": [[ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128"),
                              ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f2/128"),
                              ipaddress.IPv4Network(u"192.168.100.128/25"),
                              ipaddress.IPv4Network(u"255.255.255.255/32"),
                              ipaddress.IPv4Network(u"127.0.0.0/8"),
                              ipaddress.IPv4Network(u"127.1.0.0/16"),
                              ipaddress.IPv4Network(u"127.1.0.0/18"),
                              ipaddress.IPv4Network(u"10.0.0.0/32"),
                              ipaddress.IPv4Network(u"0.0.0.0/0"),
                              ipaddress.IPv6Network(u"ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff/128"),
                              ipaddress.IPv6Network(u"::1/128"),
                              ipaddress.IPv6Network(u"::/0")]]
    },
    "array_circle_table": {
        "array_circle_field": [((1.1,2.1),2),((2.1,4.1),3)]
    },
    "array_money_table": {
        "array_money_field": [[1.22, 3.50, 4.99], [3.33, 4.56, 55.55],
                              [[1.22, 4.50, 5.99, 20.00], [22.80, 0.00, 20477.44, 3.33]], # nested array
                             ]
    },
    "array_bool_table": {
        "array_bool_field": [[False, True, True],
                             [True, True, True],
                             [True, False, False],
                             [None, None, True]]
    },
    "array_bytea_table": {
        "array_bytea_field": [[b'some bytes', b'some more bytes']]
    },
    "array_char_table": {
        "array_char_field": [[b'a', b'r', b'T'],
                             [None, None, None]]
    },
    "array_name_table": {
        "array_name_field": [[u'just a string', u'another string']]
    },
    "array_int2_table": {
        "array_int2_field": [[32768, 0 32767], [99, None, 100], [-32768, 32767, 0, -1, 1]]
    },
    "array_int4_table": {
        "array_int4_field": [[12312432, 23423423, 2342552],
                             [222, 444, 555, 666, None],
                             [-2147483648, 2147483647, 0, -1, 1]]
    },
    "array_text_table": {
        "array_text_field": [[u'some text', u'some more text']]
    },
    "array_tid_table": {
        "array_tid_field": [[0, 1, 4294967295]]
    },
    "array_xid_table": {
        "array_xid_field": [[0, 1, 4294967295]]
    },
    "array_cid_table": {
        "array_cid_field": [[0, 1, 4294967295]]
    },
    "array_bpchar_table": {
        "array_bpchar_field": [[u'some text', u'some more text']]
    },
    "array_varchar_table": {
        "array_varchar_field": [[u'some text', u'some more text']]
    },
    "array_int8_table": {
        "array_int8_field": [[9223372036854775807, -9223372036854775808, 0, -1, 1]]
    },
    "array_point_table": {
        "array_point_field": [[(1, 2), (1.22, 3.349), (1e-6, -1e9)]]
    },
    "array_lseg_table": {
        "array_lseg_field": [[((1.0,2.0),(3.0,4.330)),((33.333333, 99), (4096, 8192))]]
    },
    "array_path_table": {
        "array_path_field": [[((3.333333333,2.0),(3.0,903899),(5.4,99),(1.0,-1e-6)),((1.0,2.0),(3.0,4.0),(3.4,99),(4.0,2.0))]]
    },
    "array_box_table": {
        "array_box_field": [[((1.0,2.0),(3.0,4.330)),((33.333333, 99), (4096, 8192))]]
    },
    "array_float4_table": {
        "array_float4_field": [[0.0, 0.1, 1.0, 100, 1e6, 333.333333333, 66.0, -0.1, -1.1, -7, -1e-6, -1e9]]
    },
    "array_float8_table": {
        "array_float8_field": [[0.0, 0.1, 1.0, 100, 1e6, 333.333333333, 66.0, -0.1, -1.1, -7, -1e-6, -1e9]]
    },
    "array_polygon_table": {
        "array_polygon_field": [[((1.0,2.0),(3.0,4),(5.4,99),(1.0,2.0)),((61.0,62.0),(63.0,64),(65.4,699),(61.0,62.0))]]
    },
    "array_oid_table": {
        "array_oid_field": [[0, 1, 4294967295]]
    },
    "array_macaddr_table": {
        "array_macaddr_field": [["00:00:00:00:00:00",
                                 "11-aa-22-bb-33-cc",
                                 "00:11:22:33:44:55",
                                 "aabbccddeeff",
                                 "ff:ff:ff:ff:ff:ff"]]
    },
    "array_inet_table": {
        "array_inet_field": [[ipaddress.IPv4Address("0.0.0.0"),
                              ipaddress.IPv4Network(u"192.168.100.128/25"),
                              ipaddress.IPv6Network(u"2001:4f8:3:ba:2e0:81ff:fe22:d1f1/128")
                              ipaddress.IPv6Address("::1"),
                              ipaddress.IPv6Address("::"),
                              ipaddress.IPv4Address("255.255.255.255")]]
    },
    "array_timestamp_table": {
        "array_timestamp_field": [[datetime.datetime.min,
                                   datetime.datetime(2017, 4, 16, 22, 3, 4, 814075),
                                   datetime.datetime(2400, 12, 31, 23, 59, 59, 999999),
                                   datetime.datetime.max]]
    },
    "array_date_table": {
        "array_date_field": [[datetime.date(1, 1, 1),
                              datetime.date(2000, 1, 1),
                              datetime.date(2400, 12, 31),
                              datetime.date(datetime.MAXYEAR, 12, 31)]]
    },
    "array_time_table": {
        "array_time_field": [[datetime.time(0,0)
                              datetime.time(16, 20),
                              datetime.time(23, 59, 59, 999999)]]
    },
    "array_timestamptz_table": {
        "array_timestamptz_field": [[datetime.datetime(2017, 4, 16, 22, 3, 4, 814075, utc),
                                     datetime.datetime(2400, 12, 31, 23, 59, 59, 999999, utc)]]
    },
    "array_interval_table": {
        "array_interval_field": [[interval.Interval(0, 0, 0)
                                  interval.Interval(0, 0, 1),
                                  interval.Interval(0, 1, 0),
                                  interval.Interval(0, 1, 1),
                                  interval.Interval(1, 0, 0),
                                  interval.Interval(1, 0, 1),
                                  interval.Interval(1, 1, 0),
                                  interval.Interval(1, 1, 1)]]
    },
    "array_numeric_table": {
        "array_numeric_field": [[-(2 ** 64),
                                 2 ** 64,
                                 -(2 ** 128),
                                 2 ** 128,
                                 -1, 0, 1],
                                [decimal.Decimal("0.00000000000000"),
                                 decimal.Decimal("1.00000000000000"),
                                 decimal.Decimal("-1.00000000000000"),
                                 decimal.Decimal("-2.00000000000000"),
                                 decimal.Decimal("1000000000000000.00000000000000"),
                                 decimal.Decimal("-0.00000000000000"),
                                 decimal.Decimal(1234),
                                 decimal.Decimal(-1234),
                                 decimal.Decimal("1234000000.00088883231"),
                                 decimal.Decimal(str(1234.00088883231))],
                                [decimal.Decimal("3123.23111"),
                                 decimal.Decimal("-3123000000.23111"),
                                 decimal.Decimal("3123.2311100000"),
                                 decimal.Decimal("-03123.0023111"),
                                 decimal.Decimal("3123.23111"),
                                 decimal.Decimal("3123.23111"),
                                 decimal.Decimal("10000.23111"),
                                 decimal.Decimal("100000.23111"),
                                 decimal.Decimal("1000000.23111"),
                                 decimal.Decimal("10000000.23111"),
                                 decimal.Decimal("100000000.23111"),
                                 decimal.Decimal("1000000000.23111")],
                                [decimal.Decimal("1000000000.3111"),
                                 decimal.Decimal("1000000000.111"),
                                 decimal.Decimal("1000000000.11"),
                                 decimal.Decimal("100000000.0"),
                                 decimal.Decimal("10000000.0"),
                                 decimal.Decimal("1000000.0"),
                                 decimal.Decimal("100000.0"),
                                 decimal.Decimal("10000.0"),
                                 decimal.Decimal("1000.0"),
                                 decimal.Decimal("100.0")]]
    },
    "array_timetz_table": {
        "array_timetz_field": [[datetime.time(23, 59, 59, 999999, utc),
                                datetime.time(16, 20, 0, 0, utc),
                                datetime.time(0, 0, 0, 0, utc)]]
    },
    "array_bit_table": {
        "array_bit_field": [[b"101010101010101010",
                             b"101"
                             b"110010001",
                             b"10010001",
                             b"11110001"]]
    },
    "array_varbit_table": {
        "array_varbit_field": [[b"101010101010101010",
                                b"101"
                                b"110010001",
                                b"10010001",
                                b"11110001"]]
    },
    "array_refcursor_table": {
        "array_refcursor_field": [[u"just a string, used to identify a cursor.", u"Treat like 'name'"]]
    },
    "array_uuid_table": {
        "array_uuid_field": [[uuid.UUID('00000000-0000-0000-0000-000000000000'),
                              uuid.UUID('12345678-abcd-1234-cdef-0123456789af'),
                              uuid.UUID('ffffffff-ffff-ffff-ffff-ffffffffffff')]]
    },
    "tsvector_table": {
        "tsvector_field": [u'one:1 two:2 three:3 upping:4 four:5', u'one:1A two:2A three:3 upping:4 four:5B']
    },
    "array_jsonb_table": {
        "array_jsonb_field": [[{"a":"A", "b":"B", "c":[1,2,3,4]},
                              u"Just a string",
                              [u"Just", u"a", u"list"]]]
    },
    "int4range_table": {
        "int4range_field": [(None, None), (None, 100), (100, None), (10, 100)]
    },
    "array_int4range_table": {
        "array_int4range_field": [[(None, None), (None, 100), (100, None), (10, 100)]]
    },
    "numrange_table": {
        "numrange_field": [(None, None), (None, 4.44), (1.22, None), (0.442, 9499423.33)]
    },
    "array_numrange_table": {
        "array_numrange_field": [[(None, None), (None, 4.44), (1.22, None), (0.442, 9499423.33)]]
    },
    "tsrange_table": {
        "tsrange_field": [(datetime.datetime.min, datetime.datetime(2017, 4, 16, 22, 3, 4, 814075)),
                          (datetime.datetime(2400, 12, 31, 23, 59, 59, 999999), datetime.datetime.max)]
    },
    "array_tsrange_table": {
        "array_tsrange_field": [[(datetime.datetime.min, datetime.datetime(2017, 4, 16, 22, 3, 4, 814075)),
                                 (datetime.datetime(2400, 12, 31, 23, 59, 59, 999999), datetime.datetime.max)]]
    },
    "tstzrange_table": {
        "tstzrange_field": [(datetime.datetime(2017, 4, 16, 22, 3, 4, 814075, utc), datetime.datetime(2400, 12, 31, 23, 59, 59, 999999, utc))]
    },
    "array_tstzrange_table": {
        "array_tstzrange_field": [
                                  [
                                   (None, None),
                                   (None, datetime.datetime(2000, 12, 31, 23, 59, 59, 999999, utc)),
                                   (datetime.datetime(2000, 1, 1, 0, 0, 0, 0, utc), None),
                                   (datetime.datetime(2017, 4, 16, 22, 3, 4, 814075, utc), datetime.datetime(2400, 12, 31, 23, 59, 59, 999999, utc)),
                                   (datetime.datetime(2000, 1, 1, 0, 0, 0, 0, utc), datetime.datetime(2000, 12, 31, 23, 59, 59, 999999, utc)),
                                  ]
                                 ]
    },
    "daterange_table": {
        "daterange_field": [
                            (None, None),
                            (None, datetime.date(2400, 12, 31)),
                            (None, datetime.date(2400, 12, 31)),
                            (datetime.date(2000, 1, 1), datetime.date(2400, 12, 31))
                           ]
    },
    "array_daterange_table": {
        "array_daterange_field": [
                                  [
                                   (None, None),
                                   (None, datetime.date(2400, 12, 31)),
                                   (None, datetime.date(2400, 12, 31)),
                                   (datetime.date(2000, 1, 1), datetime.date(2400, 12, 31))
                                  ]
                                 ]
    },
    "int8range_table": {
        "int8range_field": [
                            (-9223372036854775808, 9223372036854775807),
                            (None, None),
                            (None, 0),
                            (-1, None),
                            (-1, 9223372036854775807)
                           ]
    },
    "array_int8range_table": {
        "array_int8range_field": [
                                  [
                                   (-9223372036854775808, 9223372036854775807),
                                   (None, None),
                                   (None, 0),
                                   (-1, None),
                                   (-1, 9223372036854775807)
                                  ]
                                 ]
    }
}
