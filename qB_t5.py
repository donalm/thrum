#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import time
import struct
import traceback

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task

from thrum.twisted import connection
from thrum.twisted import cpool
from thrum import binary
from thrum import pypy



try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2
LIMIT 40'''
ARGS = (-1, True)

QUERY = "SELECT * FROM vr WHERE a = $1"
ARGS = (1,)
credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)



def connect_and_parse(_, cp):
    print(" -- connect_and_parse: conn: %s" % (cp,))
    df = cp.runQuery(QUERY, ARGS)
    df.addCallback(show)
    df.addErrback(eb)
    return df

def show(r):
    print(" -- Done: ")#%s " % (r,))
    for x in r:
        print("   %s" % (x,))

def done(r):
    reactor.stop()

def ok_go():
    # connect to the database
    cp = cpool.ConnectionPool(**credentials)
    cp.min = 8
    cp.max = 15
    df = cp.start()

    #conn = connection.Connection(reactor)
    #df = conn.connect(**credentials)
    df.addCallback(connect_and_parse, cp)
    #df.addCallback(connect_and_parse, conn)
    df.addCallback(done)
    df.addErrback(eb)
    return df

def eb(f):
    print(" -- Traceback: %s" % (f.getBriefTraceback(),))
    reactor.stop()

def txmain():
    df = ok_go()
    df.addErrback(eb)
    return df

reactor.callWhenRunning(txmain)
reactor.run()
