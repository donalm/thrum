#!/usr/bin/env pypy
# -*- coding: utf-8 -*-

import os
import sys
import time
import struct
import traceback

from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task
from twisted.python import log, util


from thrum.twisted import pgconnection
from thrum.twisted import pgstatementfactory
#from thrum.twisted import txpostgres
from thrum import binary
from thrum import pypy


try:
    RANGE = int(sys.argv[-1])
except:
    RANGE = 7

QUERY = '''
SELECT
    typname,
    typnamespace,
    typowner,
    typlen,
    typbyval,
    typcategory,
    typispreferred,
    typisdefined,
    typdelim,
    typrelid,
    typelem,
    typarray
FROM
    pg_type
WHERE
    typtypmod = $1 AND
    typisdefined = $2 '''
ARGS = (-1, True)

credentials = dict(user="benchmarkdbuser",
                   password="benchmarkdbpass",
                   host="127.0.0.1",
                   database="hello_world",
                   port=5432)






# connect to the database
conn = pgconnection.Brute()
d = conn.connect(**credentials)

# run the query and print the result
d.addCallback(lambda _: conn.runQuery('select tablename from pg_tables'))
d.addCallback(lambda result: util.println('All tables:', result))

# close the connection, log any errors and stop the reactor
d.addCallback(lambda _: conn.close())
d.addErrback(log.err)
d.addBoth(lambda _: reactor.stop())

# start the reactor to kick off connection estabilishing
reactor.run()
